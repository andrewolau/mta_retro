#!/bin/bash

conda create -n MTA_retro ipykernel -y
source activate MTA_retro

pip install --upgrade 'google-cloud-bigquery[bqstorage,pandas]'
pip install fsspec gcsfs
pip install pandas-gbq
pip install slack-notifications
pip install slackclient

pip install slackeventsapi slack_sdk Flask
conda install pandas
