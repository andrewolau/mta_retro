# python3 -m pip install pipeline-notifier
# gsutil cp gs://wx-lty-de-prod-releases/data-sync/releases/py/datasync/data-sync-latest.tar.gz .
# pip install data-sync-latest.tar.gz
cd markov_chain

python datasync_tables.py loyalty_bi_analytics safarievents_final
python datasync_tables.py loyalty_bi_analytics safarievents_final_agg
python datasync_tables.py loyalty_bi_analytics safarievents_final_missing
python datasync_tables.py loyalty_bi_analytics saf_edm_marketable_snapshot

