# Digital Attribution 

This repository contains the scripts used to generate Multi-touch attribution incremental sales following a markov chain methodology

## How to run

1. allow upstream tables to complete
2. run_datasync.sh
3 wait for 30 mins
4. mc_run_pre_fbaa.sh
5. allow fbaa to complete
6. mc_run_post_fbaa.sh
7. notify reporting team (Rohit/Wilson)