@Library('de-jenkins-libs@master')

def command = "/opt/conda/bin/python3 markov_chain/da_markov_chain.py"

pipeline {
  parameters {
    choice(name: 'DRY', choices: ['true', 'false'], description: 'Whether this is a dry run or not')
	string(name: 'S3_DT', defaultValue: '01-11-2019', description: 'S3 save date')
  }

  agent {
    kubernetes {
      defaultContainer 'ds'
      yaml xpod(de: 'pico', ds: 'large', gcpSecret: 'digital-attribution-prod', awsSecret: 'de-aws-key')
    }
  }

  stages {
	stage('run') {
      steps {
        script {
          if (params.DRY == 'true') {
            command = "echo  '[DRY RUN] ${command}'"
          } else {
            sh command
          }
        }
      }
    }

    stage('gcs-to-s3') {
      when {
        expression {
          params.DRY == 'false'
        }
      }
      steps {
        container('de') {
          sh "gsutil -m rsync -rd gs://digital-attribution-data/phase1/descriptive_stats/latest s3://data-preprod-redshift-exports/digital_attribution_dashboard/phase1/descriptive_stats/${params.S3_DT}"
		  sh "gsutil -m rsync -rd gs://digital-attribution-data/phase1/markov_out/latest s3://data-preprod-redshift-exports/digital_attribution_dashboard/phase1/markov_out/${params.S3_DT}"
        }
      }
    }
	
	stage('conv-extrapolation-check') {
      when {
        expression {
          params.DRY == 'false'
        }
      }
      steps {
        container('de') {
		  sh "gsutil cat gs://digital-attribution-data/phase1/descriptive_stats/latest/desc_conversion_extrapolation.csv"
        }
      }
    }
  
  }

  post {
    always {
      shoot_mail currentBuild: currentBuild, emailList: ['sye1@woolworths.com.au']
    }

    failure {
      shoot_mail currentBuild: currentBuild, emailList: ['ltang2@woolworths.com.au']
    }
  }
}
