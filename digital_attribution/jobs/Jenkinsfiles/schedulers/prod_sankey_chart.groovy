@Library('de-jenkins-libs@master')

def theENV='prod'

pipeline {
    parameters {
      choice(name: 'DRY', choices: ['false', 'true'], description: 'Whether this is a dry run or not')
    }

    agent {
      kubernetes {
        defaultContainer 'de'
        yaml xpod(de: 'pico')
      }
    }

    triggers {
        parameterizedCron('''
# # Sydney time 5:00am (winter) / 6:00am (summer) every Tuesday is ( UTC Time 19:00 , Monday)
0 19 * * 1 % DRY=false
        ''')
    }

    stages {
      stage('sankey_chart') {
        steps {
          build job: '/digital-attribution/sankey_chart', parameters: [
            [$class: 'StringParameterValue', name: 'DRY', value: params.DRY ]
          ]
        }
      }
    }
}
