@Library('de-jenkins-libs@master') _

def theENV='prod'

pipeline {
    parameters {
      choice(name: 'DRY', choices: ['false', 'true'], description: 'Whether this is a dry run or not')
    }

    agent {
      kubernetes {
        defaultContainer 'de'
        yaml xpod(de: 'pico')
      }
    }

    triggers {
        parameterizedCron('''
# # Sydney time 5:30am (winter) / 6:30am (summer) every Tuesday is ( UTC Time 19:30 , Monday)
0 19 * * 1 % DRY=false
        ''')
    }

    stages {
      stage('sankey_chart') {
        steps {
          build job: '/digital-attribution/da_base', parameters: [
            [$class: 'StringParameterValue', name: 'DRY', value: params.DRY ]
          ]
        }
      }
    }
}
