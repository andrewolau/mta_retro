@Library('de-jenkins-libs@master')

def command = null

pipeline {
  parameters {
    choice(name: 'DRY', choices: ['true', 'false'], description: 'Whether this is a dry run or not')
    string(name: 'END_DT', defaultValue: '2019-11-12', description: 'End date')
  }

  agent {
    kubernetes {
      defaultContainer 'ds'
      yaml xpod(de: 'pico', ds: 'medium', gcpSecret: 'digital-attribution-prod')
    }
  }

  stages {
    stage('run') {
      steps {
        script {
          command = "/opt/conda/bin/python3 markov_chain/da_base.py ${params.END_DT}"
          if (params.DRY == 'true') {
            command = "echo '[DRY RUN] ${command}'"
          } else {
            sh command
          }
        }
      }
    }
  }

  post {
    always {
      shoot_mail currentBuild: currentBuild, emailList: ['sye1@woolworths.com.au']
    }

    failure {
      shoot_mail currentBuild: currentBuild, emailList: ['ltang2@woolworths.com.au']
    }
  }
}
