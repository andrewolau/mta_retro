@Library('de-jenkins-libs@master')

def command = "/opt/conda/bin/python3 markov_chain/compile_bayesian_tables.py"

pipeline {
  parameters {
    choice(name: 'DRY', choices: ['true', 'false'], description: 'Whether this is a dry run or not')
	string(name: 'S3_DT', defaultValue: '01-11-2019', description: 'S3 save date')
  }

  agent {
    kubernetes {
      defaultContainer 'ds'
      yaml xpod(de: 'pico', ds: 'large', gcpSecret: 'digital-attribution-prod', awsSecret: 'de-aws-key')
    }
  }

  stages {
    stage('run') {
      steps {
        script {
          if (params.DRY == 'true') {
            command = "echo  '[DRY RUN] ${command}'"
          } else {
            sh command
          }
        }
      }
    }

    stage('gcs-to-s3') {
      when {
        expression {
          params.DRY == 'false'
        }
      }
      steps {
        container('de') {
          sh "gsutil -m rsync -rd gs://digital-attribution-data/phase1/bayesian_tables/latest s3://data-preprod-redshift-exports/digital_attribution_dashboard/phase1/bayesian_tables/${params.S3_DT}"
        }
      }
    }
  }

  post {
    always {
      shoot_mail currentBuild: currentBuild, emailList: ['sye1@woolworths.com.au']
    }

    failure {
      shoot_mail currentBuild: currentBuild, emailList: ['ltang2@woolworths.com.au']
    }
  }
}
