@Library('de-jenkins-libs@master')

def command = null

pipeline {
  parameters {
    choice(name: 'DRY', choices: ['true', 'false'], description: 'Whether this is a dry run or not')
  }

  agent {
    kubernetes {
      defaultContainer 'ds'
      yaml xpod(de: 'pico', ds: 'pico', gcpSecret: 'digital-attribution-prod', awsSecret: 'de-aws-key')
    }
  }

  stages {
    stage('run') {
      when {
        expression {
          params.DRY == 'false'
        }
      }
      steps {
        container('de'){
		  script {
			  command = "${sh 'gsutil cat gs://digital-attribution-data/phase1/descriptive_stats/latest/desc_conversion_extrapolation.csv'}"
			  shoot_mail currentBuild: currentBuild, text = "${gsutil cat gs://digital-attribution-data/phase1/descriptive_stats/latest/desc_conversion_extrapolation.csv}", emailList: ['sye1@woolworths.com.au']
		  }
        }
      }
    }
  }

  post {
    always {
      shoot_mail currentBuild: currentBuild,  emailList: ['sye1@woolworths.com.au']
    }
  }
}
