# from pipelinenotifier import SlackNotifier
import argparse
import sys
import os
import time

work_dir = os.getcwd()
sys.path.append(work_dir)
os.chdir(work_dir)
print('working directory: '+work_dir)

from da_util import *
from da_func import *
from slack import *
from datasync import DataSync


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('schema')
    parser.add_argument('table')
    args = parser.parse_args()
#     WEBHOOK_URL = "https://hooks.slack.com/services/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"
#     with SlackNotifier('test',
#             args.schema + args.table,WEBHOOK_URL) as bot:
        
#     with SlackBotIfFail('MTA data sync', args.schema + args.table,  "https://hooks.slack.com/services/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") as bot:
    ds = DataSync()

    response = ds.sync(
        {"schema": args.schema, "table": args.table, "expireDays": 0},
    )
#         while response['jobStatus'] =='IN_PROGRESS':
#             time.sleep(120)
#             response = ds.get_job(response['jobId'])
#             job_status = response['jobStatus']
# #             LOGGER.info(job_status)
#             if job_status == 'FAILURE':
#                 bot.send_message(job_status)
#                 sys.exit(1)
#         bot.send_message("Job finished successfully.")
