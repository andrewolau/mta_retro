import sys
import os
import datetime
import argparse

work_dir = os.getcwd()
sys.path.append(work_dir)
os.chdir(work_dir)
print('working directory: '+work_dir)

from da_util import *
from da_func import *
from slack import *

if __name__=="__main__":
    
#     with SlackBotIfFail('MTA da_base', 'SYE1', "https://hooks.slack.com/services/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") as slackbot:
        
#         parser = argparse.ArgumentParser()
#         parser.add_argument('--dry_run', action='store_true')
#         args = parser.parse_args()
        
    date_til = sys.argv[1]
    print(f'workong on date till:{date_til}')


    bq_file_mod = [ 
        'base/camp_event_missing'
    ]

    bq_dir = '../bq_query'

    bq_file_run = [
        'base/base_safari_activation_bq_missing',
        'base/eventstore_view',
        'base/camp_event_missing',
        'base/camp_user_journey_missing',
        'base/camp_transition_matrix',
        'base/camp_sankey'
    ]

    for bq in bq_file_mod:

        f = open(f'{bq_dir}/{bq}_raw.sql', "r")
        q = f.read()
        f.close()

        if len(sys.argv) == 1:
            q = q.replace('_FW_START_DATE_', f'\'{wed.strftime("%Y-%m-%d")}\'')
        else:
            q = q.replace('_FW_START_DATE_', f'\'{date_til}\'')

        fw = open(f'{bq_dir}/{bq}.sql', "w+")
        fw.write(q)
        fw.close()
        print(f'date uppdated:{bq}')


    for bq in bq_file_run:

        run_bq(bq_dir, bq)
        print(f'finished: {bq}')

#         slackbot.send_message("Finished")