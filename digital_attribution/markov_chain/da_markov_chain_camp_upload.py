import sys
import os
import datetime
import pandas as pd
import numpy as np
import time
import argparse

work_dir = os.getcwd()
sys.path.append(work_dir)
os.chdir(work_dir)
print('working directory: '+work_dir)

from da_util import *
from da_func import *
from slack import *

#Gets a needless warning in the Removal Effect section of code so suppressing it
import warnings
warnings.filterwarnings('ignore')


if __name__=="__main__":
    
#     with SlackBotIfFail('MTA da_markov_chain', 'SYE1', "https://hooks.slack.com/services/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") as slackbot:

    parser = argparse.ArgumentParser()
    parser.add_argument('--dry_run', action='store_true')
    args = parser.parse_args()

    dateTimeObj = datetime.datetime.now()
    dateObj = dateTimeObj.date()
    date = dateObj.strftime("%Y-%m-%d")

    t = time.time()


    ############## INIT ################

    simsize = 1000
    gs_dir = 'gs://digital-attribution-data/phase2'
    s3_bucket = 'data-preprod-redshift-exports'
    s3_key = 'digital_attribution_dashboard/phase2/markov_out'

    def get_data(df, key):

        trans0 = df[df['key'] == key].drop(['key'], axis = 1)

        trans = pd.DataFrame(trans0.groupby(by=['state1','state2']).sum()).reset_index()
        trans['prob'] = pd.to_numeric(trans['prob'])
        trans['CUM_PROB'] = pd.DataFrame(trans.groupby(by=['state1']).cumsum()).reset_index()['prob']
        trans['MIN_PROB'] = trans['CUM_PROB'] - trans['prob']
        trans.columns = ['FROM','TO','PROB','CUM_PROB','MIN_PROB']

        return trans



    ############### MC ################

    bq_dir = '../bq_query/mc'

#         trans_matrix = run_bq(bq_dir, 'mc_camp_trans_matrix')
#         mc_type = run_bq(bq_dir, 'mc_camp_type')
#         channels = run_bq(bq_dir, 'mc_camp_hierarchy')
#         stats = run_bq(bq_dir, 'mc_camp_stats')

#         mc_final = None
#         # for i in range(0,1):
#         for i in mc_type.index:

#             key = mc_type.key[i]
#             print(f'working on {key}')

#             ############### Start markov ################

#             sample = path_simulation(get_data(trans_matrix, key), simsize)

#             mc_channel = removal_effect(sample.copy(), channels['Channel'])[['Event','prob_norm']]\
#                             .rename(columns={'Event':'Channel', 'prob_norm':'ratio_channel'})

#             mc_medium = removal_effect(sample.copy(), channels['Medium'])[['Event','prob_norm']]\
#                             .rename(columns={'Event':'Medium', 'prob_norm':'ratio_medium'})

#             mc_event = removal_effect(sample.copy(), channels['Event']).sort_values(by = ['Event']).reset_index(drop = True)

#             mc_comb = mc_event[['Event','conv','vol','prob_norm']]\
#                                 .merge(channels, on = 'Event')\
#                                 .merge(mc_medium[['Medium','ratio_medium']], on = 'Medium')\
#                                 .merge(mc_channel[['Channel','ratio_channel']], on = 'Channel')
#             mc_comb['prob_norm_medium'] = mc_comb['prob_norm']*mc_comb['ratio_medium']/\
#                                         (mc_comb['prob_norm'].groupby(mc_comb['Medium']).transform('sum') + 1e-6)
#             mc_comb['prob_norm_channel'] = mc_comb['prob_norm_medium']*mc_comb['ratio_channel']/\
#                                         (mc_comb['prob_norm_medium'].groupby(mc_comb['Channel']).transform('sum') + 1e-6)


#             ############### Load stats data ################

#             mc_stats = stats[stats['key']==key].reset_index(drop = True)  

#             mc_comb['key'] = key
#             mc_comb['campaign_code'] = mc_type.campaign_code[i]
#             mc_comb['banner'] = mc_type.banner[i]

#             mc_comb = mc_comb[['key','banner','campaign_code','Channel','Medium','Event','prob_norm_channel']]\
#                         .merge(mc_stats, on = ['key','banner','campaign_code','Medium','Event'], how = 'left').fillna(0)
#             mc_comb = mc_comb[mc_comb['prob_norm_channel'] > 0]

#             print(mc_comb[['Event','prob_norm_channel']].sort_values(by=['prob_norm_channel'], ascending = False).head(10))


#             ############### Construct final output ################

#             if mc_final is None:
#                 mc_final = mc_comb.sort_values(by = ['prob_norm_channel'], ascending=False).copy()
#             else:
#                 mc_final = mc_final.append(mc_comb.sort_values(by = ['prob_norm_channel'], ascending=False))




#         ############### final output ################

#         mc_final = mc_final[
#             ['key', 'banner', 'campaign_code', 'campaign_start_date', 'campaign_end_date', 'Channel', 'Medium', 'Event', 'volume', 'paths', 'paths_conv', 'paths_medium', 'paths_conv_medium', 'prob_norm_channel']]
#         mc_final.to_csv(f'{gs_dir}/markov_out/latest/mc_final.csv', index = False, line_terminator='\n')

#         table_id = "wx-bq-poc.digital_attribution_modelling.dacamp_prod_mc_final"
#         client.query(f'drop table if exists {table_id}')

#         job_config = bigquery.LoadJobConfig()
#         job_config.schema = [
#             bigquery.SchemaField("key", "STRING", mode="REQUIRED"),
#             bigquery.SchemaField("banner", "STRING", mode="REQUIRED"),
#             bigquery.SchemaField("campaign_code", "STRING", mode="REQUIRED"),
#             bigquery.SchemaField("campaign_start_date", "STRING", mode="REQUIRED"),
#             bigquery.SchemaField("campaign_end_date", "STRING", mode="REQUIRED"),
#             bigquery.SchemaField("channel", "STRING", mode="REQUIRED"),
#             bigquery.SchemaField("medium", "STRING", mode="REQUIRED"),
#             bigquery.SchemaField("event", "STRING", mode="REQUIRED"),
#             bigquery.SchemaField("event_volume", "FLOAT64", mode="REQUIRED"),
#             bigquery.SchemaField("event_reach", "FLOAT64", mode="REQUIRED"),
#             bigquery.SchemaField("event_converted_crn", "FLOAT64", mode="REQUIRED"),
#             bigquery.SchemaField("medium_reach", "FLOAT64", mode="REQUIRED"),
#             bigquery.SchemaField("medium_converted_crn", "FLOAT64", mode="REQUIRED"),
#             bigquery.SchemaField("channel_prob_norm", "FLOAT64", mode="REQUIRED"),
#         ]

#         job_config.skip_leading_rows = 1
#         job_config.source_format = bigquery.SourceFormat.CSV
#         uri = f'{gs_dir}/markov_out/latest/mc_final.csv'

#         load_job = client.load_table_from_uri(
#             uri, table_id, job_config=job_config
#         )  # API request
#         print("Starting job {}".format(load_job.job_id))

#         load_job.result()  # Waits for table load to complete.
#         print("Job finished.")

#         run_bq(bq_dir, 'mc_camp_mcfinal_crn_base')

    file_path = f'./output/'
    directory = os.path.dirname(file_path)
    print(directory)
    
    if not os.path.exists(directory):
        os.mkdir(directory)  

    mc_final_crn = run_bq(bq_dir, 'mc_camp_mcfinal_crn')
    mc_final_crn_post = run_bq(bq_dir, 'mc_camp_mcfinal_crn_post')
    mc_final_agg = run_bq(bq_dir, 'mc_camp_mcfinal_agg')
    mc_final_agg_ws = run_bq(bq_dir, 'mc_camp_mcfinal_agg_ws')
    mc_final_rwapp = run_bq(bq_dir, 'mc_camp_mcfinal_rwapp')
    customer_stage = run_bq(bq_dir, 'customer_stage')
    customer_journey_dtc = run_bq(bq_dir, 'customer_journey_dtc')
    customer_journey_cnt = run_bq(bq_dir, 'customer_journey_cnt')

    mc_final_crn.to_csv(f'./output/mc_final_crn.csv', index = False, line_terminator='\n')
    mc_final_crn_post.to_csv(f'./output/mc_final_crn_post.csv', index = False, line_terminator='\n')
    mc_final_agg.to_csv(f'./output/mc_final_agg.csv', index = False, line_terminator='\n')
    mc_final_agg_ws.to_csv(f'./output/mc_final_agg_ws.csv', index = False, line_terminator='\n')
    mc_final_rwapp.to_csv(f'./output/mc_final_rwapp.csv', index = False, line_terminator='\n')
    customer_stage.to_csv(f'./output/customer_stage.csv', index = False, line_terminator='\n')
    customer_journey_dtc.to_csv(f'./output/customer_journey_dtc.csv', index = False, line_terminator='\n')
    customer_journey_cnt.to_csv(f'./output/customer_journey_cnt.csv', index = False, line_terminator='\n')

    if not args.dry_run:

        mc_final_crn.to_csv(f'{gs_dir}/markov_out/{date}/mc_final_crn.csv', index = False, line_terminator='\n')
        mc_final_crn_post.to_csv(f'{gs_dir}/markov_out/{date}/mc_final_crn_post.csv', index = False, line_terminator='\n')
        mc_final_agg.to_csv(f'{gs_dir}/markov_out/{date}/mc_final_agg.csv', index = False, line_terminator='\n')
        mc_final_rwapp.to_csv(f'{gs_dir}/markov_out/{date}/mc_final_rwapp.csv', index = False, line_terminator='\n')
        customer_stage.to_csv(f'{gs_dir}/markov_out/{date}/customer_stage.csv', index = False, line_terminator='\n')
        customer_journey_dtc.to_csv(f'{gs_dir}/markov_out/{date}/customer_journey_dtc.csv', index = False, line_terminator='\n')
        customer_journey_cnt.to_csv(f'{gs_dir}/markov_out/{date}/customer_journey_cnt.csv', index = False, line_terminator='\n')

        s3.upload_file('./output/mc_final_crn.csv', s3_bucket, f'{s3_key}/{date}/mc_final_crn.csv')
        s3.upload_file('./output/mc_final_crn_post.csv', s3_bucket, f'{s3_key}/{date}/mc_final_crn_halo_post.csv')
        s3.upload_file('./output/mc_final_agg.csv', s3_bucket, f'{s3_key}/{date}/mc_final_agg.csv')
        s3.upload_file('./output/mc_final_agg_ws.csv', s3_bucket, f'{s3_key}/{date}/mc_final_agg_ws.csv')
        s3.upload_file('./output/mc_final_rwapp.csv', s3_bucket, f'{s3_key}/{date}/mc_final_rwapp.csv')
        s3.upload_file('./output/customer_stage.csv', s3_bucket, f'{s3_key}/{date}/customer_stage.csv')
        s3.upload_file('./output/customer_journey_dtc.csv', s3_bucket, f'{s3_key}/{date}/customer_journey_dtc.csv')
        s3.upload_file('./output/customer_journey_cnt.csv', s3_bucket, f'{s3_key}/{date}/customer_journey_cnt.csv')
        s3.upload_file('./output/sankey_chart_data.csv', s3_bucket, f'{s3_key}/{date}/sankey_chart_data.csv')
        s3.upload_file('./output/sankey_chart_label.csv', s3_bucket, f'{s3_key}/{date}/sankey_chart_label.csv')

        s3.upload_file('./output/mc_final_crn.csv', s3_bucket, f'{s3_key}/to_load/mc_final_crn.csv')
        s3.upload_file('./output/mc_final_crn_post.csv', s3_bucket, f'{s3_key}/to_load/mc_final_crn_halo_post.csv')
        s3.upload_file('./output/mc_final_agg.csv', s3_bucket, f'{s3_key}/to_load/mc_final_agg.csv')
        s3.upload_file('./output/mc_final_agg_ws.csv', s3_bucket, f'{s3_key}/to_load/mc_final_agg_ws.csv')
        s3.upload_file('./output/mc_final_rwapp.csv', s3_bucket, f'{s3_key}/to_load/mc_final_rwapp.csv')
        s3.upload_file('./output/customer_stage.csv', s3_bucket, f'{s3_key}/to_load/customer_stage.csv')
        s3.upload_file('./output/customer_journey_dtc.csv', s3_bucket, f'{s3_key}/to_load/customer_journey_dtc.csv')
        s3.upload_file('./output/customer_journey_cnt.csv', s3_bucket, f'{s3_key}/to_load/customer_journey_cnt.csv')
        s3.upload_file('./output/sankey_chart_data.csv', s3_bucket, f'{s3_key}/to_load/sankey_chart_data.csv')
        s3.upload_file('./output/sankey_chart_label.csv', s3_bucket, f'{s3_key}/to_load/sankey_chart_label.csv')

    else:

        mc_final_crn.to_csv(f'{gs_dir}/markov_out_test/{date}/mc_final_crn.csv', index = False, line_terminator='\n')
        mc_final_crn_post.to_csv(f'{gs_dir}/markov_out_test/{date}/mc_final_crn_halo_post.csv', index = False, line_terminator='\n')
        mc_final_agg.to_csv(f'{gs_dir}/markov_out_test/{date}/mc_final_agg.csv', index = False, line_terminator='\n')
        mc_final_rwapp.to_csv(f'{gs_dir}/markov_out_test/{date}/mc_final_rwapp.csv', index = False, line_terminator='\n')
        customer_stage.to_csv(f'{gs_dir}/markov_out_test/{date}/customer_stage.csv', index = False, line_terminator='\n')
        customer_journey_dtc.to_csv(f'{gs_dir}/markov_out_test/{date}/customer_journey_dtc.csv', index = False, line_terminator='\n')
        customer_journey_cnt.to_csv(f'{gs_dir}/markov_out_test/{date}/customer_journey_cnt.csv', index = False, line_terminator='\n')

        s3.upload_file('./output/mc_final_crn.csv', s3_bucket, f'{s3_key}/{date}/mc_final_crn_test.csv')
        s3.upload_file('./output/mc_final_crn_post.csv', s3_bucket, f'{s3_key}/{date}/mc_final_crn_halo_post_test.csv')
        s3.upload_file('./output/mc_final_agg.csv', s3_bucket, f'{s3_key}/{date}/mc_final_agg_test.csv')
        s3.upload_file('./output/mc_final_agg_ws.csv', s3_bucket, f'{s3_key}/{date}/mc_final_agg_ws_test.csv')
        s3.upload_file('./output/mc_final_rwapp.csv', s3_bucket, f'{s3_key}/{date}/mc_final_rwapp_test.csv')
        s3.upload_file('./output/customer_stage.csv', s3_bucket, f'{s3_key}/{date}/customer_stage_test.csv')
        s3.upload_file('./output/customer_journey_dtc.csv', s3_bucket, f'{s3_key}/{date}/customer_journey_dtc_test.csv')
        s3.upload_file('./output/customer_journey_cnt.csv', s3_bucket, f'{s3_key}/{date}/customer_journey_cnt_test.csv')
        s3.upload_file('./output/sankey_chart_data.csv', s3_bucket, f'{s3_key}/{date}/sankey_chart_data_test.csv')
        s3.upload_file('./output/sankey_chart_label.csv', s3_bucket, f'{s3_key}/{date}/sankey_chart_label_test.csv')
            
#         slackbot.send_message("Finished")
        