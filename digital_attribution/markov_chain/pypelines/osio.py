import os
import pandas as pd
import datetime as dt

def make_local_view(view_name, date=dt.datetime.now(), local_loc = 'data'):
    """
For structuring projects. Creates a path for the data and returns the path as a string

view_name: the name of the view (dataset)
date: The working date - usually the date the data was produced, or the data's keyed date
local_loc: The local directory to place the view and subfolders
"""
    outpath = local_loc + "/" + as_view(view_name, date=date)
    try:
        os.makedirs(outpath)
    except:
        None
    return outpath

def make_headerfile(path_to_file):
    """
Given a file path and name, return the path and name of that file's header as a string
"""
    return "/".join(path_to_file.split("/")[:-1]+["_"+path_to_file.split("/")[-1]+".header"])

def pd_read_csv(path_to_snapshot, sep="|", **kwargs):
    """
A wrapper for pandas' read_csv function that automatically handles header files
"""
    rfile = path_to_snapshot
    hfile = make_headerfile(path_to_snapshot)
    headers = pd.read_csv(hfile, sep=sep).columns.values
    return pd.read_csv(rfile, sep=sep, names=headers, **kwargs)

def pd_write_csv(data, outpath, sep="|", **kwargs):
    """
A wrapper for pandas' write.csv method that automatically handles header files
"""
    hfile = make_headerfile(outpath)
    data.to_csv(outpath, header=False, index=False, sep=sep, **kwargs)
    data[:0].to_csv(hfile, header=True, index=False, sep=sep, **kwargs)
    return outpath

def as_view(view_name, date=dt.datetime.now()):
    """
Takes a view name and a date and returns a string that reflects the directory structure of that pair
"""
    return date.strftime(view_name+"/year=%Y/month=%m/day=%d")

def _strip_trailing_slash(path):
    if path[-1] == '/':
        return path[:-1]
    else:
        return path
