import boto, boto3, os
from . import osio

def ls(prefix="", bucket="data-preprod-redshift-exports"):
    """
List the contents of a folder on a bucket. If no folder given, list the top level contents of the bucket
"""
    s3 = boto.connect_s3()
    s3root = s3.get_bucket(bucket)
    if len(prefix) > 0 and prefix[-1] != '/':
        prefix += "/"
    return [item.name for item in list(s3root.list(prefix,"/"))]

def download(filepath, bucket="data-preprod-redshift-exports"):
    """
Download a file from s3 to a local location. Keeps the filename of the s3 key.
"""
    s3 = boto3.resource('s3')
    outfile = filepath.split("/")[-1]
    outpath= osio.make_local_view(outfile.split(".")[0].replace(" ","_"))+"/"+outfile+".csv"
    s3.Bucket(bucket).download_file(filepath, outpath)
    return outpath

def downsync(s3_folder="", bucket="data-preprod-redshift-exports", local_loc="data"):
    """
Download an entire directory from s3, inserting files into view-based subdirectories
"""
    if s3_folder[-1] != "/":
        s3_folder += "/"
    s3 = boto.connect_s3()
    s3root = s3.get_bucket(bucket)
    download_files = [item.name for item in list(s3root.list(s3_folder))]
    download_files = [dfile for dfile in download_files if dfile[-1] != '/']
    file_list = []
    for dfile in download_files:
        file_list.append(download(dfile, bucket=bucket))
    return file_list

def upload(local_file, s3_folder, bucket="data-preprod-redshift-exports", verbose=False):
    """
Upload a file to an s3 folder and a bucket
"""
    s3 = boto3.resource('s3')
    outfile = local_file.split("/")[-1]
    s3_path = "/".join([s3_folder,outfile])
    if verbose:
        print("bucket:",bucket,"\nlocal_file:",local_file,"\ns3_path:",s3_path)
    s3.Bucket(bucket).upload_file(local_file, s3_path)
    return s3_path

def upload_view(view_name, bucket="data-preprod-redshift-exports", local_loc="data", verbose=False):
    """
Upload a local folder, preserving view structure
"""
    view_path = osio.make_local_view(view_name, local_loc=local_loc)
    s3_loc = osio.as_view(view_name)
    for ifile in os.listdir(view_path):
        inloc = view_path +"/"+ifile
        outloc = s3_loc
        upload(inloc, outloc, bucket=bucket, verbose=verbose)
            
    

def upsync(local_folder, s3_folder, bucket="data-preprod-redshift-exports", verbose=False):
    """
Upload a directory to s3, WITHOUT  directory structure
"""
    for ifile in os.listdir(local_folder):
        upload('/'.join([local_folder,ifile]), s3_folder, bucket)
