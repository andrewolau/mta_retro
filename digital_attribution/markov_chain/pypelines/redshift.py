import psycopg2
from psycopg2 import extensions as ext
import json
import pandas as pd
import os
from . import osio

class connection:

    def __init__(self, credfile=None, credentials=None):
        """
Open a connection to an SQL database specified in a credfile. The credfile is a json with the following fields:

dbname: The name of the database being queried (e.g 'datacap')
host: The host of the database being queried (e.g 'data-cap-preprod-cluster.wowmp.com.au')
port: The port used to access the database (e.g '443')
user: The username used for access, same as Redshift
password: The user's Redshift password

All these fields are the same as those used to query tables on Redshift

The class will automatically generate a list of available tables, visible via the tables attribute
"""

        if credentials is None:
            with open(credfile, 'r') as creds:
             self.creds = json.load(creds)
        else:
            self.creds = credentials
        
        self.conn = psycopg2.connect(
            dbname = self.creds['dbname'],
            host = self.creds['host'],
            port = self.creds['port'],
            user = self.creds['user'],
            password = self.creds['password']
        )
        self.tables = pd.DataFrame(
            self.fetch("""SELECT table_schema,table_name FROM information_schema.tables"""),
            columns=['schema','table']
            )
        self.typedic = {
            # THESE WILL OBVIOUSLY NEED TO BE EXPANDED AT SOME POINT
            # datetime64[ns], datetime64[ns, tz] and timedelta[ns] may want precise manage thus not included
            'object':'VARCHAR',
            'category':'VARCHAR',
            'int64':'INT',
            'int32':'INT',
            'int16':'INT',
            'int8':'INT',
            'int':'INT',
            'float128':'FLOAT8',
            'float64':'FLOAT8',
            'float32':'FLOAT8',
            'float16':'FLOAT8',
            'float':'FLOAT4',
            'bool':'BOOLEAN'
        }
        
    def close(self):
        """
Close the connection
"""
        self.conn.close()
        self.conn = None
        
    def reset_connection(self):
        """
Used to reset a connection that is stuck on an error, not intended for API usage
"""
        self.conn = psycopg2.connect(
            dbname = self.creds['dbname'],
            host = self.creds['host'],
            port = self.creds['port'],
            user = self.creds['user'],
            password = self.creds['password']
        )
            
    def fetch(self, sql_statement, verbose=False):
        """
Execute an SQL query and return the result as a list
"""
        if verbose:
            print(sql_statement)
        self.reset_connection()
        cursor = self.conn.cursor()
        cursor.execute(sql_statement)
        return cursor.fetchall()
    
    def fetch_pandas(self, sql_statement, verbose=False):
        """
Execute an SQL query and return the result as a Pandas Dataframe
"""
        if verbose:
            print(sql_statement)
        return pd.read_sql(sql_statement, self.conn)
    

    def commit(self, sql_statement, verbose=False):
        """
Execute an SQL query and make lasting changes to the database (but do not fetch)
"""
        if verbose:
            print(sql_statement)
        self.reset_connection()
        cursor = self.conn.cursor()
        cursor.execute(sql_statement)
        self.conn.commit()
    
    def print_columns(self, table_name):
        """
Print the columns of a given table
"""
        if '.' in table_name: # check schema input
            tschema = table_name.split('.')[0]
            tname = table_name.split('.')[1]
            return pd.DataFrame(self.fetch(f"""
            SELECT ordinal_position,column_name 
            FROM information_schema.columns where table_name = '{tname}' and table_schema = '{tschema}' 
            ORDER BY ordinal_position
        """),columns=['position','column_name'])
        return pd.DataFrame(self.fetch(f"""
        SELECT ordinal_position,column_name 
        FROM information_schema.columns where table_name = '{table_name}' 
        ORDER BY ordinal_position
    """),columns=['position','column_name'])

    
    def download_table(self, table_name, delim="|", sample=False):
        """
Downloads a table to a file on the disk. Automatically strips header and sets up directory structure.

table_name: The name of the table to download, excluding the table_schema (e.g mars_28_bigw_txn_bskt, do not including loyalty_modeling)
local_loc: Pathing for the output file
delim: The delimiter to use for writing out the data (default is pipe)
sample: If True, only download the first 10,000 rows
table_schema: The named collection of tables. (e.g the loyalty_modeling part of the mars_28_bigw_txn_bskt)
"""
        outfile = "/".join([osio.make_local_view(table_name), table_name+".csv"])
        hfile = osio.make_headerfile(outfile)
        
        if '.' in table_name: # check schema input
            tschema = table_name.split('.')[0]
            tname = table_name.split('.')[1]
            metadata = pd.DataFrame(self.fetch(f"""
                SELECT table_schema,table_name,column_name,ordinal_position
                FROM information_schema.columns where table_name = '{tname}' and table_schema = '{tschema}'
            """), columns=['schema','table','column','position'])
        else:
            metadata = pd.DataFrame(self.fetch(f"""
                SELECT table_schema,table_name,column_name,ordinal_position 
                FROM information_schema.columns where table_name = '{table_name}'
            """), columns=['schema','table','column','position'])

        tobject = '.'.join([metadata['schema'].values[0], table_name])
        query = f"""SELECT * FROM {tobject}"""
        if sample:
            query += " LIMIT 10000"

        cursor = self.conn.cursor()
        result = cursor.execute(query)
        metadata = metadata.sort_values(by='position').set_index('position')['column'].unique()

        with open(hfile,'w', encoding='utf-8') as header_out:
            header_out.write(delim.join(metadata))
        with open(outfile,'w', encoding='utf-8') as data_out:
            for chunk in cursor:
                data_out.write(delim.join([str(j) for j in chunk]))
                data_out.write('\n')
        return outfile

    def upload_to_table(self, df, table_name, columns=None, schema='loyalty_modeling', overwrite=False, verbose=False):
        """
Takes a data frame and saves it as a redshift table with name table_name. The data will lie in the same database as the connection (i.e prod or preprod is determined by the credentials)

df: A pandas Data Frame object
table_name: The table name as it will appear in Redshift
columns: A subset (list) of columns to be uploaded
schema: The subgroup of the database the table will reside in
overwrite: Whether to overwrite the existing table
verbose: Prints the SQL commands sent to Redshift
"""
        self.reset_connection()
        cur = self.conn.cursor()
        table_long ='{}.{}'.format(schema, table_name)
        if overwrite:
            del_cmd = 'drop table if exists {}'.format(table_long)
            if verbose:
                print(del_cmd)
            self.commit(del_cmd)
            
        if columns == None:
            columns = df.columns.values
            
        df = df.copy()[columns]
        data = tuple([tuple(i) for i in df.values])
        coltypes = [(col, self.typedic[df[col].dtype.name]) for col in columns]
        coltypes = ' , '.join([' '.join(i) for i in coltypes])
        create_cmd = "create table {} ({});".format(table_long, coltypes)
        if verbose:
            print(create_cmd)
        self.commit(create_cmd)
        
        records_list_template = ','.join(['%s'] * len(df))
        insert_cmd = ''' insert into {} ({}) VALUES {} '''.format(
            table_long, ', '.join(columns), records_list_template
            )
        if verbose:
            print(insert_cmd[:1000])
        cursor = self.conn.cursor()
        cursor.execute(insert_cmd, data)
        self.conn.commit()
        return table_long

    def execute_file(self, script_location, return_query=False):
        """
Takes the path to an sql file, then runs that file on Redshift. Will return the query if required.
"""
        with open(script_location,'r') as infile:
            query = infile.read()
        self.commit(query)
        if return_query:
            return query
