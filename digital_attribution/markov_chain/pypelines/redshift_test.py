
import configparser
from pprint import pprint
import re
import sys
import pandas as pd
import redshift 

config = configparser.ConfigParser()
config.read('credentials.txt') 

cfg = config["PRE"]
#
#[PRE]
#user=<USER>
#password=
#dbname=datacap
#host=data-cap-preprod-cluster.wowmp.com.au
#port=443
#
redX = redshift.connection(credentials=cfg)
cfg = None

tb_list_df = redX.fetch_pandas("select * from loyalty_modeling.mars_01_membership limit 20;", verbose=True)

print(tb_list_df.head(20))

redX.close()	
