"""Contextmanager for Slackbot notifications
"""
import requests
import json
from contextlib import AbstractContextManager

def send_message(MESSAGE:str, webhook_url="https://hooks.slack.com/services/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", project_manager="JWONG7"):
    """Upload slack data"""
    slack_data = {'text': project_manager + ": " + MESSAGE}
    response = requests.post(
        webhook_url, data=json.dumps(slack_data),
        headers={'Content-Type': 'application/json'}
    )
    if response.status_code != 200:
        raise ValueError(
            'Request to slack returned an error %s, the response is:\n%s'
            % (response.status_code, response.text)
        )


class SlackBot:
    """Class wrapper for a chatbot in Slack to work.
    """
    def __init__(self, project_name: str, project_manager: str, webhook_url: str):
        self.project_name = project_name
        self.project_manager = project_manager
        self.webhook_url = webhook_url

    def send_message(self, message):
        """Send slack message"""
        slack_data = {'text': self.project_manager + " | " + self.project_name + ": " + message}
        response = requests.post(
            self.webhook_url, data=json.dumps(slack_data),
            headers={'Content-Type': 'application/json'}
        )
        if response.status_code != 200:
            raise ValueError(
                'Request to slack returned an error %s, the response is:\n%s'
                % (response.status_code, response.text)
            )

class SlackBotIfFail(AbstractContextManager, SlackBot):
    """Slackbot that automatically messages if started or began.
    """
    def __init__(self, project_name, project_manager, webhook_url):
        super(SlackBotIfFail, self).__init__(project_name, project_manager, webhook_url)

    def __enter__(self):
        self.send_message("Began.")
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        if exc_type is None:
            self.send_message("Finished successfully.")
        else:
            self.send_message(str(exc_type) + str(exc_value))
