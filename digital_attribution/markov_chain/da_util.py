import boto3
import s3fs
import pandas as pd
import numpy as np
import functools
import psycopg2
import json
import os
from google.cloud import storage
from google.cloud import bigquery
my_path = os.path.abspath(os.path.dirname(__file__))

# specify AWS key pair cred here
credfile = os.path.join(my_path, 'cred.json')

with open(credfile, 'r') as creds:
    cred = json.load(creds)

redshift_user = cred['user']
redshift_password = cred['password']
prod_url = cred['host']
aws_key = cred['aws_key']
aws_secret_key = cred['aws_secret_key']
s3_bucket = cred['s3_bucket']
s3_dir = cred['s3_dir']
gcp_project_id = cred['gcp_project_id']
gcp_cred_bq = cred['gcp_cred_bq']
gs_bucket = cred['gs_bucket']
gs_dir = cred['gs_dir']


# specify GCP service account here 
os.environ["GOOGLE_APPLICATION_CREDENTIALS"] = './gcp_serv_ac.json'

session = boto3.Session(aws_access_key_id=aws_key,
                        aws_secret_access_key=aws_secret_key)
s3 = boto3.client('s3', aws_access_key_id=aws_key,
                        aws_secret_access_key=aws_secret_key)
fs = s3fs.S3FileSystem(key=aws_key, secret=aws_secret_key)

client = bigquery.Client(gcp_project_id)
bucket = storage.Client(gcp_project_id).get_bucket('digital-attribution-data')


def get_matching_s3_objects(bucket, prefix='', suffix=''):
    """
    Generate objects in an S3 bucket.

    :param bucket: Name of the S3 bucket.
    :param prefix: Only fetch objects whose key starts with
        this prefix (optional).
    :param suffix: Only fetch objects whose keys end with
        this suffix (optional).
    """
    s3 = session.client('s3')
    kwargs = {'Bucket': bucket}

    # If the prefix is a single string (not a tuple of strings), we can
    # do the filtering directly in the S3 API.
    if isinstance(prefix, str):
        kwargs['Prefix'] = prefix

    while True:

        resp = s3.list_objects_v2(**kwargs)

        try:
            contents = resp['Contents']
        except KeyError:
            return

        for obj in contents:
            key = obj['Key']
            if key.startswith(prefix) and key.endswith(suffix):
                yield obj

        try:
            kwargs['ContinuationToken'] = resp['NextContinuationToken']
        except KeyError:
            break


def get_matching_s3_keys(bucket, prefix='', suffix=''):
    """
    Generate the keys in an S3 bucket.

    :param bucket: Name of the S3 bucket.
    :param prefix: Only fetch keys that start with this prefix (optional).
    :param suffix: Only fetch keys that end with this suffix (optional).
    """
    for obj in get_matching_s3_objects(bucket, prefix, suffix):
        yield obj['Key']


def start_con():
    global con, cur
    con = psycopg2.connect(dbname='datacap', host=prod_url, port='443',
                           user=redshift_user, password=redshift_password)
    cur = con.cursor()


def close_con():
    cur.close()
    con.close()


def get_schema(schema_name, tbl_name):
    """ Get schema from redshift table

    Args:
        schema_name (str): folder name of the table
        tbl_name (str): table name

    Returns:
        Dataframe containing variable names and types

    Example:
        get_schema('loyalty_modeling', 'mars_00_article')

    """

    cur.execute(f"""
                SELECT column_name, udt_name, character_maximum_length, numeric_precision, numeric_scale
                FROM information_schema.columns
                WHERE table_schema = '{schema_name}' AND table_name = '{tbl_name}'
                ORDER BY ordinal_position;""")

    rdx_schema = pd.DataFrame(cur.fetchall())
    rdx_schema.columns = ['name', 'type', 'char_length', 'num_precision',
                          'num_scale']
    return rdx_schema


def run_bq(bq_dir, bq_file):
    
    print(f'running query: {bq_file}')

    f = open(f'{bq_dir}/{bq_file}.sql', "r")
    q = f.read()
    f.close()

    query_job = client.query(q)
    result = query_job.result()
    df = result.to_dataframe()
    
    return df




