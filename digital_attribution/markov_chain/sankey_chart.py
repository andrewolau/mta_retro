import sys
import os
import datetime
import pandas as pd
import plotly as py
import plotly.graph_objects as go
import argparse

work_dir = os.getcwd()
sys.path.append(work_dir)
os.chdir(work_dir)
print('working directory: '+work_dir)

from da_util import *
from da_func import *
from slack import *

dateTimeObj = datetime.datetime.now()
dateObj = dateTimeObj.date()
date = dateObj.strftime("%Y-%m-%d")


def get_data(is_activate, key):
    client = bigquery.Client()

    if is_activate == 0:
        query_job = client.query(f'SELECT sum(t1.conv_flag) as c1, sum(1-t1.conv_flag) as c2 FROM digital_attribution_modelling.dacamp_prod_sankey t1 where t1.key = \'{key}\'')

    results = query_job.result()  # Waits for job to complete.

    data_set1 = []
    for row in results:
        volume1 = max(int(row["c1"]*0.01), 10)
        volume2 = max(int(row["c2"]*0.01), 100)

    print ("converted:", volume1)
    print ("not converted:", volume2)


    if is_activate == 0:
        query_job = client.query(f'SELECT t1.* FROM digital_attribution_modelling.dacamp_prod_sankey t1 where t1.conv_flag=1 and t1.key = \'{key}\' LIMIT {volume1}')

    results = query_job.result()  # Waits for job to complete.
    data_set1 = [row for row in results]

    if is_activate == 0:
        query_job = client.query(f'SELECT t1.* FROM digital_attribution_modelling.dacamp_prod_sankey t1 where t1.conv_flag=0 and t1.key = \'{key}\' LIMIT {volume2}')

    results = query_job.result()  # Waits for job to complete.x
    data_set2 = [row for row in results]

    return data_set1, data_set2 


def get_label(is_pos, is_activate):

    if is_pos:
        if is_activate == 0:
            return "Conversion"

    else:
        if is_activate == 0:
            return "No Conversion"


def create_edge_set(data_set, is_positive_case, event_map, occur_count, spend_sum, is_activate):

    sources = []
    targets = []
    values_sum = []
    values_count = []

    for row in data_set:

        event_names = []
        prev_pos = None
        for pos1 in reversed(range(1, 21)):
            if row["pos_" + str(pos1)] == None:
                continue

            if prev_pos != row["pos_" + str(pos1)]:
                event_names.append(row["pos_" + str(pos1)])
                prev_pos = event_names[-1]

        event_names.append(get_label(is_positive_case, is_activate))

        for pos1 in range(len(event_names) - 1):

            key1 = str(event_names[pos1]) + ": " + str(pos1)

            if key1 not in event_map:
                event_map[key1] = len(event_map)

            pos2 = pos1 + 1

            if pos2 != len(event_names) - 1:
                key2 = str(event_names[pos2]) + ": " + str(pos2)
            else:
                key2 = str(event_names[pos2])

            if key2 not in event_map:
                event_map[key2] = len(event_map)

            sources.append(event_map[key1])
            targets.append(event_map[key2])
            values_sum.append(row['spend'])
            values_count.append(1)

    return event_map, occur_count, spend_sum, sources, targets, values_sum, values_count

def check_path(sources, targets, values, sink_ids):

    is_error = True
    tree_map = {}

    for s, t, v in zip(sources, targets, values):
        if s not in tree_map:
            tree_map[s] = []

        tree_map[s].append([t, v])

    while is_error:

        is_error = False
        for s in tree_map:
            replace_edges = []
            for e in tree_map[s]:
                if (e[0] in tree_map and len(tree_map[e[0]]) > 0) or (e[0] in sink_ids):
                    replace_edges.append(e)

            tree_map[s] = replace_edges

        for s in tree_map:
            for e in tree_map[s]:
                if (e[0] in tree_map and len(tree_map[e[0]]) > 0) or (e[0] in sink_ids):
                    continue

                is_error = True


    sources = []
    targets = []
    values = []
    for s in tree_map:
        for e in tree_map[s]:
            sources.append(s)
            targets.append(e[0])
            values.append(e[1])

    return sources, targets, values



def get_edge(item, edge_type):

    if edge_type == 0:
        return item["count"]

    
    
def save_chart_edges_to_file(sink_values, event_map, sources, targets, values, bucket, ref_date, is_activate, edge_type):
    labels = ([v.split(":")[0] if v not in sink_values else v for v in list(event_map.keys())])

    if edge_type == 0:
        file_ext = "_by_volume"
 
    if is_activate == 0:
        file_name = "for_activation" + file_ext + ".csv"

    gs_dir = 'gs://digital-attribution-data/phase2'

    df0 = pd.DataFrame()
    df0['sources'] = [hex(v) for v in sources]
    df0['targets'] = [hex(v) for v in targets]
    df0['values'] = values
    df0['values'] = df0['values'] * 100

#     df0.to_csv(f'{gs_dir}/sankey_data/latest/sankey_chart_data_{file_name}', index = False, line_terminator='\n')
#     df0.to_csv(f'./sankey_chart_data_{file_name}', index = False, line_terminator='\n')


    df = pd.DataFrame()
    df['labels'] = labels
    df['ids'] = [hex(v) for v in range(len(labels))]
        

#     df.to_csv(f'{gs_dir}/sankey_data/latest/sankey_chart_labels_{file_name}', index = False, line_terminator='\n')
#     df.to_csv(f'./sankey_chart_labels_{file_name}', index = False, line_terminator='\n')

    return df0, df


def create_sankey_plot(edge_type, data_set1, data_set2, is_activate, conv_type):

    event_map = {}
    occur_count = {}
    spend_sum = {}

    event_map, occur_count, spend_sum, sources1, targets1, values_sum1, values_count1 = create_edge_set(data_set1, True, event_map, occur_count, spend_sum, is_activate)
    
    sources = sources1
    targets = targets1
    values_sum = values_sum1
    values_count = values_count1
    
    if conv_type == 1:
        event_map, occur_count, spend_sum, sources2, targets2, values_sum2, values_count2 = create_edge_set(data_set2, False, event_map, occur_count, spend_sum, is_activate)
    
        sources = sources1 + sources2
        targets = targets1 + targets2
        values_sum = values_sum1 + values_sum2
        values_count = values_count1 + values_count2
            

    final_values = {}
    for s, t, v_sum, v_count in zip(sources, targets, values_sum, values_count):
        key = str(s) + "_" + str(t)

        if key not in final_values:
            final_values[key] = {"s" : s, "t" : t, "count" : 0, "sum" : 0}

        final_values[key]["count"] += v_count
        final_values[key]["sum"] += v_sum

    value_dists = []
    for item in final_values.values():
        value_dists.append({"item" : item, "w" : float(get_edge(item, edge_type))})
    value_dists = sorted(value_dists, key=lambda x: x["w"], reverse=True)

    if conv_type == 1:
        sink_ids = [event_map[get_label(True, is_activate)], event_map[get_label(False, is_activate)]]
        sink_values = [get_label(True, is_activate), get_label(False, is_activate)]
    else:
        sink_ids = [event_map[get_label(True, is_activate)], event_map[get_label(True, is_activate)]]
        sink_values = [get_label(True, is_activate), get_label(True, is_activate)]

    labels = ([v.split(":")[0] if v not in sink_values else v for v in list(event_map.keys())])

    sources = []
    targets = []
    values = []
    for v in value_dists:
        item = v["item"]

        if labels[item["s"]] == labels[item["t"]]:
            continue

        sources.append(item["s"])
        targets.append(item["t"])
        values.append(get_edge(item, edge_type))

        if len(values) > 200:
            break

    sources, targets, values = check_path(sources, targets, values, sink_ids)

    data=[go.Sankey(
        node = dict(
          pad = 20,
          thickness = 20,
          line = dict(color = "black", width = 0.5),
          label = [v.split(":")[0] if v not in sink_values else v for v in list(event_map.keys())],
          color = "blue"
        ),
        link = dict(
          source = sources, # indices correspond to labels, eg A1, A2, A2, B1, ...
          target = targets,
          value = values
      ))]

    if edge_type == 0:
        file_ext = "_by_volume"

    if is_activate == 0:
        file_name = "customer_journey_for_activation" + file_ext + ".html"

#     py.offline.plot(data, filename=file_name)

    dateTimeObj = datetime.datetime.now()
    dateObj = dateTimeObj.date()
    ref_date = dateObj.strftime("%Y-%m-%d")
#     ref_date = sys.argv[1]

#     storage_client = storage.Client()

#     bucket = storage_client.get_bucket('digital-attribution-data')
#     blob = bucket.blob('phase2/customer_journey/' + ref_date +'/' + file_name)
#     blob.upload_from_filename(file_name)

    df0, df1 = save_chart_edges_to_file(sink_values, event_map, sources, targets, values, bucket, ref_date, is_activate, edge_type)

    return df0, df1



if __name__=="__main__":
    
    
#     with SlackBotIfFail('MTA da_sankey_chart', 'SYE1', "https://hooks.slack.com/services/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") as slackbot:
            
    parser = argparse.ArgumentParser()
    parser.add_argument('--dry_run', action='store_true')
    args = parser.parse_args()

    bq_dir = '../bq_query/mc'
    gs_dir = 'gs://digital-attribution-data/phase2'
    s3_dir = 's3://data-preprod-redshift-exports/digital_attribution_dashboard/phase2'

    mc_type = run_bq(bq_dir, 'mc_camp_type_sankey')

    data_final = None
    label_final = None
    for i in mc_type.index:

        key = mc_type.key[i]
        print(key)
        data_set1, data_set2 = get_data(0, key)
        df0, df1 = create_sankey_plot(0, data_set1, data_set2, 0, 1)

        df0['key'] = key
        df1['key'] = key

        if data_final is None:
            data_final = df0.copy()
            label_final = df1.copy()
        else:
            data_final = data_final.append(df0.copy())
            label_final = label_final.append(df1.copy())

    data_final = mc_type.merge(data_final, on = ['key'], how = 'inner').fillna(0).drop(['key','segment'], axis = 1)
    data_final['type'] = 'all'
    label_final = mc_type.merge(label_final, on = ['key'], how = 'inner').fillna(0).drop(['key','segment'], axis = 1)
    label_final['type'] = 'all'


    data_final1 = None
    label_final1 = None
    for i in mc_type.index:

        key = mc_type.key[i]
        print(key)
        data_set1, data_set2 = get_data(0, key)
        df0, df1 = create_sankey_plot(0, data_set1, data_set2, 0, 0)

        df0['key'] = key
        df1['key'] = key

        if data_final1 is None:
            data_final1 = df0.copy()
            label_final1 = df1.copy()
        else:
            data_final1 = data_final1.append(df0.copy())
            label_final1 = label_final1.append(df1.copy())

    data_final1 = mc_type.merge(data_final1, on = ['key'], how = 'inner').fillna(0).drop(['key','segment'], axis = 1)
    data_final1['type'] = 'redemption'
    label_final1 = mc_type.merge(label_final1, on = ['key'], how = 'inner').fillna(0).drop(['key','segment'], axis = 1)
    label_final1['type'] = 'redemption'

    data_final = data_final.append(data_final1).drop(['campaign_type'], axis = 1)
    label_final = label_final.append(label_final1).drop(['campaign_type'], axis = 1)

    data_final.to_csv(f'./output/sankey_chart_data.csv', index = False, line_terminator='\n')
    label_final.to_csv(f'./output/sankey_chart_label.csv', index = False, line_terminator='\n')

    if not args.dry_run:
        data_final.to_csv(f'{gs_dir}/markov_out/{date}/sankey_chart_data.csv', index = False, line_terminator='\n')
        label_final.to_csv(f'{gs_dir}/markov_out/{date}/sankey_chart_label.csv', index = False, line_terminator='\n')
    else:
        data_final.to_csv(f'{gs_dir}/markov_out_test/{date}/sankey_chart_data.csv', index = False, line_terminator='\n')
        label_final.to_csv(f'{gs_dir}/markov_out_test/{date}/sankey_chart_label.csv', index = False, line_terminator='\n')

#         slackbot.send_message("Finished")
