import pandas as pd
import numpy as np
import functools


def path_simulation(trans, simsize):

    sample = pd.DataFrame(np.arange(simsize) + 1, columns=['UID'])
    sample['CONV'] = 0
    sample['STATE1'] = 'START'
    sample['INCOMPLETE'] = 1

    x=1
    colname = 'STATE'+ str(x)

    while sum(sample.INCOMPLETE)!=0:
        #Add a random seed
        sample['RAND'] = np.random.random(len(sample))
        sample = pd.merge(sample, trans[['FROM', 'TO', 'MIN_PROB', 'CUM_PROB']], left_on=[colname], right_on=['FROM'], how='left')
        x=x+1
        colname = 'STATE'+ str(x)
        sample = sample[((sample['RAND'] >= sample['MIN_PROB']) & (sample['RAND'] <= sample['CUM_PROB'])) | sample['CUM_PROB'].isnull()].reset_index(drop = True)
        sample.loc[sample['TO'].isin(['CONV', 'NO CONV']), 'INCOMPLETE'] = 0
        sample.loc[sample['TO'].isin(['CONV']), 'CONV'] = 1

        #Rename TO to latest STATE in LOOP
        sample.rename(columns = {'TO':colname}, inplace = True)
        #drop unneeded columns
        sample = sample.drop(['RAND', 'FROM', 'MIN_PROB', 'CUM_PROB'],1)

    return sample.drop(['INCOMPLETE'],1)



def removal_effect(sample, channels):

    x = sample.shape[1] - 2
    channels = channels.unique().tolist()

    for chan in channels:
        colname = str(chan) + '_flag'
        sample[colname] = functools.reduce(np.logical_or, \
                                [sample['STATE{}'.format(i+1)].str.startswith(chan) for i in range(x)])*1
        sample[colname].fillna(0, inplace=True)

    mc_out = pd.DataFrame(channels)
    mc_out.columns = ['Event']
    mc_out[['vol','conv','prob','no_prob']] = pd.DataFrame([[0,0,0,0]])

    for chan in channels:
        colflag = str(chan) + '_flag'
        mc_out.vol[mc_out.Event == chan] = sum(sample.fillna(0)[colflag])
        mc_out.conv[mc_out.Event == chan] = sum(sample['CONV']*(sample.fillna(0)[colflag]))
        mc_out.prob[mc_out.Event == chan] = sum(sample['CONV']*(sample.fillna(0)[colflag]))/(sum(sample['CONV'])+1e-6)
        mc_out.no_prob[mc_out.Event == chan] = sum((1-sample['CONV'])*(sample.fillna(0)[colflag]))/(sum((1-sample['CONV']))+1e-6)

    #Final Attribution result
    mc_out['prob_norm'] = mc_out.prob/(sum(mc_out.prob)+1e-6)
    mc_out['no_prob_norm'] = mc_out.no_prob/(sum(mc_out.no_prob)+1e-6)

    return mc_out


