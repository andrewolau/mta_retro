import sys
import os
import datetime
import pandas as pd
import numpy as np
import time

work_dir = os.getcwd()
sys.path.append(work_dir)
os.chdir(work_dir)
print('working directory: '+work_dir)

from da_func import *

#Gets a needless warning in the Removal Effect section of code so suppressing it
import warnings
warnings.filterwarnings('ignore')

dateTimeObj = datetime.datetime.now()
dateObj = dateTimeObj.date()
date = dateObj.strftime("%Y-%m-%d")

t = time.time()

############## INIT ################

simsize = 1000
gs_dir = 'gs://digital-attribution-data/phase2'


def get_data(df, key):

    trans0 = df[df['key'] == key].drop(['key'], axis = 1)

    trans = pd.DataFrame(trans0.groupby(by=['state1','state2']).sum()).reset_index()
    trans['prob'] = pd.to_numeric(trans['prob'])
    trans['CUM_PROB'] = pd.DataFrame(trans.groupby(by=['state1']).cumsum()).reset_index()['prob']
    trans['MIN_PROB'] = trans['CUM_PROB'] - trans['prob']
    trans.columns = ['FROM','TO','PROB','CUM_PROB','MIN_PROB']
        
    return trans


############### MC ################

bq_dir = '../bq_query/mc'

trans_matrix = run_bq(bq_dir, 'mc_camp_trans_matrix')
mc_type = run_bq(bq_dir, 'mc_camp_type')
channels = run_bq(bq_dir, 'mc_camp_hierarchy')
stats = run_bq(bq_dir, 'mc_camp_stats')
conv_tot = run_bq(bq_dir, 'mc_camp_conv_tot')

# Markov Chain
mc_final = None
# for i in range(0,1):
for i in mc_type.index:
   
    key = mc_type.key[i]
    print(f'working on {key}')
               
    ############### Start markov ################
    
    sample = path_simulation(get_data(trans_matrix, key), simsize)

    mc_channel = removal_effect(sample.copy(), channels['Channel'])[['Event','prob_norm']]\
                    .rename(columns={'Event':'Channel', 'prob_norm':'ratio_channel'})

    mc_medium = removal_effect(sample.copy(), channels['Medium'])[['Event','prob_norm']]\
                    .rename(columns={'Event':'Medium', 'prob_norm':'ratio_medium'})

    mc_event = removal_effect(sample.copy(), channels['Event']).sort_values(by = ['Event']).reset_index(drop = True)
    
    mc_comb = mc_event[['Event','conv','vol','prob_norm']]\
                        .merge(channels, on = 'Event')\
                        .merge(mc_medium[['Medium','ratio_medium']], on = 'Medium')\
                        .merge(mc_channel[['Channel','ratio_channel']], on = 'Channel')
    mc_comb['prob_norm_medium'] = mc_comb['prob_norm']*mc_comb['ratio_medium']/\
                                (mc_comb['prob_norm'].groupby(mc_comb['Medium']).transform('sum') + 1e-6)
    mc_comb['prob_norm_channel'] = mc_comb['prob_norm_medium']*mc_comb['ratio_channel']/\
                                (mc_comb['prob_norm_medium'].groupby(mc_comb['Channel']).transform('sum') + 1e-6)

    
    ############### Load stats data ################
        
    mc_stats = stats[stats['key']==key].reset_index(drop = True)  
    mc_conv_tot_d = pd.to_numeric(conv_tot[conv_tot.key==key]['spend'])
    
    if (len(mc_conv_tot_d) == 0):
        mc_conv_tot_d = 0
    else:
        mc_conv_tot_d = mc_conv_tot_d.values.item(0)
    
    mc_comb['key'] = key
    mc_comb['campaign_code'] = mc_type.campaign_code[i]
    mc_comb['banner'] = mc_type.banner[i]
    
    mc_comb = mc_comb[['key','banner','campaign_code','Channel','Medium','Event','prob_norm_channel']]\
                .merge(mc_stats, on = ['key','banner','campaign_code','Event'], how = 'left').fillna(0)
    mc_comb['attribution_dollar'] = mc_conv_tot_d*mc_comb['prob_norm_channel']
    mc_comb['effiency_measure'] = mc_comb['attribution_dollar']/(mc_comb['paths']+1e-6)
    mc_comb = mc_comb[mc_comb['attribution_dollar'] > 0]
    
    print(mc_comb[['Event','prob_norm_channel']].sort_values(by=['prob_norm_channel'], ascending = False).head())
    
    
    ############### Construct final output ################
    
    if mc_final is None:
        mc_final = mc_comb.sort_values(by = ['prob_norm_channel'], ascending=False).copy()
    else:
        mc_final = mc_final.append(mc_comb.sort_values(by = ['prob_norm_channel'], ascending=False))

        
# final output
mc_final = mc_final[
    ['key', 'banner', 'campaign_code', 'Channel', 'Medium', 'Event', 'volume', 
    'paths', 'paths_conv', 'prob_norm_channel']]
# mc_final.to_csv(f'{gs_dir}/markov_out/{date}/mc_final.csv', index = False, line_terminator='\n')
mc_final.to_csv(f'{gs_dir}/markov_out/latest/mc_final.csv', index = False, line_terminator='\n')


table_id = "wx-bq-poc.digital_attribution_modelling.dacamp_mc_final"
client.query(f'drop table if exists {table_id}')

job_config = bigquery.LoadJobConfig()
job_config.schema = [
    bigquery.SchemaField("key", "STRING", mode="REQUIRED"),
    bigquery.SchemaField("banner", "STRING", mode="REQUIRED"),
    bigquery.SchemaField("campaign_code", "STRING", mode="REQUIRED"),
    bigquery.SchemaField("channel", "STRING", mode="REQUIRED"),
    bigquery.SchemaField("medium", "STRING", mode="REQUIRED"),
    bigquery.SchemaField("event", "STRING", mode="REQUIRED"),
    bigquery.SchemaField("event_volume", "FLOAT64", mode="REQUIRED"),
    bigquery.SchemaField("reach", "FLOAT64", mode="REQUIRED"),
    bigquery.SchemaField("converted_paths", "FLOAT64", mode="REQUIRED"),
    bigquery.SchemaField("prob_norm_channel", "FLOAT64", mode="REQUIRED"),
]

job_config.skip_leading_rows = 1
job_config.source_format = bigquery.SourceFormat.CSV
uri = f'{gs_dir}/markov_out/latest/mc_final.csv'

load_job = client.load_table_from_uri(
    uri, table_id, job_config=job_config
)  # API request
print("Starting job {}".format(load_job.job_id))

load_job.result()  # Waits for table load to complete.
print("Job finished.")

run_bq(bq_dir, 'mc_camp_mcfinal')