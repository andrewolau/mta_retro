import os
import sys
import gcsfs
import pandas as pd
import numpy as np

# append lib dir
# sys.path.append('../lib/')

work_dir = os.getcwd()
sys.path.append(work_dir)
os.chdir(work_dir)
print('working directory: '+work_dir)

from pypelines import redshift
from da_util import *

model_code = 'da'
table_name = 'loyalty_modeling.sy_safari_activation_new'

#process
bq_dir = '../bq_query'
f = open(f'{bq_dir}/base/base_safari_activation.sql', "r")
rx_query = f.read()
f.close()
rx = redshift.connection(credfile)
rx.commit(rx_query,verbose = False)
rx.close()
print('process done')

#unload
rx_query = f"UNLOAD ('select * from {table_name}') \
TO 's3://{s3_bucket}/{s3_dir}/{model_code}/{table_name}/' \
CREDENTIALS 'aws_access_key_id={aws_key};aws_secret_access_key={aws_secret_key}' \
DELIMITER '\t' GZIP \
ESCAPE \
PARALLEL ON \
ALLOWOVERWRITE;"
rx = redshift.connection(credfile)
rx.commit(rx_query,verbose = False)
rx.close()
print('unload done')

# connect to s3
start_con()
base = None
indata_list =  list(get_matching_s3_keys(f'{s3_bucket}', prefix=f'{s3_dir}/{model_code}/{table_name}/', 
                                         suffix=''))
for file in indata_list:
    with fs.open(f's3://{s3_bucket}/{file}') as f:
        table = pd.read_csv(f, compression = 'gzip', header=None, dtype=object, sep='\t')
        if base is None:
            base = table
        else:
            base = pd.concat([base, table], ignore_index=True)
        
base.columns = get_schema(table_name.split('.')[0], table_name.split('.')[1]).name
close_con()
print('save to s3 done')

# save to gs
gs_data_dir = f'gs://wx-prod-hive/datasets/digital_attribution.db/sy_safari_activation_new/sy_safari_activation_new.parquet'
base.to_parquet(gs_data_dir)
print('save to gs done')


query_job = client.query('''drop table if exists `digital_attribution_modelling.sy_safari_activation_new`''')
query_job.result()

dataset_ref = client.dataset('digital_attribution_modelling')
job_config = bigquery.LoadJobConfig()
job_config.source_format = bigquery.SourceFormat.PARQUET

load_job = client.load_table_from_uri(
    gs_data_dir, dataset_ref.table("sy_safari_activation_new"), job_config=job_config
)  # API request
print("Starting job {}".format(load_job.job_id))

load_job.result()  # Waits for table load to complete.
print("Job finished.")



