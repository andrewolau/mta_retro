import sys
import os
import datetime
import pandas as pd
import numpy as np

work_dir = os.getcwd()
sys.path.append(work_dir)
os.chdir(work_dir)
print('working directory: '+work_dir)

from da_func import *

#Gets a needless warning in the Removal Effect section of code so suppressing it
import warnings
warnings.filterwarnings('ignore')

dateTimeObj = datetime.datetime.now()
dateObj = dateTimeObj.date()
date = dateObj.strftime("%Y-%m-%d")
# date = sys.argv[1]

############### INIT ################

simsize = 100000
gs_dir = 'gs://digital-attribution-data/phase1'
channels = pd.read_csv(f'{gs_dir}/markov_input/channel_hierarchy.csv')
mc_type = pd.read_csv(f'{gs_dir}/markov_input/mc_type.csv')


############### BQ ################

bq_dir = 'bq_query/desc'
bq_file = [
    'desc_events',
    'desc_events_daily',
    'desc_conv',
    'desc_conv_daily',
    'desc_dx',
    'desc_dx_daily',
    'desc_journey_pos_act',
    'desc_journey_pos_onl',
    'desc_journey_diff_act',
    'desc_journey_diff_onl',
    'desc_trans_matrix',
    'desc_shopper_id_match_rate',
    'desc_conversion_extrapolation'
]


for bq in bq_file:
    
    df = run_bq(bq_dir, bq)
    df.to_csv(f'{gs_dir}/descriptive_stats/{date}/{bq}.csv', index = False, line_terminator='\n')
    df.to_csv(f'{gs_dir}/descriptive_stats/latest/{bq}.csv', index = False, line_terminator='\n')
    print(f'finished:{bq}')


############### MC ################


bq_dir = 'bq_query/mc'
stats = run_bq(bq_dir, 'mc_stats').sort_values(by = ['banner', 'channel_event']).reset_index(drop = True)\
            .rename(columns={'channel_event':'Event'})
stats_bmp = run_bq(bq_dir, 'mc_stats_bmp').sort_values(by = ['banner', 'bmp_segment', 'channel_event']).reset_index(drop = True)\
            .rename(columns={'channel_event':'Event'})

conv_tot = run_bq(bq_dir, 'mc_conv_tot') # overall level
conv_tot_bmp = run_bq(bq_dir, 'mc_conv_tot_bmp') # overall level

mc_final = None
# mc_type = mc_type.head(2)
# Markov Chain
for i in mc_type.index:
    
    
    ############### Load data ################

    b = mc_type.banner[i]
    c = mc_type.conv_type[i]
    bmp = mc_type.bmp[i]
    
    if (bmp == 'Total'):
        mc_stats = stats[stats['banner']==b].reset_index(drop = True)
        mc_stats['bmp_segment'] = 'Total'
        if (conv_tot.loc[conv_tot.banner==b,c].empty):
            mc_conv_tot = mc_conv_tot_d = 0
        else:
            mc_conv_tot = (conv_tot.loc[conv_tot.banner==b,c].reset_index(drop=True))[0]
            mc_conv_tot_d = pd.to_numeric(conv_tot.loc[conv_tot.banner==b,c+'_spend'].reset_index(drop=True))[0]
    else:
        mc_stats = stats_bmp[(stats_bmp['banner']==b) & (stats_bmp['bmp_segment']==bmp)].reset_index(drop = True)
        if (conv_tot_bmp.loc[(conv_tot_bmp.banner==b) & (conv_tot_bmp.bmp_segment==bmp),c].empty):
            mc_conv_tot = mc_conv_tot_d = 0
        else:
            mc_conv_tot = (conv_tot_bmp.loc[(conv_tot_bmp.banner==b) & (conv_tot_bmp.bmp_segment==bmp),c]\
                                        .reset_index(drop=True))[0]
            mc_conv_tot_d = pd.to_numeric(conv_tot_bmp.loc[(conv_tot_bmp.banner==b) & (conv_tot_bmp.bmp_segment==bmp) ,c+'_spend']\
                                          .reset_index(drop=True))[0]

    print(f'working on {b}_{c}_{bmp}')

    
    ############### Start markov on total level ################
    
    trans = get_data(bq_dir, b, c, bmp)
    sample = path_simulation(trans, simsize)
    mc_channel = removal_effect(sample.copy(), channels['Channel'])[['Event','prob_norm']]\
                    .rename(columns={'Event':'Channel', 'prob_norm':'ratio_channel'})
    mc_medium = removal_effect(sample.copy(), channels['Medium'])[['Event','prob_norm']]\
                    .rename(columns={'Event':'Medium', 'prob_norm':'ratio_medium'})
    mc_event = removal_effect(sample.copy(), channels['Event']).sort_values(by = ['Event']).reset_index(drop = True)
        
    mc_comb = mc_event[['Event','vol','prob_norm']]\
                        .merge(channels, on = 'Event')\
                        .merge(mc_medium[['Medium','ratio_medium']], on = 'Medium')\
                        .merge(mc_channel[['Channel','ratio_channel']], on = 'Channel')
    mc_comb['prob_norm_medium'] = mc_comb['prob_norm']*mc_comb['ratio_medium']/\
                                (mc_comb['prob_norm'].groupby(mc_comb['Medium']).transform('sum') + 1e-6)
    mc_comb['prob_norm_channel'] = mc_comb['prob_norm_medium']*mc_comb['ratio_channel']/\
                                (mc_comb['prob_norm_medium'].groupby(mc_comb['Channel']).transform('sum') + 1e-6)
    
    mc_comb['banner'] = b
    mc_comb['conv_type'] = c
    mc_comb['bmp_segment'] = bmp
    mc_comb = mc_comb[['conv_type','banner','bmp_segment','Channel','Medium','Event','prob_norm_channel']]\
                .merge(mc_stats, on = ['banner','Event','conv_type','bmp_segment'], how = 'left').fillna(0)
    mc_comb['attribution'] = mc_conv_tot*mc_comb['prob_norm_channel']
    mc_comb['attribution_dollar'] = mc_conv_tot_d*mc_comb['prob_norm_channel']
    mc_comb['effiency_measure'] = mc_comb['attribution']/(mc_comb['paths']+1e-6)
    
    
    ############### Construct final output ################
    
    if mc_final is None:
        mc_final = mc_comb.drop(['prob_norm_channel'], axis = 1).copy()
    else:
        mc_final = mc_final.append(mc_comb.drop(['prob_norm_channel'], axis = 1))
    
#     mc_channel.to_csv(f'{gs_dir}/markov_out/{date}/{b}_{c}_mc_channel.csv', index = False, line_terminator='\n')
#     mc_medium.to_csv(f'{gs_dir}/markov_out/{date}/{b}_{c}_mc_medium.csv', index = False, line_terminator='\n')
#     mc_event.to_csv(f'{gs_dir}/markov_out/{date}/{b}_{c}_mc_event.csv', index = False, line_terminator='\n')


# final output
mc_final = mc_final.set_index(['bmp_segment','banner','Channel','Medium','Event','volume','unique_crn','conv_type'])\
                .unstack('conv_type').fillna(0)
mc_final.columns = ['_'.join(col).strip() for col in mc_final.columns.values]
mc_final = mc_final.reset_index()
mc_final['attributed_success'] = mc_final['attribution_activation'] + mc_final['attribution_online']
mc_final = mc_final[
    ['bmp_segment', 'banner', 'Channel', 'Medium', 'Event', 'volume', 'unique_crn', 
    'paths_activation', 'paths_conv_activation', 'attribution_activation', 'attribution_dollar_activation', 'effiency_measure_activation',
    'paths_online', 'paths_conv_online', 'attribution_online', 'attribution_dollar_online', 'effiency_measure_online','attributed_success']
]
mc_final['Channel'] = mc_final['Channel'].str.upper()
mc_final['Medium'] = mc_final['Medium'].str.upper()
mc_final.to_csv(f'{gs_dir}/markov_out/{date}/mc_final.csv', index = False, line_terminator='\n')
mc_final.to_csv(f'{gs_dir}/markov_out/latest/mc_final.csv', index = False, line_terminator='\n')