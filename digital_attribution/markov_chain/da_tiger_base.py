import sys
import os
import datetime
import argparse

work_dir = os.getcwd()
sys.path.append(work_dir)
os.chdir(work_dir)
print('working directory: '+work_dir)

from da_util import *
from da_func import *
from slack import *


if __name__=="__main__":
    
    with SlackBotIfFail('MTA Tiger da_base', 'SYE1', "https://hooks.slack.com/services/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") as slackbot:
        
        parser = argparse.ArgumentParser()
        parser.add_argument('--dry_run', action='store_true')
        args = parser.parse_args()

        bq_dir = '../bq_query/tiger'

        bq_file_run = [
            'eventstore_view',
            'tiger_event',
            'tiger_transition_matrix',
        ]

        for bq in bq_file_run:

            run_bq(bq_dir, bq)
            print(f'finished: {bq}')

        
        slackbot.send_message("Finished")