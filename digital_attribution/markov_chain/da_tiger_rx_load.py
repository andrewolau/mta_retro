import sys
import os
import datetime

work_dir = os.getcwd()
sys.path.append(work_dir)
os.chdir(work_dir)
print('working directory: '+work_dir)


from pypelines import redshift
from da_util import *
from da_func import *

############# tiger #############


model_code = 'dacamp'
table_name = 'loyalty_modeling.sy_TIGER_TRAP_INCSALES'
bq_dir = '../bq_query'
gs_data_dir = f'gs://wx-prod-hive/datasets/digital_attribution.db/sy_TIGER_TRAP_INCSALES/sy_TIGER_TRAP_INCSALES.parquet'

print(f'loading {table_name}')
#process
f = open(f'{bq_dir}/tiger/base_tiger_rx.sql', "r")
rx_query = f.read()
f.close()
rx = redshift.connection(credfile)
rx.commit(rx_query,verbose = False)
rx.close()
print('process done')

#unload
rx_query = f"UNLOAD ('select * from {table_name}') \
TO 's3://{s3_bucket}/{s3_dir}/{model_code}/{table_name}/' \
CREDENTIALS 'aws_access_key_id={aws_key};aws_secret_access_key={aws_secret_key}' \
DELIMITER '\t' GZIP \
ESCAPE \
PARALLEL OFF \
ALLOWOVERWRITE;"
rx = redshift.connection(credfile)
rx.commit(rx_query,verbose = False)
rx.close()
print('unload done')

# connect to s3
start_con()
base = None
indata_list =  list(get_matching_s3_keys(f'{s3_bucket}', prefix=f'{s3_dir}/{model_code}/{table_name}/', 
                                         suffix=''))

print(indata_list)
for file in indata_list:
    with fs.open(f's3://{s3_bucket}/{file}') as f:
        table = pd.read_csv(f, compression = 'gzip', header=None, dtype=object, sep='\t')
        print(table.head())
        if base is None:
            base = table
        else:
            base = pd.concat([base, table], ignore_index=True)
        
base.columns = ['crn', 'uc_crn', 'campaign_code', 'campaign_start_date', 'offer_nbr', 'redeem_cnt', 'tpg_inc_avg']
close_con()
print('save to s3 done')

# save to gs
base.to_parquet(gs_data_dir)
print('save to gs done')


query_job = client.query('''drop table if exists `digital_attribution_modelling.sy_TIGER_TRAP_INCSALES`''')
query_job.result()

dataset_ref = client.dataset('digital_attribution_modelling')
job_config = bigquery.LoadJobConfig()
job_config.source_format = bigquery.SourceFormat.PARQUET

load_job = client.load_table_from_uri(
    gs_data_dir, dataset_ref.table("sy_TIGER_TRAP_INCSALES"), job_config=job_config
)  # API request
print("Starting job {}".format(load_job.job_id))

load_job.result()  # Waits for table load to complete.
print("Job finished.")

