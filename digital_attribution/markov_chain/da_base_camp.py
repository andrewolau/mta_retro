import sys
import os
import datetime
import argparse

work_dir = os.getcwd()
sys.path.append(work_dir)
os.chdir(work_dir)
print('working directory: '+work_dir)

from da_util import *
from da_func import *
from slack import *


if __name__=="__main__":
    
#     with SlackBotIfFail('MTA da_base', 'SYE1', "https://hooks.slack.com/services/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") as slackbot:
        
#     parser = argparse.ArgumentParser()
#     parser.add_argument('--dry_run', action='store_true')
#     args = parser.parse_args()

    bq_dir = '../bq_query'
    
    version_type=sys.argv[1]
    date_til = sys.argv[2]
    date_til2=date_til.replace('-','')
    
    bq_file_mod = [ 
        'base/ws_current',
        'base/camp_transition_matrix',
        'base/base_safari_activation_bq',
        'base/camp_event',
        'base/camp_user_journey'
    ]

    for bq in bq_file_mod:

        f = open(f'{bq_dir}/{bq}_raw.sql', "r")
        q = f.read()
        f.close()

        if len(sys.argv) == 1:
            print('Error: Checking query ',bq,'_raw.sql')
#             q = q.replace('_FW_START_DATE_', f'\'{wed.strftime("%Y-%m-%d")}\'')
        else:
            q = q.replace('_FW_START_DATE_', f'\'{date_til}\'')
            q = q.replace('_FWSTARTDATE_', f'{date_til2}')
            q = q.replace('_VersionType_', f'{version_type}')

#             q = q.replace('_FW_START_DATE_', f'\'{date_til}\'').replace('_FWSTARTDATE_', f'\'{wed.strftime("%Y%m%d")}\'')

        fw = open(f'{bq_dir}/{bq}_{version_type}.sql', "w+")
        fw.write(q)
        fw.close()
        print(f'date uppdated:{bq}')


    file_path = f'./output/'
    directory = os.path.dirname(file_path)
    print(directory)
    
    if not os.path.exists(directory):
        os.mkdir(directory)        
    ### the full history tables will not created again and they are:
    ### 'base/eventstore_view'+version_type,
    bq_file_run = [
        'base/ws_current'+'_'+version_type,
        'base/base_safari_activation_bq'+'_'+version_type,
        'base/camp_event'+'_'+version_type,
        'base/camp_user_journey'+'_'+version_type,
        'base/camp_transition_matrix'+'_'+version_type
    ]

    for bq in bq_file_run:

        run_bq(bq_dir, bq)
        print(f'finished: {bq}.sql')

     
    

####         slackbot.send_message("Finished")