SELECT *
FROM
(
	with base as
	(
		SELECT concat(a.crn, cast(ref_dt as string)) as key, a.crn, ref_dt, time_utc, a.banner, channel_event, activation_time, 
		case when activation_flag is null then 0 else 1 end as activation_flag,
		case when bmp_segment is null then 'None' else bmp_segment end as bmp_segment
		FROM
		(
			SELECT crn, ref_dt, banner, min(time_utc) as time_utc, channel_event
			FROM
			(
				SELECT crn, ref_dt, time_utc, banner,
					case 
					when channel_event like 'dx_view%' then 'dx_view' 
					when channel_event like 'dx_clk%' then 'dx_clk'
					else channel_event end as channel_event
				FROM `wx-bq-poc.digital_attribution_modelling.da_event_mw`
			)
			group by 1,2,3,5
		) a 

		LEFT JOIN
		(
			SELECT crn, date_utc, banner, min(time_utc) as activation_time, 1 as activation_flag
			FROM `wx-bq-poc.digital_attribution_modelling.da_activation`
			group by 1,2,3
		) b on a.crn = b.crn and a.ref_dt = b.date_utc and a.banner = b.banner
		
		left join
		(
			SELECT distinct crn, bmp_segment
			FROM `wx-bq-poc.digital_attribution_modelling.da_bmp_segment` 
		) c on a.crn = c.crn
		
		where time_utc < activation_time or activation_time is null
	)

	SELECT 'activation' as conv_type, a.*, b.paths, 
		case when c.paths_conv is null then 0 else c.paths_conv end as paths_conv 
	FROM
	(
		SELECT banner, bmp_segment, channel_event, count(*) as volume, count(distinct crn) as unique_crn  
		FROM 
		(
			SELECT a.crn, banner, date_utc, 
				case 
					when channel_event like 'dx_view%' then 'dx_view' 
					when channel_event like 'dx_clk%' then 'dx_clk'
					else channel_event end as channel_event,
				case when bmp_segment is null then 'None' else bmp_segment end as bmp_segment
			FROM `wx-bq-poc.digital_attribution_modelling.da_event` a
			left join 
			(
				SELECT distinct crn, bmp_segment
				FROM `wx-bq-poc.digital_attribution_modelling.da_bmp_segment` 
			) b on a.crn = b.crn
		)
		group by 1,2,3
	) a 
	
	inner join
	
	(
		SELECT banner, channel_event, bmp_segment, count(distinct key) as paths 
		from base 
		group by 1,2,3
	) b on a.banner = b.banner and a.channel_event = b.channel_event and a.bmp_segment = b.bmp_segment
	
	left join
	
	(
		SELECT banner, channel_event, bmp_segment, count(distinct key) as paths_conv 
		from base 
		where activation_flag = 1 
		group by 1,2,3
	) c on a.banner = c.banner and a.channel_event = c.channel_event and a.bmp_segment = c.bmp_segment

) 

UNION ALL 

SELECT * 
FROM
(
	with base as
	(
		SELECT concat(a.crn, cast(ref_dt as string)) as key, a.crn, ref_dt, time_utc, a.banner, channel_event, 
		case when online_flag is null then 0 else 1 end as online_flag,
		case when bmp_segment is null then 'None' else bmp_segment end as bmp_segment
		FROM
		(
			SELECT crn, ref_dt, banner, min(time_utc) as time_utc, channel_event
			FROM
			(
				SELECT crn, ref_dt, time_utc, banner,
					case 
					when channel_event like 'dx_view%' then 'dx_view' 
					when channel_event like 'dx_clk%' then 'dx_clk'
					else channel_event end as channel_event
				FROM `wx-bq-poc.digital_attribution_modelling.da_event_mw`
			)
			group by 1,2,3,5
		) a 

		LEFT JOIN
		(
			SELECT crn, date_utc, 'supermarkets' as banner, min(time_utc) as online_time, 1 as online_flag
			FROM `wx-bq-poc.digital_attribution_modelling.da_online_sales`
			group by 1,2,3
		) b on a.crn = b.crn and a.ref_dt = b.date_utc and a.banner = b.banner
		
		left join
		(
			SELECT distinct crn, bmp_segment
			FROM `wx-bq-poc.digital_attribution_modelling.da_bmp_segment` 
		) c on a.crn = c.crn
		
		where time_utc < online_time or online_time is null
	)

	SELECT 'online' as conv_type, a.*, b.paths, 
		case when c.paths_conv is null then 0 else c.paths_conv end as paths_conv 
	FROM
	(
		SELECT banner, bmp_segment, channel_event, count(*) as volume, count(distinct crn) as unique_crn  
		FROM 
		(
			SELECT a.crn, banner, date_utc, 
			case 
				when channel_event like 'dx_view%' then 'dx_view' 
				when channel_event like 'dx_clk%' then 'dx_clk'
				else channel_event end as channel_event,
			case when bmp_segment is null then 'None' else bmp_segment end as bmp_segment
			FROM `wx-bq-poc.digital_attribution_modelling.da_event` a
			left join 
			(
				SELECT distinct crn, bmp_segment
				FROM `wx-bq-poc.digital_attribution_modelling.da_bmp_segment` 
			) b on a.crn = b.crn
		)
		group by 1,2,3
	) a 
	
	inner join
	
	(
		SELECT banner, channel_event, bmp_segment, count(distinct key) as paths 
		from base 
		group by 1,2,3
	) b on a.banner = b.banner and a.channel_event = b.channel_event and a.bmp_segment = b.bmp_segment
	
	left join
	
	(
		SELECT banner, channel_event, bmp_segment, count(distinct key) as paths_conv 
		from base 
		where online_flag = 1 
		group by 1,2,3
	) c on a.banner = c.banner and a.channel_event = c.channel_event and a.bmp_segment = c.bmp_segment
	
)



