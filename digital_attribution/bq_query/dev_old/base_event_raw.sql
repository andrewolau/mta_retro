--------------------------------------------DA EVENTS----------------------------------------------------

--- online sales ---

DROP TABLE IF EXISTS `wx-bq-poc.digital_attribution_modelling.da_online_sales`;
create table `wx-bq-poc.digital_attribution_modelling.da_online_sales` as
(
	select crn, start_txn_time as time_utc, Date(start_txn_time) as date_utc, sum(tot_amt_incld_gst-ass.tot_wow_dollar_incld_gst) as spend
	from
	(
		select lylty_card_nbr, start_txn_time, division_nbr, prod_nbr, tot_amt_incld_gst, tot_wow_dollar_incld_gst 
		from wx_lty_digital_attribution_dev.redx_loyalty_article_sales_summary 
		where division_nbr in (1005,1030) and checkout_nbr=100 and void_flag <> 'Y'
	) ass
   inner join 
   (
		select prod_nbr, division_nbr 
        from 
		(
			select *, rank() over (partition by prod_nbr, division_nbr order by last_update_date desc) as date_rank 
			from `wx-bq-poc.wx_lty_digital_attribution_dev.redx_loyalty_article_master`
		)
		where date_rank=1
	) am on ass.prod_nbr=am.prod_nbr and ass.division_nbr=am.division_nbr
   inner join 
   (
		select lylty_card_nbr,crn 
        from 
		(
			select *,rank() over (partition by crn order by last_update_date desc) as date_rank 
            from `wx-bq-poc.wx_lty_digital_attribution_dev.redx_loyalty_lylty_card_detail`
		)
		where date_rank=1 and lylty_card_status=1 and crn is not NULL and crn <> '0'
	) as lcd on ass.lylty_card_nbr=lcd.lylty_card_nbr
	where Date(start_txn_time) BETWEEN DATE_SUB(Date(DATE_TO_), INTERVAL 27 DAY) AND Date(DATE_TO_)
	group by 1,2
);




--- activation sales ---
DROP TABLE IF EXISTS `wx-bq-poc.digital_attribution_modelling.da_activation`;
create table `wx-bq-poc.digital_attribution_modelling.da_activation` as
(
	SELECT * 
	FROM
	(
		SELECT a.crn, campaign_code, campaign_type, a.banner, campaign_start_date, time_utc, Date(time_utc) as date_utc, 
		case when sum(activation_spend) is null then 0 else sum(activation_spend) end as spend 
		FROM
		(
			SELECT crn, banner, campaign_code, 
				cast(campaign_start_date as Date) as campaign_start_date, 
				DATE_ADD(cast(campaign_start_date as Date), INTERVAL cast(camp_dur_days as INT64) DAY) as campaign_end_date,
				'activate' as campaign_type, 
				min(CAST(time_utc as Timestamp)) as time_utc
			FROM `wx-bq-poc.digital_attribution_modelling.sy_safari_activation_new`
			where action_type = 'ACTIVATE'
			group by 1,2,3,4,5
		) a 
		left join
		(
			select crn, banner, start_txn_time, sum(tot_amt_incld_gst-ass.tot_wow_dollar_incld_gst) as activation_spend
			from 
			(
				select lylty_card_nbr, start_txn_time, prod_nbr, tot_amt_incld_gst, tot_wow_dollar_incld_gst, division_nbr,
				case 
				when division_nbr in (1005, 1030) then 'supermarkets'
				when division_nbr = 1010 then 'bws'
				when division_nbr = 1060 then 'bigw'
				when division_nbr = 1021 then 'caltex'
				when division_nbr = 1020 then 'fuelco'
				else 'other' end as banner
				from wx_lty_digital_attribution_dev.redx_loyalty_article_sales_summary 
				where division_nbr in (1005,1010,1030,1021,1020,1060) and void_flag <> 'Y'
			) ass
			inner join
			(
				select prod_nbr, division_nbr 
				from 
				(
					select *,rank() over (partition by prod_nbr, division_nbr order by last_update_date desc) as date_rank 
					from `wx-bq-poc.wx_lty_digital_attribution_dev.redx_loyalty_article_master`
				)
				where date_rank=1
			) am on ass.prod_nbr=am.prod_nbr and ass.division_nbr=am.division_nbr
			inner join 
			(
				select lylty_card_nbr,crn 
				from 
				(
					select *,rank() over (partition by crn order by last_update_date desc) as date_rank 
					from `wx-bq-poc.wx_lty_digital_attribution_dev.redx_loyalty_lylty_card_detail` 
					where lylty_card_status=1 and crn is not NULL and crn <> '0'
				)
				where date_rank=1
			) as lcd on ass.lylty_card_nbr=lcd.lylty_card_nbr
			group by 1,2,3
		) b on a.crn = b.crn and a.campaign_start_date <= Date(b.start_txn_time) and a.campaign_end_date >= Date(b.start_txn_time) and a.banner = b.banner
		group by 1,2,3,4,5,6,7

		union all

		SELECT a.crn, campaign_code, campaign_type, banner, campaign_start_date, min(time_utc) as time_utc, Date(min(time_utc)) as date_utc, 
		case when sum(purchase_spend) is null then 0 else sum(purchase_spend) end as spend
		FROM
		(
			SELECT distinct crn, banner, campaign_code, cast(campaign_start_date as Date) as campaign_start_date, 'non-activate' as campaign_type, CAST(time_utc as Timestamp) as time_utc, basket_key
			FROM `wx-bq-poc.digital_attribution_modelling.sy_safari_activation_new`
			where action_type = 'PURCHASE'
		) a 
		left join
		(
			SELECT basket_key, 
			sum(tot_amt_incld_gst - tot_wow_dollar_incld_gst) as purchase_spend
			from 
			(
				SELECT basket_key, prod_nbr, tot_amt_incld_gst, tot_wow_dollar_incld_gst, division_nbr
				FROM wx_lty_digital_attribution_dev.redx_loyalty_article_sales_summary
				where void_flag <> 'Y'
			) ass 
			inner join
			(
				select prod_nbr, division_nbr 
				from 
				(
					select *,rank() over (partition by prod_nbr, division_nbr order by last_update_date desc) as date_rank 
					from `wx-bq-poc.wx_lty_digital_attribution_dev.redx_loyalty_article_master`
				)
				where date_rank=1
			) am on ass.prod_nbr=am.prod_nbr and ass.division_nbr=am.division_nbr
			group by 1
		) b on a.basket_key = b.basket_key
		group by 1,2,3,4,5
	)
	where date_utc BETWEEN DATE_SUB(Date(DATE_TO_), INTERVAL 27 DAY) AND Date(DATE_TO_) and banner <> 'other' 
);


--- timestamped campaign level event ---

DROP TABLE IF EXISTS `wx-bq-poc.digital_attribution_modelling.da_event`;
CREATE table IF NOT EXISTS `wx-bq-poc.digital_attribution_modelling.da_event` as 
(
	SELECT * FROM
	(
		SELECT distinct crn, time_utc, Date(time_utc) as date_utc, banner, channel, 
		case 
		when event_name = 'email open' then 'email_open'
		when event_name = 'email click' then 'email_clk'
		when channel = 'programmatic image' AND event_name = "google impression" then 'prog_img_imp'
		when channel = 'programmatic image' AND event_name = "google click" then 'prog_img_clk'
		when channel = 'programmatic video' AND event_name = "google impression" then 'prog_vdo_imp'
		when channel = 'programmatic video' AND event_name = "google click" then 'prog_vdo_clk'
		when channel = 'youtube' AND event_name = "google impression" then 'yt_imp'
		when channel = 'youtube' AND event_name = "google click" then 'yt_clk'
		when channel = 'gmail' AND event_name = "google impression" then 'gmail_imp'
		when channel = 'gmail' AND event_name = "google click" then 'gmail_clk'
		when event_name = 'google click' AND channel = 'sem' then CONCAT("sem_", REPLACE(json_extract(attributes, "$.paid_search_type[0]"), "\"", ""), "_clk")
		when event_name = "google impression" then "other_google_imp"
		when event_name = "google click" then "other_google_clk"
		else 'other' end as channel_event, 
		CASE
		when rw_campaign_code IS NOT NULL THEN rw_campaign_code
		when gx_campaign_code IS NOT NULL THEN gx_campaign_code
		else '' end as campaign_code
		FROM 
		(
			SELECT *, 
				REPLACE(json_extract(attributes, "$.campaign_code[0]"), "\"", "") as rw_campaign_code, 
				REPLACE(json_extract(attributes, "$.campaign_id[0]"), "\"", "") as campaign_id 
				FROM `wx-bq-poc.wx_lty_digital_attribution_dev.eventstore_events`
			where channel <> 'dx'
		) a 
		LEFT JOIN 
		(
			SELECT campaign_id, UPPER(max(campaign_code)) as gx_campaign_code
			FROM
			(
				SELECT distinct campaign_id, 
					CASE WHEN ARRAY_LENGTH(REGEXP_EXTRACT_ALL(campaign, r"\D\D\D-\d\d\d\d")) >0 THEN REGEXP_EXTRACT_ALL(campaign, r"\D\D\D-\d\d\d\d")[OFFSET(0)] else null end as campaign_code 
				FROM `wx-bq-poc.wx_lty_digital_attribution_dev.googlex_campaigns`
			)
			where campaign_code IS NOT NULL 
			group by 1
		) b on a.campaign_id = b.campaign_id

		union all

		SELECT distinct crn, time_utc, Date(time_utc) as date_utc, banner, channel, channel_event, '' as campaign_code
		FROM
		(
			SELECT *, rank() over (partition by crn, json_extract(attributes, "$.udo_tealium_session_id[0]"), channel_event order by time_utc asc) as event_rank
			FROM 
			(
				SELECT *,
					Case	
					when channel = 'dx' AND event_name in ('page view', 'search view') then CONCAT('dx_view'," - ",lower(REPLACE(json_extract(attributes, "$.referrer_source[0]"), "\"", "")))
					else CONCAT('dx_clk'," - ",lower(REPLACE(json_extract(attributes, "$.asset_type[0]"), "\"", ""))) end as channel_event
				FROM `wx-bq-poc.wx_lty_digital_attribution_dev.eventstore_events` 
				where channel = 'dx' and event_name in ('page view', 'search view', 'click view')
			)
		)
		where event_rank = 1
	)
	where banner in ('supermarkets','bws','bigw','fuelco','caltex') and channel_event <> 'other' and date_utc BETWEEN DATE_SUB(DATE_SUB(Date(DATE_TO_), INTERVAL 27 DAY), INTERVAL 14 DAY) AND Date(DATE_TO_)
);




------------------------------------DX----------------------------------------------------

--- Deduped DX events unique dx_view per referrer source and dx_clk per asset type ---

DROP TABLE IF EXISTS `wx-bq-poc.digital_attribution_modelling.da_dx_event`;
CREATE table IF NOT EXISTS `wx-bq-poc.digital_attribution_modelling.da_dx_event` as 
(
	SELECT crn, 
		CASE WHEN channel_event like 'dx_view%' then 'dx_view'
		ELSE 'dx_clk' end as dx_event, 
	channel_event, 
	time_utc, 
	date_utc
	FROM `wx-bq-poc.digital_attribution_modelling.da_event`
	where banner = 'supermarkets' and channel = 'dx' and channel_event <> 'dx_view - other'
);


--- Left join table (to da_dx_event) of labels for DX Bayesian ---

DROP TABLE IF EXISTS `wx-bq-poc.digital_attribution_modelling.da_dx_label`;
create table `wx-bq-poc.digital_attribution_modelling.da_dx_label` as
(
	SELECT crn, time_utc, date_utc, 
	max(banner_hp_wx) as banner_hp_wx,
	max(tile_hp_wx) as tile_hp_wx,
	max(contentcard_inshop_vf) as contentcard_inshop_vf,
	max(contantcard_inshop_search_wx) as contantcard_inshop_search_wx,
	max(banner_inshop_search_vf) as banner_inshop_search_vf,
	max(digital_catelogue) as digital_catelogue,
	max(contentcard_overlay_vf) as contentcard_overlay_vf,
	max(contentcard_overlay_wx) as contentcard_overlay_wx,
	max(carousel_hp) as carousel_hp,
	max(other_clk_wx_vf) as other_clk_wx_vf,
	max(online_flag) as online_flag,
	max(online_spend) as online_spend
	FROM 
	(
		(
			SELECT crn, time_utc, Date(time_utc) as date_utc,
			case when channel_event in ('dx_clk - banner - home page bottom - wx', 'dx_clk - banner - home page top - wx', 'dx_clk - banner - home page middle - wx') then 1 else 0 end as banner_hp_wx,
			case when channel_event in ('dx_clk - tile - home page recipe - wx', 'dx_clk - tile - home page - wx') then 1 else 0 end as tile_hp_wx,
			case when channel_event in ('dx_clk - content card - in shop - vf') then 1 else 0 end as contentcard_inshop_vf,
			case when channel_event in ('dx_clk - content card - in shop/search - wx') then 1 else 0 end as contantcard_inshop_search_wx,
			case when channel_event in ('dx_clk - banner - in shop/search - vf') then 1 else 0 end as banner_inshop_search_vf,
			case when channel_event in ('dx_clk - digital catalogue') then 1 else 0 end as digital_catelogue,
			case when channel_event in ('dx_clk - content card - overlay - vf') then 1 else 0 end as contentcard_overlay_vf,
			case when channel_event in ('dx_clk - content card - overlay - wx') then 1 else 0 end as contentcard_overlay_wx,
			case when channel_event in ('dx_clk - carousel - home page - wx/vf') then 1 else 0 end as carousel_hp,
			case when channel_event in ('dx_clk - other - wx/vf') then 1 else 0 end as other_clk_wx_vf,
			0 as online_flag,
			0 as online_spend
			from `wx-bq-poc.digital_attribution_modelling.da_dx_event`
			where dx_event = 'dx_clk'
		)
		
		union all

		(
			SELECT crn, time_utc, date_utc,
			0 as banner_hp_wx,
			0 as tile_hp_wx,
			0 as contentcard_inshop_vf,
			0 as contantcard_inshop_search_wx,
			0 as banner_inshop_search_vf,
			0 as digital_catelogue,
			0 as contentcard_overlay_vf,
			0 as contentcard_overlay_wx,
			0 as carousel_hp,
			0 as other_clk_wx_vf,
			0 as online_flag, 
			0 as online_spend
			from `wx-bq-poc.digital_attribution_modelling.da_dx_event`
			where dx_event = 'dx_view'
		)

		union all

		(
			SELECT crn, time_utc, date_utc,
			0 as banner_hp_wx,
			0 as tile_hp_wx,
			0 as contentcard_inshop_vf,
			0 as contantcard_inshop_search_wx,
			0 as banner_inshop_search_vf,
			0 as digital_catelogue,
			0 as contentcard_overlay_vf,
			0 as contentcard_overlay_wx,
			0 as carousel_hp,
			0 as other_clk_wx_vf,
			1 as online_flag, spend as online_spend 
			from `wx-bq-poc.digital_attribution_modelling.da_online_sales`
			where crn in (select distinct crn from `wx-bq-poc.digital_attribution_modelling.da_dx_event`)	
		)
	)
	group by 1,2,3
);




