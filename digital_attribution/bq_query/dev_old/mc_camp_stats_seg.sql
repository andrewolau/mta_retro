SELECT *, concat(banner, '_', campaign_code, '_', cast(campaign_start_date as string), '_', cast(campaign_end_date as string)) as key 
FROM
(
	with base as
	(
		SELECT concat(a.crn, a.campaign_code, cast(a.campaign_start_date as string), cast(a.campaign_end_date as string)) as key, 
		a.crn, a.banner, a.campaign_code, a.campaign_start_date, a.campaign_end_date, time_utc, channel_event, activation_time, case when activation_flag is null then 0 else 1 end as activation_flag,
		case when segment is null then 'None' else segment end as segment
		FROM
		(
			SELECT * FROM `wx-bq-poc.digital_attribution_modelling.dacamp_event_mw`
		) a 

		LEFT JOIN
		(
			SELECT crn, banner, campaign_code, campaign_start_date, campaign_end_date, min(time_utc) as activation_time, 1 as activation_flag, sum(spend) as activation_spend
			FROM `wx-bq-poc.digital_attribution_modelling.dacamp_activation`
			group by 1,2,3,4,5	
		) b on a.crn = b.crn and a.banner = b.banner and a.campaign_code = b.campaign_code and a.campaign_start_date = b.campaign_start_date and a.campaign_end_date = b.campaign_end_date
		
		left join
		(
			SELECT distinct crn, segment
			FROM `wx-bq-poc.digital_attribution_modelling.da_segment` 
		) c on a.crn = c.crn
		
		where time_utc < activation_time or activation_time is null
	)

	SELECT 'activation' as conv_type, a.*, b.paths, 
		case when c.paths_conv is null then 0 else c.paths_conv end as paths_conv 
	FROM
	(
		(
			SELECT banner, campaign_code, campaign_start_date, campaign_end_date, segment, channel_event, count(*) as volume, count(distinct crn) as unique_crn 
			FROM 
			(
				SELECT a.*,
					case when segment is null then 'None' else segment end as segment
				FROM  `wx-bq-poc.digital_attribution_modelling.dacamp_event_mw` a
				left join 
				(
					SELECT distinct crn, segment
					FROM `wx-bq-poc.digital_attribution_modelling.da_segment` 
				) b on a.crn = b.crn
			)
			group by 1,2,3,4,5,6
		) a 
	
		inner join
		(
			SELECT banner, campaign_code, campaign_start_date, campaign_end_date, segment, channel_event, count(distinct key) as paths 
			from base 
			group by 1,2,3,4,5,6
		) b on a.banner = b.banner and a.campaign_code = b.campaign_code and a.campaign_start_date = b.campaign_start_date and a.campaign_end_date = b.campaign_end_date and a.segment = b.segment and a.channel_event = b.channel_event
	
		left join
		(
			SELECT banner, campaign_code, campaign_start_date, campaign_end_date, segment, channel_event, count(distinct key) as paths_conv 
			from base 
			where activation_flag = 1 
			group by 1,2,3,4,5,6
		) c on a.banner = c.banner and a.campaign_code = c.campaign_code and a.campaign_start_date = c.campaign_start_date and a.campaign_end_date = c.campaign_end_date and a.segment = c.segment and a.channel_event = c.channel_event
	)	
) 
