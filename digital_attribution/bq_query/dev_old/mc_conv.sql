SELECT * FROM
(
SELECT 'activation' as conv_type, a.crn, a.ref_dt, a.banner, channel_event, case when activation_spend is null then 0 else activation_spend end as spend
		FROM
		(
			SELECT crn, banner, ref_dt, min(time_utc) as time_utc, channel_event
			FROM
			(
				SELECT crn, ref_dt, time_utc, banner,
					case 
					when channel_event like 'dx_view%' then 'dx_view' 
					when channel_event like 'dx_clk%' then 'dx_clk'
					else channel_event end as channel_event
				FROM `wx-bq-poc.digital_attribution_modelling.da_event_7mw`
			)
			group by 1,2,3,5
		) a 

		INNER JOIN

		(
			SELECT crn, banner, date_utc, min(time_utc) as activation_time, 1 as activation_flag, sum(spend) as activation_spend
			FROM `wx-bq-poc.digital_attribution_modelling.da_activation`
			group by 1,2,3
		) b on a.crn = b.crn and a.ref_dt = b.date_utc and a.banner = b.banner
		where time_utc < activation_time or activation_time is null
)
union all
(
SELECT 'online' as conv_type, a.crn, a.ref_dt, a.banner, channel_event, case when online_spend is null then 0 else online_spend end as spend
		FROM
		(
			SELECT crn, banner, ref_dt, min(time_utc) as time_utc, channel_event
			FROM
			(
				SELECT crn, ref_dt, time_utc, banner,
					case 
					when channel_event like 'dx_view%' then 'dx_view' 
					when channel_event like 'dx_clk%' then 'dx_clk'
					else channel_event end as channel_event
				FROM `wx-bq-poc.digital_attribution_modelling.da_event_7mw`
			)
			group by 1,2,3,5
		) a 

		INNER JOIN

		(
			SELECT crn, 'supermarkets' as banner, date_utc, min(time_utc) as online_spend_time, 1 as online_flag, sum(spend) as online_spend
			FROM `wx-bq-poc.digital_attribution_modelling.da_online_sales`
			group by 1,2,3
		) b on a.crn = b.crn and a.ref_dt = b.date_utc and a.banner = b.banner
		where time_utc < online_spend_time or online_spend_time is null
)
