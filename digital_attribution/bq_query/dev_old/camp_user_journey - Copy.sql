
--------------------------------------------USER JOURNEY----------------------------------------------------

Drop table if exists `wx-bq-poc.digital_attribution_modelling.dacamp_prod_event_mw`;
Create table `wx-bq-poc.digital_attribution_modelling.dacamp_prod_event_mw` as 
(
	
	with base_onl as 
	(
		SELECT a.*
				, lead(a.time_utc) over (partition by a.crn, a.banner, a.campaign_code, a.campaign_start_date, a.campaign_end_date order by a.time_utc desc) as time_utc_pre
				, lead(CONCAT(a.campaign_code, '_', a.channel_event)) over (partition by a.crn, a.banner, a.campaign_code, a.campaign_start_date, a.campaign_end_date order by a.time_utc desc) as channel_event_pre
		FROM 
		(
		SELECT a.crn
				, a.date
				, a.time_utc
				, a.banner
				, a.campaign_code
				, a.campaign_start_date
				, a.campaign_end_date
				, CONCAT(a.campaign_code, '_', a.channel_event) as channel_event
				, a.campaign_code as cc_event
				, 1 as conv_flag
				, Date(max(b.time_utc)) as end_date 
				, max(b.time_utc) as conv_time
				, sum(b.spend) as spend

		FROM
		(
			SELECT *, min(time_utc) OVER (PARTITION BY banner, crn, campaign_code, campaign_start_date) as min_time_utc
			FROM
			(
			SELECT distinct banner, crn, min(time_utc) as time_utc, min(date) as date, campaign_code, campaign_start_date, campaign_end_date, channel_event
			from `wx-bq-poc.digital_attribution_modelling.dacamp_prod_event`
			group by 1,2,5,6,7,8
			)
		) a

		inner join 
		(
			select crn, time_utc, date
				, campaign_code
				, campaign_start_date
				, sum(spend) as spend
			from `wx-bq-poc.digital_attribution_modelling.dacamp_prod_sales`
			group by 1,2,3,4,5
		) b on a.crn = b.crn and a.min_time_utc < b.time_utc and b.date <= a.campaign_end_date and a.campaign_code = b.campaign_code and a.campaign_start_date = b.campaign_start_date
		
		group by 1,2,3,4,5,6,7,8,9,10
		) a
		
	
	)
	
	SELECT banner, campaign_code, 'TTL' as campaign_type, campaign_start_date, campaign_end_date, crn, time_utc, date, channel_event, conv_time, conv_flag, spend
	from
	(
		
		select * 
		from base_onl 
		where ((regexp_contains(channel_event, r'(?i)clk') and DATE_add(date, INTERVAL 7 DAY) >= end_date and ((conv_flag is null and date <= end_date) or (conv_flag = 1 and time_utc < conv_time)))
				or (not regexp_contains(channel_event, r'(?i)clk') and DATE_add(date, INTERVAL 1 DAY) >= end_date) and ((conv_flag is null and date <= end_date) or (conv_flag = 1 and time_utc < conv_time)))
			   and (timestamp_diff(time_utc, time_utc_pre, SECOND) >= 600
				or time_utc_pre is null
				or channel_event_pre <> channel_event)
	)	
);


