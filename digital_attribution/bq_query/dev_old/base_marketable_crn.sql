---- safari activation ----
drop table if exists loyalty_modeling.sy_marketable_crn;

create table loyalty_modeling.sy_marketable_crn as
(
	select crn, edm_marketable_flag as marketable
	from loyalty_bi_analytics.saf_edm_marketable_snapshot
	where fw_start_date = (select max(fw_start_date) from loyalty_bi_analytics.saf_edm_marketable_snapshot)
);
	
	