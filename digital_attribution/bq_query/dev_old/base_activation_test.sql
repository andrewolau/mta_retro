SELECT a.banner, count(distinct concat(a.crn, cast(ref_dt as string)))
		FROM
		(
			SELECT crn, banner, ref_dt, min(time_utc) as time_utc, channel_event
			FROM
			(
				SELECT crn, ref_dt, time_utc, banner,
					case 
					when channel_event like 'dx_view%' then 'dx_view' 
					when channel_event like 'dx_clk%' then 'dx_clk'
					else channel_event end as channel_event
				FROM `wx-bq-poc.digital_attribution_modelling.da_event_7mw`
			)
			group by 1,2,3,5
		) a 

		INNER JOIN

		(
			SELECT crn, banner, date_utc, min(time_utc) as activation_time, 1 as activation_flag, sum(spend) as activation_spend
			FROM `wx-bq-poc.digital_attribution_modelling.da_activation`
			group by 1,2,3
		) b on a.crn = b.crn and a.ref_dt = b.date_utc
		where time_utc < activation_time or activation_time is null
group by 1



-- all activation
SELECT banner, count(*) from
		(
			SELECT crn, banner, date_utc, min(time_utc) as activation_time, 1 as activation_flag, sum(spend) as activation_spend
			FROM `wx-bq-poc.digital_attribution_modelling.da_activation`
			group by 1,2,3
		)
group by 1

1	
supermarkets
2827406
2	
bigw
108043
3	
bws
127173
4	
fuelco
15594
5	
caltex
7815

--- activation with events
SELECT a.banner, count(*) from
		(
			SELECT crn, banner, date_utc, min(time_utc) as activation_time, 1 as activation_flag, sum(spend) as activation_spend
			FROM `wx-bq-poc.digital_attribution_modelling.da_activation`
			group by 1,2,3
		) a
    inner join
    (
      SELECT distinct crn, banner from `wx-bq-poc.digital_attribution_modelling.da_event`
    ) b on a.crn = b.crn and a.banner = b.banner
group by 1

1	
supermarkets
2326108
2	
bws
77854
3	
caltex
5309
4	
bigw
53725


SELECT a.banner, count(*) from
		(
			SELECT crn, banner, date_utc, min(time_utc) as activation_time, 1 as activation_flag, sum(spend) as activation_spend
			FROM `wx-bq-poc.digital_attribution_modelling.da_activation`
			group by 1,2,3
		) a
    inner join
    (
      SELECT distinct crn, banner from `wx-bq-poc.digital_attribution_modelling.da_event_7mw`
    ) b on a.crn = b.crn and a.banner = b.banner
group by 1

1	
supermarkets
2266699
2	
bws
75185
3	
bigw
53725
4	
caltex
5309



SELECT a.banner, count(*) from
		(
			SELECT crn, banner, date_utc, min(time_utc) as activation_time, 1 as activation_flag, sum(spend) as activation_spend
			FROM `wx-bq-poc.digital_attribution_modelling.da_activation`
			group by 1,2,3
		) a
    inner join
    (
      SELECT distinct crn, banner, ref_dt from `wx-bq-poc.digital_attribution_modelling.da_event_7mw`
    ) b on a.crn = b.crn and a.banner = b.banner and a.date_utc = b.ref_dt
group by 1

1	
supermarkets
1141606
2	
bws
41490
3	
bigw
41124
4	
caltex
5271


SELECT a.banner, count(*) from
		(
			SELECT crn, banner, date_utc, min(time_utc) as activation_time, 1 as activation_flag, sum(spend) as activation_spend
			FROM `wx-bq-poc.digital_attribution_modelling.da_activation`
			group by 1,2,3
		) a
    inner join
    (
      SELECT distinct crn, banner, ref_dt from `wx-bq-poc.digital_attribution_modelling.da_event_30mw`
    ) b on a.crn = b.crn and a.banner = b.banner and a.date_utc = b.ref_dt
group by 1

1	
supermarkets
1821552 (80%)
2	
bws
59432
3	
bigw
45021
4	
caltex
5309


SELECT a.banner, count(*) from
		(
			SELECT crn, banner, date_utc, min(time_utc) as activation_time, 1 as activation_flag, sum(spend) as activation_spend
			FROM `wx-bq-poc.digital_attribution_modelling.da_activation`
			group by 1,2,3
		) a
    inner join
    (
      SELECT distinct crn, banner, ref_dt from `wx-bq-poc.digital_attribution_modelling.da_event_14mw`
    ) b on a.crn = b.crn and a.banner = b.banner and a.date_utc = b.ref_dt
group by 1

1	
supermarkets
1587810 (70%)
2	
bws
45403
3	
bigw
42793
4	
caltex
5309