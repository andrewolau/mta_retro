--------------------------------------------DA EVENTS----------------------------------------------------
DROP TABLE IF EXISTS `wx-bq-poc.digital_attribution_modelling.dacamp_online_sales`;
DROP TABLE IF EXISTS `wx-bq-poc.digital_attribution_modelling.dacamp_event`;
DROP TABLE IF EXISTS `wx-bq-poc.digital_attribution_modelling.dacamp_activation`;


--- timestamped campaign level event ---

CREATE table `wx-bq-poc.digital_attribution_modelling.dacamp_event` as 
(
	SELECT * FROM
	(
		-- original event store (email & google events)
		SELECT distinct crn, time_utc, Date(time_utc, "Australia/Sydney") as date, banner, channel, 
			case 
			when event_name = 'email open' then 'email_open'
			when event_name = 'email click' then 'email_clk'
			when channel = 'programmatic image' AND event_name = "google impression" then 'prog_img_imp'
			when channel = 'programmatic image' AND event_name = "google click" then 'prog_img_clk'
			when channel = 'programmatic video' AND event_name = "google impression" then 'prog_vdo_imp'
			when channel = 'programmatic video' AND event_name = "google click" then 'prog_vdo_clk'
			when channel = 'youtube' AND event_name = "google impression" then 'yt_imp'
			when channel = 'youtube' AND event_name = "google click" then 'yt_clk'
			when channel = 'gmail' AND event_name = "google impression" then 'gmail_imp'
			when channel = 'gmail' AND event_name = "google click" then 'gmail_clk'
			when channel = 'sem' AND  event_name = 'google click' then CONCAT("sem_", REPLACE(json_extract(attributes, "$.paid_search_type[0]"), "\"", ""), "_clk")
			else 'other' end as channel_event, 
			CASE
			when rw_campaign_code IS NOT NULL AND rw_campaign_code <> '' THEN SUBSTR(rw_campaign_code,1,8)
			when gx_campaign_code IS NOT NULL AND gx_campaign_code <> '' THEN SUBSTR(gx_campaign_code,1,8)
			else '_AlwaysOn' end as campaign_code,
			CASE
			when rw_campaign_start_date IS NOT NULL THEN rw_campaign_start_date
			when gx_campaign_start_date IS NOT NULL THEN gx_campaign_start_date
			else null end as campaign_start_date,
			CASE
			when rw_campaign_end_date IS NOT NULL THEN rw_campaign_end_date
			when gx_campaign_end_date IS NOT NULL THEN gx_campaign_end_date
			else null end as campaign_end_date
		FROM 
		(
			SELECT *, 
				REPLACE(json_extract(attributes, "$.campaign_code[0]"), "\"", "") as rw_campaign_code, 
				REPLACE(json_extract(attributes, "$.campaign_id[0]"), "\"", "") as campaign_id,
				CAST(REPLACE(json_extract(attributes, "$.campaign_or_control_start_date[0]"), "\"", "") AS DATE) as rw_campaign_start_date,
				CAST(REPLACE(json_extract(attributes, "$.campaign_or_control_end_date[0]"), "\"", "") AS DATE) as rw_campaign_end_date
				FROM `wx-bq-poc.wx_lty_digital_attribution_dev.eventstore_events`
			where channel in ('email', 'programmatic image', 'programmatic video', 'sem', 'youtube', 'gmail')
		) a 
		LEFT JOIN 
		(
			SELECT a.campaign_id, min(campaign_start_date) as gx_campaign_start_date, max(campaign_end_date) as gx_campaign_end_date, UPPER(max(campaign_code)) as gx_campaign_code
			FROM 
			(
				SELECT *, CASE WHEN ARRAY_LENGTH(REGEXP_EXTRACT_ALL(placement, r"[A-Z]+-\d\d\d\d")) >0 THEN REGEXP_EXTRACT_ALL(placement, r"[A-Z]+-\d\d\d\d")[OFFSET(0)] else null end as campaign_code
				FROM `wx-bq-poc.wx_lty_digital_attribution_dev.googlex_placements`
			) a
			INNER JOIN
			(
				SELECT * FROM `wx-bq-poc.wx_lty_digital_attribution_dev.googlex_campaigns`
			) b on a.campaign_id = b.campaign_id
			where campaign_code IS NOT NULL
			group by 1
		) b on a.campaign_id = b.campaign_id 
		
		union all

		-- webside events from Tealium --
		SELECT distinct crn, time_utc, Date(time_utc, "Australia/Sydney") as date, banner, channel, 
			channel_event, 
			'_AlwaysOn' as campaign_code, 
			cast(null as date) as campaign_start_date, 
			cast(null as date) as campaign_end_date
		FROM
		(
			SELECT *, rank() over (partition by crn, session_id, channel_event order by time_utc asc) as event_rank
			FROM 
			(
				SELECT *,
					case when event_name in ('page view', 'search view') then 'dx_view'
					else 'dx_clk' end as channel_event,
					json_extract(attributes, "$.udo_tealium_session_id[0]") as session_id
				FROM `wx-bq-poc.wx_lty_digital_attribution_dev.eventstore_events` 
				where channel = 'dx' and event_name in ('page view', 'search view', 'click view')
			)
		)
		where event_rank = 1 
		
		union all
		
		-- rewards app events from Tealium --
		SELECT distinct crn, time_utc, Date(time_utc, "Australia/Sydney") as date, 
			banner
			channel, 
			channel_event,
			campaign_code, 
			cast(null as date) as campaign_start_date, 
			cast(null as date) as campaign_end_date
		FROM
		(
			SELECT crn, time_utc, 
				banner, 
				channel,
				channel_event,
				case when tealium_event like '%product%' and banner = 'supermarkets' then 'CVM-1661' 
					 when tealium_event like '%product%' and banner = 'BWS' then 'BCV-1076' 
					else SUBSTR(campaign_code,1,8) end as campaign_code, 
				rank() over (partition by crn, session_id, channel_event, tealium_event, campaign_code order by time_utc asc) as event_rank
			FROM 
			(
				SELECT *, 
					REPLACE(json_extract(attributes, "$.udo_offer_code[0]"), "\"", "") as campaign_code,
					case when event_name = 'in-app impression' then 'rw_app_imp' else 'rw_app_clk' end as channel_event,
					json_extract(attributes, "$.udo_tealium_session_id[0]") as session_id,
					REPLACE(json_extract(attributes, "$.udo_tealium_event[0]"), "\"", "") as tealium_event,
				FROM `wx-bq-poc.wx_lty_digital_attribution_dev.eventstore_events` 
				where channel = 'rewards app' and event_name in ('in-app impression', 'in-app click')
			)
		)
		where event_rank = 1 and ARRAY_LENGTH(REGEXP_EXTRACT_ALL(campaign_code, r"[A-Z]+-\d\d\d\d")) >0
		
	)
	where banner in ('supermarkets','bws','bigw') 
			and channel_event <> 'other' 
			and date BETWEEN DATE_SUB(DATE_TRUNC(CURRENT_DATE(), week(Sunday)), INTERVAL 41 DAY) AND DATE_TRUNC(CURRENT_DATE(), week(Sunday))
);




--- online sales ---
create table `wx-bq-poc.digital_attribution_modelling.dacamp_online_sales` as
(
	select crn, start_txn_time as time_utc, date, sum(tot_amt_incld_gst-ass.tot_wow_dollar_incld_gst) as spend
	from
	(
		select lylty_card_nbr, start_txn_time, Date(start_txn_time, "Australia/Sydney") as date, division_nbr, prod_nbr, tot_amt_incld_gst, tot_wow_dollar_incld_gst 
		from wx_lty_digital_attribution_dev.redx_loyalty_article_sales_summary 
		where division_nbr in (1005,1030) and checkout_nbr=100 and void_flag <> 'Y'
	) ass
   inner join 
   (
		select prod_nbr, division_nbr 
		from 
		(
			select *, rank() over (partition by prod_nbr, division_nbr order by last_update_date desc) as date_rank 
			from `wx-bq-poc.wx_lty_digital_attribution_dev.redx_loyalty_article_master`
		)
		where date_rank=1
	) am on ass.prod_nbr=am.prod_nbr and ass.division_nbr=am.division_nbr
   inner join 
   (
		select lylty_card_nbr,crn 
		from 
		(
			select *,rank() over (partition by crn order by last_update_date desc) as date_rank 
			from `wx-bq-poc.wx_lty_digital_attribution_dev.redx_loyalty_lylty_card_detail`
		)
		where date_rank=1 and lylty_card_status=1 and crn is not NULL and crn <> '0'
	) as lcd on ass.lylty_card_nbr=lcd.lylty_card_nbr
	where date BETWEEN DATE_SUB(DATE_TRUNC(CURRENT_DATE(), week(Sunday)), INTERVAL 6 DAY) AND DATE_TRUNC(CURRENT_DATE(), week(Sunday))
	group by 1,2,3
);




--- activation sales ---
create table `wx-bq-poc.digital_attribution_modelling.dacamp_activation` as
(
	
	SELECT crn, Timestamp(time_utc) as time_utc, Date(date) as date, banner, campaign_code, campaign_start_date, campaign_end_date,
		case when campaign_tpg_amt = True then sales_amt_tpg else sales_amt end as spend
	FROM `wx-bq-poc.digital_attribution_modelling.sy_safari_activation_new`
);

