--------------------------------------------DA EVENTS----------------------------------------------------
DROP TABLE IF EXISTS `wx-bq-poc.digital_attribution_modelling.dacamp_prod_event`;
DROP TABLE IF EXISTS `wx-bq-poc.digital_attribution_modelling.dacamp_prod_sales`;


--- timestamped campaign level event ---

CREATE table `wx-bq-poc.digital_attribution_modelling.dacamp_prod_event` as 
(
	SELECT * FROM
	(
		SELECT distinct crn, time_utc, Date(time_utc, "Australia/Sydney") as date
			, 'supermarkets' as banner
			, channel
			, CASE WHEN campaign_code is null then CONCAT(channel, '_a_', event) else CONCAT(channel, '_', event) end as channel_event
			, CASE WHEN campaign_code is null then 'CVM-3306' else campaign_code end as campaign_code
			, DATE_TRUNC(date, week(Wednesday)) as campaign_start_date
			, DATE_ADD(DATE_TRUNC(date, week(Wednesday)),interval 6 day) as campaign_end_date
		FROM  `wx-bq-poc.digital_attribution_modelling.dacamp_prod_eventstore_view_snapshot`
	)
	where campaign_code in ('CVM-3306', '_AlwaysOn')
			and channel_event <> 'other' 
			and date BETWEEN Date('2020-05-27') and Date('2020-06-28') 
);



--- online sales ---
create table `wx-bq-poc.digital_attribution_modelling.dacamp_prod_sales` as
(
	select lcd.crn, start_txn_time as time_utc, date
		, 'CVM-3306' as campaign_code
		, DATE_TRUNC(date, week(Wednesday)) as campaign_start_date
		, sum(tot_amt_incld_gst-ass.tot_wow_dollar_incld_gst) as tot_spend
		, sum(tot_amt_incld_gst-ass.tot_wow_dollar_incld_gst) as spend
	from
	(
		select lylty_card_nbr, start_txn_time, Date(start_txn_time, "Australia/Sydney") as date, division_nbr, prod_nbr, tot_amt_incld_gst, tot_wow_dollar_incld_gst 
		from wx_lty_digital_attribution_dev.redx_loyalty_article_sales_summary 
		where division_nbr in (1005,1030) and void_flag <> 'Y' and Date(start_txn_time) BETWEEN Date('2020-05-27') and Date('2020-06-28')
	) ass
   inner join 
   (
		select prod_nbr, division_nbr 
		from 
		(
			select *, rank() over (partition by prod_nbr, division_nbr order by last_update_date desc) as date_rank 
			from `wx-bq-poc.wx_lty_digital_attribution_dev.redx_loyalty_article_master`
		)
		where date_rank=1
	) am on ass.prod_nbr=am.prod_nbr and ass.division_nbr=am.division_nbr
   inner join 
   (
		select lylty_card_nbr,crn 
		from 
		(
			select *,rank() over (partition by crn order by last_update_date desc) as date_rank 
			from `wx-bq-poc.wx_lty_digital_attribution_dev.redx_loyalty_lylty_card_detail`
		)
		where date_rank=1 and lylty_card_status=1 and crn is not NULL and crn <> '0'
	) as lcd on ass.lylty_card_nbr=lcd.lylty_card_nbr
	
	inner join
	(
		SELECT distinct crn from `wx-bq-poc.digital_attribution_modelling.dacamp_prod_event`
	) crn_base on lcd.crn = crn_base.crn
	group by 1,2,3,4,5
);
