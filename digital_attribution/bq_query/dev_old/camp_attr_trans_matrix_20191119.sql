--------------------------------------------DA EVENTS----------------------------------------------------


--- online sales ---
DROP TABLE IF EXISTS `wx-bq-poc.digital_attribution_modelling.dacamp_online_sales`;
create table `wx-bq-poc.digital_attribution_modelling.dacamp_online_sales` as
(
	select crn, start_txn_time as time_utc, Date(start_txn_time) as date_utc, sum(tot_amt_incld_gst-ass.tot_wow_dollar_incld_gst) as spend
	from
	(
		select lylty_card_nbr, start_txn_time, division_nbr, prod_nbr, tot_amt_incld_gst, tot_wow_dollar_incld_gst 
		from wx_lty_digital_attribution_dev.redx_loyalty_article_sales_summary 
		where division_nbr in (1005,1030) and checkout_nbr=100 and void_flag <> 'Y'
	) ass
   inner join 
   (
		select prod_nbr, division_nbr 
        from 
		(
			select *, rank() over (partition by prod_nbr, division_nbr order by last_update_date desc) as date_rank 
			from `wx-bq-poc.wx_lty_digital_attribution_dev.redx_loyalty_article_master`
		)
		where date_rank=1
	) am on ass.prod_nbr=am.prod_nbr and ass.division_nbr=am.division_nbr
   inner join 
   (
		select lylty_card_nbr,crn 
        from 
		(
			select *,rank() over (partition by crn order by last_update_date desc) as date_rank 
            from `wx-bq-poc.wx_lty_digital_attribution_dev.redx_loyalty_lylty_card_detail`
		)
		where date_rank=1 and lylty_card_status=1 and crn is not NULL and crn <> '0'
	) as lcd on ass.lylty_card_nbr=lcd.lylty_card_nbr
	where Date(start_txn_time) BETWEEN DATE_SUB(Date('2019-11-19'), INTERVAL 6 DAY) AND Date('2019-11-19')
	group by 1,2
);



--- timestamped campaign level event ---

DROP TABLE IF EXISTS `wx-bq-poc.digital_attribution_modelling.dacamp_event`;
CREATE table IF NOT EXISTS `wx-bq-poc.digital_attribution_modelling.dacamp_event` as 
(
	SELECT * FROM
	(
		SELECT distinct crn, time_utc, Date(time_utc) as date_utc, date, banner, channel, 
		case 
		when event_name = 'email open' then 'email_open'
		when event_name = 'email click' then 'email_clk'
		when channel = 'programmatic image' AND event_name = "google impression" then 'prog_img_imp'
		when channel = 'programmatic image' AND event_name = "google click" then 'prog_img_clk'
		when channel = 'programmatic video' AND event_name = "google impression" then 'prog_vdo_imp'
		when channel = 'programmatic video' AND event_name = "google click" then 'prog_vdo_clk'
		when channel = 'youtube' AND event_name = "google impression" then 'yt_imp'
		when channel = 'youtube' AND event_name = "google click" then 'yt_clk'
		when channel = 'gmail' AND event_name = "google impression" then 'gmail_imp'
		when channel = 'gmail' AND event_name = "google click" then 'gmail_clk'
		when event_name = 'google click' AND channel = 'sem' then CONCAT("sem_", REPLACE(json_extract(attributes, "$.paid_search_type[0]"), "\"", ""), "_clk")
		when event_name = "google impression" then "other_google_imp"
		when event_name = "google click" then "other_google_clk"
		else 'other' end as channel_event, 
		CASE
		when rw_campaign_code IS NOT NULL AND rw_campaign_code <> '' THEN SUBSTR(rw_campaign_code,1,8)
		when gx_campaign_code IS NOT NULL AND gx_campaign_code <> '' THEN SUBSTR(gx_campaign_code,1,8)
		else '_OTH' end as campaign_code,
		CASE
		when rw_campaign_start_date IS NOT NULL THEN rw_campaign_start_date
		when gx_campaign_start_date IS NOT NULL THEN gx_campaign_start_date
		else null end as campaign_start_date,
		CASE
		when rw_campaign_end_date IS NOT NULL THEN rw_campaign_end_date
		when gx_campaign_end_date IS NOT NULL THEN gx_campaign_end_date
		else null end as campaign_end_date
		FROM 
		(
			SELECT *, 
				REPLACE(json_extract(attributes, "$.campaign_code[0]"), "\"", "") as rw_campaign_code, 
				REPLACE(json_extract(attributes, "$.campaign_id[0]"), "\"", "") as campaign_id,
				CAST(REPLACE(json_extract(attributes, "$.campaign_or_control_start_date[0]"), "\"", "") AS DATE) as rw_campaign_start_date,
				CAST(REPLACE(json_extract(attributes, "$.campaign_or_control_end_date[0]"), "\"", "") AS DATE) as rw_campaign_end_date
				FROM `wx-bq-poc.wx_lty_digital_attribution_dev.eventstore_events`
			where channel <> 'dx'
		) a 
		LEFT JOIN 
		(
			SELECT a.campaign_id, min(campaign_start_date) as gx_campaign_start_date, max(campaign_end_date) as gx_campaign_end_date, UPPER(max(campaign_code)) as gx_campaign_code
			FROM 
			(
				SELECT *, CASE WHEN ARRAY_LENGTH(REGEXP_EXTRACT_ALL(placement, r"[A-Z]+-\d\d\d\d")) >0 THEN REGEXP_EXTRACT_ALL(placement, r"[A-Z]+-\d\d\d\d")[OFFSET(0)] else null end as campaign_code
				FROM `wx-bq-poc.wx_lty_digital_attribution_dev.googlex_placements`
			) a
			INNER JOIN
			(
				SELECT * FROM `wx-bq-poc.wx_lty_digital_attribution_dev.googlex_campaigns`
			) b on a.campaign_id = b.campaign_id
			where campaign_code IS NOT NULL
			group by 1
		) b on a.campaign_id = b.campaign_id 
		
		union all

		SELECT distinct crn, time_utc, Date(time_utc) as date_utc, date, banner, channel, channel_event, '_OTH' as campaign_code, cast(null as date) as campaign_start_date, cast(null as date) as campaign_end_date
		FROM
		(
			SELECT *, rank() over (partition by crn, json_extract(attributes, "$.udo_tealium_session_id[0]"), channel_event order by time_utc asc) as event_rank
			FROM 
			(
				SELECT *,
					case when event_name in ('page view', 'search view') then 'dx_view'
					else 'dx_clk' end as channel_event
				FROM `wx-bq-poc.wx_lty_digital_attribution_dev.eventstore_events` 
				where channel = 'dx' and event_name in ('page view', 'search view', 'click view')
			)
		)
		where event_rank = 1 
	)
	where banner in ('supermarkets','bws','bigw') and channel_event <> 'other' and date_utc BETWEEN DATE_SUB(Date('2019-11-19'), INTERVAL 41 DAY) AND Date('2019-11-19')
);




DROP TABLE IF EXISTS `wx-bq-poc.digital_attribution_modelling.dacamp_activation_list`;
CREATE TABLE `wx-bq-poc.digital_attribution_modelling.dacamp_activation_list` as
(
	Select * from
	(
		SELECT crn, banner, SUBSTR(campaign_code,1,8) as campaign_code, Cast(date as Date) as date,
					cast(campaign_start_date as Date) as campaign_start_date, 
					DATE_ADD(cast(campaign_start_date as Date), INTERVAL cast(camp_dur_days as INT64)-1 DAY) as campaign_end_date,
					CAST(time_utc as Timestamp) as time_utc,
					action_type,
					basket_key
		FROM `wx-bq-poc.digital_attribution_modelling.sy_safari_activation_new`
	)
	where banner in ('supermarkets','bws','bigw') and date <= Date('2019-11-19')
);



--- activation sales ---
DROP TABLE IF EXISTS `wx-bq-poc.digital_attribution_modelling.dacamp_activation`;
create table `wx-bq-poc.digital_attribution_modelling.dacamp_activation` as
(
	with base as
	(
		SELECT * 
		FROM
		(
			SELECT a.crn, campaign_code, campaign_type, a.banner, campaign_start_date, campaign_end_date, time_utc, Date(time_utc) as date_utc,
			case when sum(activation_spend) is null then 0 else sum(activation_spend) end as spend 
			FROM
			(
				SELECT crn, banner, campaign_code, campaign_start_date, campaign_end_date, 'activate' as campaign_type, min(time_utc) as time_utc
				FROM `wx-bq-poc.digital_attribution_modelling.dacamp_activation_list`
				where action_type = 'ACTIVATE'
				group by 1,2,3,4,5
			) a 
			left join
			(
				select crn, banner, start_txn_time, sum(tot_amt_incld_gst-ass.tot_wow_dollar_incld_gst) as activation_spend
				from 
				(
					select lylty_card_nbr, start_txn_time, prod_nbr, tot_amt_incld_gst, tot_wow_dollar_incld_gst, division_nbr,
					case 
					when division_nbr in (1005, 1030) then 'supermarkets'
					when division_nbr = 1010 then 'bws'
					when division_nbr = 1060 then 'bigw'
					when division_nbr = 1021 then 'caltex'
					when division_nbr = 1020 then 'fuelco'
					else 'other' end as banner
					from wx_lty_digital_attribution_dev.redx_loyalty_article_sales_summary 
					where division_nbr in (1005,1010,1030,1021,1020,1060) and void_flag <> 'Y'
				) ass
				inner join
				(
					select prod_nbr, division_nbr 
					from 
					(
						select *,rank() over (partition by prod_nbr, division_nbr order by last_update_date desc) as date_rank 
						from `wx-bq-poc.wx_lty_digital_attribution_dev.redx_loyalty_article_master`
					)
					where date_rank=1
				) am on ass.prod_nbr=am.prod_nbr and ass.division_nbr=am.division_nbr
				inner join 
				(
					select lylty_card_nbr,crn 
					from 
					(
						select *,rank() over (partition by crn order by last_update_date desc) as date_rank 
						from `wx-bq-poc.wx_lty_digital_attribution_dev.redx_loyalty_lylty_card_detail` 
						where lylty_card_status=1 and crn is not NULL and crn <> '0'
					)
					where date_rank=1
				) as lcd on ass.lylty_card_nbr=lcd.lylty_card_nbr
				group by 1,2,3
			) b on a.crn = b.crn and a.campaign_start_date <= Date(b.start_txn_time) and a.campaign_end_date >= Date(b.start_txn_time) and a.banner = b.banner
			group by 1,2,3,4,5,6,7

			union all

			SELECT a.crn, campaign_code, campaign_type, banner, campaign_start_date, campaign_end_date, min(time_utc) as time_utc, Date(min(time_utc)) as date_utc, 
			case when sum(purchase_spend) is null then 0 else sum(purchase_spend) end as spend
			FROM
			(
				SELECT distinct crn, banner, campaign_code, campaign_start_date, campaign_end_date, 'non-activate' as campaign_type, time_utc, basket_key
				FROM `wx-bq-poc.digital_attribution_modelling.dacamp_activation_list`
				where action_type = 'PURCHASE'
			) a 
			left join
			(
				SELECT basket_key, 
				sum(tot_amt_incld_gst - tot_wow_dollar_incld_gst) as purchase_spend
				from 
				(
					SELECT basket_key, prod_nbr, tot_amt_incld_gst, tot_wow_dollar_incld_gst, division_nbr
					FROM wx_lty_digital_attribution_dev.redx_loyalty_article_sales_summary
					where void_flag <> 'Y'
				) ass 
				inner join
				(
					select prod_nbr, division_nbr 
					from 
					(
						select *,rank() over (partition by prod_nbr, division_nbr order by last_update_date desc) as date_rank 
						from `wx-bq-poc.wx_lty_digital_attribution_dev.redx_loyalty_article_master`
					)
					where date_rank=1
				) am on ass.prod_nbr=am.prod_nbr and ass.division_nbr=am.division_nbr
				group by 1
			) b on a.basket_key = b.basket_key
			group by 1,2,3,4,5,6
		)
		where campaign_end_date >= DATE_SUB(Date('2019-11-19'), INTERVAL 6 DAY) and campaign_end_date <= Date('2019-11-19')
	)
	
	SELECT crn, time_utc, date_utc, a.campaign_code, a.banner, spend,
			case when campaign_type = 'activate' and Date_diff(campaign_first_date, campaign_start_date, DAY) <= 0 
					then campaign_first_date else campaign_start_date end as campaign_start_date, campaign_end_date
	FROM 
	(	
		(
			select * 
				from base
		) a
		left join 
		(   
			select banner, campaign_code, min(Date(time_utc, 'Australia/Sydney')) as campaign_first_date
				from `wx-bq-poc.digital_attribution_modelling.dacamp_event`
			group by 1,2
		) b on a.banner = b.banner and a.campaign_code = b.campaign_code
	)
);



drop table if exists `wx-bq-poc.digital_attribution_modelling.dacamp_segment`;

create table `wx-bq-poc.digital_attribution_modelling.dacamp_segment` as
(
	SELECT crn, max(lifestage) as segment
	FROM `wx-bq-poc.wx_lty_digital_attribution_dev.redx_loyalty_customer_value_model`
	where Date(pw_end_date_utc) = DATE_SUB(Date('2019-11-19'), INTERVAL 5 DAY)
	group by 1
);



--------------------------------------------USER JOURNEY----------------------------------------------------

Drop table if exists `wx-bq-poc.digital_attribution_modelling.dacamp_event_mw`;
Create table `wx-bq-poc.digital_attribution_modelling.dacamp_event_mw` as 
(
	with base_act as 
	(
		SELECT a.*
				, b.crn
				, b.date
				, b.time_utc
				, CONCAT(b.campaign_code, '_', b.channel_event) as channel_event
				, b.campaign_code as cc_event
				, d.conv_time
				, d.conv_flag
				, d.spend
				, lead(CONCAT(b.campaign_code, '_', b.channel_event)) over (partition by b.crn, a.banner, a.campaign_code, a.campaign_start_date, a.campaign_end_date order by b.time_utc desc) as channel_event_pre
				, case when d.conv_time is null then a.campaign_end_date else Date(d.conv_time, 'Australia/Sydney') end as end_date 
		FROM
		(
			(
				select distinct banner
							, campaign_code, campaign_start_date, campaign_end_date 
				from `wx-bq-poc.digital_attribution_modelling.dacamp_activation`
			) a
			
			inner join 
			(
				SELECT distinct banner, campaign_code, crn, time_utc, date, channel_event 
				from `wx-bq-poc.digital_attribution_modelling.dacamp_event`
			) b on a.banner = b.banner 
					and b.date BETWEEN a.campaign_start_date AND a.campaign_end_date
					
			inner join
			(
				SELECT distinct crn, campaign_code, banner from `wx-bq-poc.digital_attribution_modelling.dacamp_event` 
			) c on b.crn = c.crn 
					and a.campaign_code = c.campaign_code
					and a.banner = c.banner
			
			left join
			(
				select crn, banner, campaign_code, campaign_start_date, campaign_end_date, min(time_utc) as conv_time, 1 as conv_flag, sum(spend) as spend
				from `wx-bq-poc.digital_attribution_modelling.dacamp_activation`
				group by 1,2,3,4,5
			) d on b.crn = d.crn 
					and a.banner = d.banner 
					and a.campaign_code = d.campaign_code and a.campaign_start_date = d.campaign_start_date and a.campaign_end_date = d.campaign_end_date
		)
		where d.conv_time is null or b.time_utc <= d.conv_time 
	),
	
	base_onl as 
	(
		SELECT a.*
				, b.crn
				, b.date
				, b.time_utc
				, CONCAT(b.campaign_code, '_', b.channel_event) as channel_event
				, b.campaign_code as cc_event
				, c.conv_time
				, c.conv_flag
				, c.spend
				, lead(CONCAT(b.campaign_code, '_', b.channel_event)) over (partition by b.crn, a.banner, a.campaign_code, a.campaign_start_date, a.campaign_end_date order by b.time_utc desc) as channel_event_pre
				, case when c.conv_time is null then a.campaign_end_date else Date(c.conv_time, 'Australia/Sydney') end as end_date 
		FROM
		(
			(
				select 'supermarkets' as banner, 'ONLINE' as campaign_code, min(date_utc) as campaign_start_date, max(date_utc) as campaign_end_date 
				from `wx-bq-poc.digital_attribution_modelling.dacamp_online_sales`
				group by 1
			) a
			
			inner join 
			(
				SELECT distinct banner, crn, time_utc, date, campaign_code, channel_event 
				from `wx-bq-poc.digital_attribution_modelling.dacamp_event`
			) b on a.banner = b.banner and b.date BETWEEN DATE_SUB(a.campaign_start_date, INTERVAL 6 DAY) AND a.campaign_end_date
			
			left join 
			(
				select crn, min(time_utc) as conv_time, 1 as conv_flag, sum(spend) as spend
				from `wx-bq-poc.digital_attribution_modelling.dacamp_online_sales`
				group by 1
			) c on b.crn = c.crn 
		)
		where (c.conv_time is null and b.date between a.campaign_start_date AND a.campaign_end_date) 
				or (c.conv_time is not null and b.time_utc <= c.conv_time and b.date >= date_sub(Date(c.conv_time, "Australia/Sydney"), INTERVAL 6 DAY))
	),

	base as 
	(
		SELECT banner, campaign_code, campaign_start_date, campaign_end_date, crn, time_utc, date, channel_event, conv_time, conv_flag, spend, cc_event
		FROM
		(
			SELECT *
			FROM base_act
			
			UNION ALL
			
			SELECT *
			FROM base_onl
		) 
		where (channel_event_pre <> channel_event or channel_event_pre is null)
				and (
						SUBSTR(cc_event,1,8) = campaign_code -- same campaign events
						or SUBSTR(cc_event,1,8) <> campaign_code and channel_event like '%clk%' -- click events of other campaign
						or SUBSTR(cc_event,1,8) in ('CVM-1661') and channel_event like '%open%' and date >= date_sub(end_date, INTERVAL 1 Day) -- imp events of TTL campaigns
					)
	)
	
	
	SELECT banner, campaign_code, campaign_start_date, campaign_end_date, crn, time_utc, channel_event, conv_time, conv_flag, spend
	from
	(
		SELECT a.*, b.campaign_start_date as csd_event, b.campaign_end_date as ced_event, conv_time_event,
				row_number() over (partition by a.banner, a.campaign_code, a.campaign_start_date, a.campaign_end_date, a.crn, a.time_utc, a.channel_event 
									order by abs(DATE_DIFF(a.date, b.campaign_start_date, DAY)) + abs(DATE_DIFF(b.campaign_end_date, a.date, DAY))) as pos
		FROM
		(
			SELECT * from base
			where campaign_code <> 'ONLINE' and channel_event like '%email%' and cc_event not like '%OTH%'
		) a
		inner join 
		(
			SELECT distinct banner, campaign_code, campaign_start_date, campaign_end_date 
			from `wx-bq-poc.digital_attribution_modelling.dacamp_activation_list`
		) b on a.banner = b.banner and a.cc_event = b.campaign_code
		left join
		(
			SELECT crn, banner, campaign_code, campaign_start_date, campaign_end_date, min(time_utc) as conv_time_event 
			from `wx-bq-poc.digital_attribution_modelling.dacamp_activation_list`
			group by 1,2,3,4,5
		) c on b.banner = c.banner and b.campaign_code = c.campaign_code and b.campaign_start_date = c.campaign_start_date and b.campaign_end_date = c.campaign_end_date and a.crn = c.crn
	)
	where pos = 1 
			and ((campaign_code = SUBSTR(cc_event,1,8) and campaign_end_date = ced_event) 
					or (campaign_code <> SUBSTR(cc_event,1,8) and conv_time_event is null and ced_event > campaign_start_date)) 
	
	union all
	
 	SELECT banner, campaign_code, campaign_start_date, campaign_end_date, crn, time_utc, channel_event, conv_time, conv_flag, spend
	from base
	where (campaign_code = 'ONLINE') or cc_event like '%OTH%' 
			or (channel_event not like '%email%' and date BETWEEN campaign_start_date AND campaign_end_date) 
	
);


DROP TABLE IF EXISTS `digital_attribution_modelling.dacamp_trans_matrix_all`;
CREATE TABLE `digital_attribution_modelling.dacamp_trans_matrix_all` as
(
	with states as 
	(
		with base as 
		(
		    with onl_start_date as (select min(Date(time_utc)) from `wx-bq-poc.digital_attribution_modelling.dacamp_online_sales`),
			onl_end_date as (select max(Date(time_utc)) from `wx-bq-poc.digital_attribution_modelling.dacamp_online_sales`)
			
			SELECT banner, campaign_code, campaign_start_date, campaign_end_date,
				case when channel_event_pre is null then 'START' else channel_event_pre end as state1,
				channel_event as state2,
				case when channel_event_post is null then 'NO CONV' else channel_event_post end as state3,
				count(*) as cnt
			FROM
			(
				(
					SELECT crn, banner, campaign_code, campaign_start_date, campaign_end_date, time_utc,
					 lead(channel_event) over (partition by crn, banner, campaign_code, campaign_start_date, campaign_end_date order by time_utc desc) as channel_event_pre,
					 channel_event, 
					 lag(channel_event) over (partition by crn, banner, campaign_code, campaign_start_date, campaign_end_date order by time_utc desc) as channel_event_post,
					 row_number() over (partition by crn, banner, campaign_code, campaign_start_date, campaign_end_date order by time_utc desc) as pos
					FROM
					(
						SELECT banner, campaign_code, campaign_start_date, campaign_end_date, crn, time_utc,  channel_event FROM `wx-bq-poc.digital_attribution_modelling.dacamp_event_mw` where campaign_code <> 'ONLINE'

						UNION ALL

						SELECT banner, campaign_code, campaign_start_date, campaign_end_date, crn, min(time_utc) as time_utc, 'CONV' as channel_event
						FROM `wx-bq-poc.digital_attribution_modelling.dacamp_activation`
						group by 1,2,3,4,5	
					)
				)	
			
				union all
			
				(
					SELECT crn, 'supermarkets' as banner, 'ONLINE' as campaign_code, (select * FROM onl_start_date) as campaign_start_date, (select * from onl_end_date) as campaign_end_date, time_utc,
					 lead(channel_event) over (partition by crn order by time_utc desc) as channel_event_pre,
					 channel_event, 
					 lag(channel_event) over (partition by crn order by time_utc desc) as channel_event_post,
					 row_number() over (partition by crn order by time_utc desc) as pos
					FROM
					(
						SELECT crn, time_utc, channel_event FROM `wx-bq-poc.digital_attribution_modelling.dacamp_event_mw` where campaign_code = 'ONLINE'

						UNION ALL

						SELECT crn, min(time_utc) as time_utc, 'CONV' as channel_event
						FROM `wx-bq-poc.digital_attribution_modelling.dacamp_online_sales`
						group by 1
					)
				)
			)
			
			where (channel_event_pre <> 'CONV' or channel_event_pre is null) AND not (channel_event = 'CONV' and channel_event_pre is null)
			group by 1,2,3,4,5,6,7
	    )

		SELECT banner, campaign_code, campaign_start_date, campaign_end_date, state1, state2, sum(cnt) as cnt
		FROM 
		(
			SELECT banner, campaign_code, campaign_start_date, campaign_end_date, state1, state2, cnt from base
			UNION ALL
			SELECT banner, campaign_code, campaign_start_date, campaign_end_date, state2, state3, cnt from base where state3 = 'NO CONV' AND state2 <> 'CONV'
		)
		group by 1,2,3,4,5,6
	)

	SELECT a.banner, a.campaign_code, a.campaign_start_date, a.campaign_end_date, a.state1, a.state2, a.cnt, a.cnt/b.cnt_state1 as prob
	FROM
	(
		(
			SELECT * FROM states
		) a
		left join
		(
			SELECT banner, campaign_code, campaign_start_date, campaign_end_date, state1, sum(cnt) as cnt_state1 
			from states 
			group by 1,2,3,4,5
		) b on a.banner = b.banner and a.campaign_code = b.campaign_code and a.campaign_start_date = b.campaign_start_date and a.campaign_end_date = b.campaign_end_date and a.state1 = b.state1 
	)

);


DROP TABLE IF EXISTS `digital_attribution_modelling.dacamp_trans_matrix_seg`;
CREATE TABLE `digital_attribution_modelling.dacamp_trans_matrix_seg` as
(
	with states as 
	(
		with base as 
		(
			with onl_start_date as (select min(Date(time_utc)) from `wx-bq-poc.digital_attribution_modelling.dacamp_online_sales`),
			onl_end_date as (select max(Date(time_utc)) from `wx-bq-poc.digital_attribution_modelling.dacamp_online_sales`)
			
			SELECT banner, campaign_code, campaign_start_date, campaign_end_date, segment,
				case when channel_event_pre is null then 'START' else channel_event_pre end as state1,
				channel_event as state2,
				case when channel_event_post is null then 'NO CONV' else channel_event_post end as state3,
				count(*) as cnt
			FROM
			(
				(
					SELECT a.crn, banner, campaign_code, campaign_start_date, campaign_end_date, time_utc,
					 case when segment is null then 'None' else segment end as segment,
					lead(channel_event) over (partition by a.crn, a.banner, a.campaign_code, a.campaign_start_date, a.campaign_end_date order by a.time_utc desc) as channel_event_pre,
					channel_event, 
					lag(channel_event) over (partition by a.crn, a.banner, a.campaign_code, a.campaign_start_date, a.campaign_end_date order by a.time_utc desc) as channel_event_post,
					row_number() over (partition by a.crn, a.banner, a.campaign_code, a.campaign_start_date, a.campaign_end_date order by a.time_utc desc) as pos
					FROM
					(
						SELECT banner, campaign_code, campaign_start_date, campaign_end_date, crn, time_utc,  channel_event FROM `wx-bq-poc.digital_attribution_modelling.dacamp_event_mw` where campaign_code <> 'ONLINE'

						UNION ALL

						SELECT banner, campaign_code, campaign_start_date, campaign_end_date, crn, min(time_utc) as time_utc, 'CONV' as channel_event
						FROM `wx-bq-poc.digital_attribution_modelling.dacamp_activation`
						group by 1,2,3,4,5		
					) a 
					left join
					(
						SELECT distinct crn, segment
						FROM `wx-bq-poc.digital_attribution_modelling.dacamp_segment` 
					) c on a.crn = c.crn
			    )
				
				union all
				
				(
					SELECT a.crn, 'supermarkets' as banner, 'ONLINE' as campaign_code, (select * FROM onl_start_date) as campaign_start_date, (select * FROM onl_end_date) as campaign_end_date, time_utc,
					case when segment is null then 'None' else segment end as segment,
					 lead(channel_event) over (partition by a.crn order by time_utc desc) as channel_event_pre,
					 channel_event, 
					 lag(channel_event) over (partition by a.crn order by time_utc desc) as channel_event_post,
					 row_number() over (partition by a.crn order by time_utc desc) as pos
					FROM
					(
						SELECT crn, time_utc, channel_event FROM `wx-bq-poc.digital_attribution_modelling.dacamp_event_mw` where campaign_code = 'ONLINE'

						UNION ALL

						SELECT crn, min(time_utc) as time_utc, 'CONV' as channel_event
						FROM `wx-bq-poc.digital_attribution_modelling.dacamp_online_sales`
						group by 1
					) a 
					left join
					(
						SELECT distinct crn, segment
						FROM `wx-bq-poc.digital_attribution_modelling.dacamp_segment` 
					) c on a.crn = c.crn
				)
				
			)
			where (channel_event_pre <> 'CONV' or channel_event_pre is null) AND not (channel_event = 'CONV' and channel_event_pre is null)
			group by 1,2,3,4,5,6,7,8
	    )

		SELECT banner, segment, campaign_code, campaign_start_date, campaign_end_date, state1, state2, sum(cnt) as cnt
		FROM 
		(
			SELECT banner, segment, campaign_code, campaign_start_date, campaign_end_date, state1, state2, cnt from base
			UNION ALL
			SELECT banner, segment, campaign_code, campaign_start_date, campaign_end_date, state2, state3, cnt from base where state3 = 'NO CONV' AND state2 <> 'CONV'
		)
		group by 1,2,3,4,5,6,7
	)

	SELECT a.segment, a.banner, a.campaign_code, a.campaign_start_date, a.campaign_end_date, a.state1, a.state2, a.cnt, a.cnt/b.cnt_state1 as prob
	FROM
	(
		(
			SELECT * FROM states
		) a
		left join
		(
			SELECT banner, segment, campaign_code, campaign_start_date, campaign_end_date, state1, sum(cnt) as cnt_state1 
			from states 
			group by 1,2,3,4,5,6
		) b on a.banner = b.banner and a.campaign_code = b.campaign_code and a.campaign_start_date = b.campaign_start_date and a.campaign_end_date = b.campaign_end_date and a.state1 = b.state1 and a.segment = b.segment
	)

);


DROP TABLE IF EXISTS `digital_attribution_modelling.dacamp_trans_matrix`;
CREATE TABLE `digital_attribution_modelling.dacamp_trans_matrix` as
(
	SELECT 'Total' as segment, * from `digital_attribution_modelling.dacamp_trans_matrix_all`
	union all
	SELECT * from `digital_attribution_modelling.dacamp_trans_matrix_seg`
);

DROP TABLE IF EXISTS `digital_attribution_modelling.dacamp_trans_matrix_all`;
DROP TABLE IF EXISTS `digital_attribution_modelling.dacamp_trans_matrix_seg`;
