DROP TABLE IF EXISTS `digital_attribution_modelling.da_markov_trans_matrix_activation_bmp`;
CREATE TABLE `digital_attribution_modelling.da_markov_trans_matrix_activation_bmp` as
(
	with states as 
	(
		with base as 
		(
			SELECT banner, bmp_segment,
				case when channel_event_pre is null then 'START' else channel_event_pre end as state1,
				channel_event as state2,
				case when channel_event_post is null then 'NO CONV' else channel_event_post end as state3,
				count(*) as cnt
			FROM
			(
				SELECT a.crn, ref_dt, time_utc, banner,
				 case when bmp_segment is null then 'None' else bmp_segment end as bmp_segment,
				 lead(channel_event) over (partition by a.crn, ref_dt, banner order by time_utc desc) as channel_event_pre,
				 channel_event, 
				 lag(channel_event) over (partition by a.crn, ref_dt, banner order by time_utc desc) as channel_event_post,
				 row_number() over (partition by a.crn, ref_dt, banner order by time_utc desc) as pos
				FROM
				(
					SELECT crn, ref_dt, min(time_utc) as time_utc, banner, channel_event
					FROM
					(
						SELECT crn, ref_dt, time_utc, banner,
							case 
							when channel_event like 'dx_view%' then 'dx_view' 
							when channel_event like 'dx_clk%' then 'dx_clk'
							else channel_event end as channel_event
						FROM `wx-bq-poc.digital_attribution_modelling.da_event_mw`
					)
					group by 1,2,4,5

					UNION ALL

					SELECT crn, date_utc, min(time_utc) as time_utc, banner, 'CONV' as channel_event
					FROM `wx-bq-poc.digital_attribution_modelling.da_activation`
					group by 1,2,4,5		
				) a 
				left join
				(
					SELECT distinct crn, bmp_segment
					FROM `wx-bq-poc.digital_attribution_modelling.da_bmp_segment` 
				) c on a.crn = c.crn
			)
			where (channel_event_pre <> 'CONV' or channel_event_pre is null) AND not (channel_event = 'CONV' and channel_event_pre is null)
			group by 1,2,3,4,5
	    )


		SELECT banner, bmp_segment, state1, state2, sum(cnt) as cnt
		FROM 
		(
			SELECT banner, bmp_segment, state1, state2, cnt from base
			UNION ALL
			SELECT banner, bmp_segment, state2, state3, cnt from base where state3 = 'NO CONV' AND state2 <> 'CONV'
		)
		group by 1,2,3,4
	)


	SELECT a.banner, a.bmp_segment, a.state1, a.state2, a.cnt, a.cnt/b.cnt_state1 as prob
	FROM
	(
		(SELECT * FROM states) a
		left join
		(SELECT banner, bmp_segment, state1, sum(cnt) as cnt_state1 from states group by 1,2,3) b on a.banner = b.banner and a.state1 = b.state1 and a.bmp_segment = b.bmp_segment
	)

);




DROP TABLE IF EXISTS `digital_attribution_modelling.da_markov_trans_matrix_online_bmp`;
CREATE TABLE `digital_attribution_modelling.da_markov_trans_matrix_online_bmp` as
(
	with states as 
	(
		with base as 
		(
			SELECT banner, bmp_segment,
				case when channel_event_pre is null then 'START' else channel_event_pre end as state1,
				channel_event as state2,
				case when channel_event_post is null then 'NO CONV' else channel_event_post end as state3,
				count(*) as cnt
			FROM
			(
				SELECT a.crn, ref_dt, time_utc, banner,
				 case when bmp_segment is null then 'None' else bmp_segment end as bmp_segment,
				 lead(channel_event) over (partition by a.crn, ref_dt, banner order by time_utc desc) as channel_event_pre,
				 channel_event, 
				 lag(channel_event) over (partition by a.crn, ref_dt, banner order by time_utc desc) as channel_event_post,
				 row_number() over (partition by a.crn, ref_dt, banner order by time_utc desc) as pos
				FROM
				(
					SELECT crn, ref_dt, min(time_utc) as time_utc, banner, channel_event
					FROM
					(
						SELECT crn, ref_dt, time_utc, banner,
							case 
							when channel_event like 'dx_view%' then 'dx_view' 
							when channel_event like 'dx_clk%' then 'dx_clk'
							else channel_event end as channel_event
						FROM `wx-bq-poc.digital_attribution_modelling.da_event_mw`
					)
					group by 1,2,4,5

					UNION ALL

					SELECT crn, date_utc, min(time_utc) as time_utc, 'supermarkets' as banner, 'CONV' as channel_event
					FROM `wx-bq-poc.digital_attribution_modelling.da_online_sales`
					group by 1,2,4,5		
				) a 
				left join
				(
					SELECT distinct crn, bmp_segment
					FROM `wx-bq-poc.digital_attribution_modelling.da_bmp_segment` 
				) c on a.crn = c.crn
			)
			where (channel_event_pre <> 'CONV' or channel_event_pre is null) AND not (channel_event = 'CONV' and channel_event_pre is null)
			group by 1,2,3,4,5
	    )


		SELECT banner, bmp_segment, state1, state2, sum(cnt) as cnt
		FROM 
		(
			SELECT banner, bmp_segment, state1, state2, cnt from base
			UNION ALL
			SELECT banner, bmp_segment, state2, state3, cnt from base where state3 = 'NO CONV' AND state2 <> 'CONV'
		)
		group by 1,2,3,4
	)


	SELECT a.banner, a.bmp_segment, a.state1, a.state2, a.cnt, a.cnt/b.cnt_state1 as prob
	FROM
	(
		(SELECT * FROM states) a
		left join
		(SELECT banner, bmp_segment, state1, sum(cnt) as cnt_state1 from states group by 1,2,3) b on a.banner = b.banner and a.state1 = b.state1 and a.bmp_segment = b.bmp_segment
	)

);