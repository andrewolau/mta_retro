

--- activation flag no spend (not used) ---
/*/
create table `wx-bq-poc.digital_attribution_modelling.da_activation` as
(
SELECT distinct crn, campaign_code, action_type as campaign_type, banner, campaign_start_date, Cast(time_utc as timestamp) as time_utc, Date(CAST(time_utc as timestamp)) as date_utc, 1 as spend
FROM `wx-bq-poc.digital_attribution_modelling.safari_activation_purchase_temp`
where action_type = 'ACTIVATE' OR (action_type = 'PURCHASE' AND purchase_idx = '1')
)
/*/


--- left join table (to da_event) of online and activation flag (not used anymore) ---

CREATE table IF NOT EXISTS `wx-bq-poc.digital_attribution_modelling.da_label_super` as 
(
SELECT crn, time_utc, date_utc,
max(online_flag) as online_flag,
sum(online_spend) as online_spend,
max(activation_flag) as activation_flag,
sum(activation_spend) as activation_spend
FROM 
(
(SELECT distinct crn, time_utc, date_utc,
0 as online_flag,
0 as online_spend, 
0 as activation_flag,
0 as activation_spend
from `wx-bq-poc.digital_attribution_modelling.da_event`
where banner = 'supermarkets'
) 

UNION ALL

(SELECT crn, time_utc, date_utc,
1 as online_flag, 
sum(spend) as online_spend,
0 as activation_flag,
0 as activation_spend
FROM `wx-bq-poc.digital_attribution_modelling.da_online_sales`
where crn in (select distinct crn from `wx-bq-poc.digital_attribution_modelling.da_event`)
and date_utc >= (select min(date_utc) from `wx-bq-poc.digital_attribution_modelling.da_event`)
and date_utc <= (select max(date_utc) from `wx-bq-poc.digital_attribution_modelling.da_event`)
group by 1,2,3
)

UNION ALL

(SELECT crn, time_utc, date_utc, 
0 as online_flag,
0 as online_spend, 
1 as activation_flag, 
sum(spend) as activation_spend 
FROM `wx-bq-poc.digital_attribution_modelling.da_activation`
where banner = 'supermarkets' and 
crn in (select distinct crn from `wx-bq-poc.digital_attribution_modelling.da_event`)
and date_utc >= (select min(date_utc) from `wx-bq-poc.digital_attribution_modelling.da_event`)
and date_utc <= (select max(date_utc) from `wx-bq-poc.digital_attribution_modelling.da_event`)
group by 1,2,3)
)

group by 1,2,3
)


--- positional data for markov chain (not used) ---

Create table `wx-bq-poc.digital_attribution_modelling.da_markov_7mw` as 
(
	WITH crn_date_lookup AS 
	(
		WITH dates AS 
		(
			SELECT min(date_utc) as from_date, max(date_utc) as to_date 
			FROM 
			(
				SELECT distinct date_utc from `wx-bq-poc.digital_attribution_modelling.da_activation`
				UNION DISTINCT 
				SELECT distinct date_utc from `wx-bq-poc.digital_attribution_modelling.da_online_sales`
			)
		)
		
		SELECT crn, date_utc as ref_dt
		FROM
		(
			SELECT distinct crn FROM `wx-bq-poc.digital_attribution_modelling.da_event` where date_utc BETWEEN (SELECT from_date from dates) AND (SELECT to_date from dates)
		)
		CROSS JOIN
		(
			SELECT date_utc
			FROM UNNEST(GENERATE_DATE_ARRAY((SELECT from_date from dates), (SELECT to_date from dates), INTERVAL 1 DAY)) AS date_utc
		)
		
	)

	SELECT a.crn, ref_dt, time_utc as event_time, banner, channel_event
	FROM
	(
		(
			select * from crn_date_lookup
		) a
		
		left join 
		(
			SELECT distinct crn, time_utc, date_utc, banner, channel_event 
			from `wx-bq-poc.digital_attribution_modelling.da_event`
			
			union all
			
			SELECT distinct crn, time_utc, date_utc, banner, 'activation' as channel_event
			FROM 
			(
				SELECT *, rank() over (partition by crn, banner, date_utc order by time_utc asc) as event_rank 
				FROM `wx-bq-poc.digital_attribution_modelling.da_activation`
			) where event_rank = 1
			
			union all
			
			SELECT distinct crn, time_utc, date_utc, 'supermarkets' as banner, 'online_spend' as channel_event
			FROM 
			(
				SELECT *, rank() over (partition by crn, date_utc order by time_utc asc) as event_rank 
				FROM `wx-bq-poc.digital_attribution_modelling.da_online_sales`
			) where event_rank = 1	
		) b on a.crn = b.crn and date_utc BETWEEN DATE_SUB(ref_dt, INTERVAL 7 DAY) AND ref_dt)
	where time_utc is not null
) 




--- old logic for da_dx_event (not used) ---

CREATE table IF NOT EXISTS `wx-bq-poc.digital_attribution_modelling.da_dx_event` as 
(
	SELECT crn, session_id, dx_event, 
	case 
	when dx_event = 'dx_view' then concat(dx_event, ' - ', lower(referrer_source)
	when dx_event = 'dx_clk' then concat(dx_event, ' - ', lower(asset_type))
	else null end as channel_event,
	min(time_utc) as time_utc, 
	Date(min(time_utc)) as date_utc
	FROM 
	(
		SELECT *, 
			case 
			when channel in ('dx') AND event_name in('page view', 'search view') then 'dx_view'
			when channel in ('dx') AND event_name in('click view') then 'dx_clk'
			else 'other' end as dx_event, 
			REPLACE(json_extract(attributes, "$.udo_tealium_session_id[0]"), "\"", "") as session_id, 
			REPLACE(json_extract(attributes, "$.referrer_source[0]"), "\"", "") as referrer_source,
			REPLACE(json_extract(attributes, "$.asset_type[0]"), "\"", "") as asset_type,
			rank() over (partition by crn, json_extract(attributes, "$.udo_tealium_session_id[0]") order by time_utc asc) as event_rank
		FROM `wx-bq-poc.wx_lty_digital_attribution_dev.eventstore_events`
	)
	where banner = 'supermarkets'  and ((dx_event='dx_view' and referrer_source is not null) or (dx_event='dx_clk' and asset_type is not null))
	group by 1,2,3,4 
)

