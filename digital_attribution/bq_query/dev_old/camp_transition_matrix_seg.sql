DROP TABLE IF EXISTS `digital_attribution_modelling.dacamp_trans_matrix_seg`;
CREATE TABLE `digital_attribution_modelling.dacamp_trans_matrix_seg` as
(
	with states as 
	(
		with base as 
		(
			SELECT banner, campaign_code, campaign_start_date, campaign_end_date, segment,
				case when channel_event_pre is null then 'START' else channel_event_pre end as state1,
				channel_event as state2,
				case when channel_event_post is null then 'NO CONV' else channel_event_post end as state3,
				count(*) as cnt
			FROM
			(
				(
					SELECT a.crn, banner, campaign_code, campaign_start_date, campaign_end_date, time_utc,
					 case when segment is null then 'None' else segment end as segment,
					lead(channel_event) over (partition by a.crn, a.banner, a.campaign_code, a.campaign_start_date, a.campaign_end_date order by a.time_utc desc) as channel_event_pre,
					channel_event, 
					lag(channel_event) over (partition by a.crn, a.banner, a.campaign_code, a.campaign_start_date, a.campaign_end_date order by a.time_utc desc) as channel_event_post,
					row_number() over (partition by a.crn, a.banner, a.campaign_code, a.campaign_start_date, a.campaign_end_date order by a.time_utc desc) as pos
					FROM
					(
						SELECT banner, campaign_code, campaign_start_date, campaign_end_date, crn, time_utc,  channel_event FROM `wx-bq-poc.digital_attribution_modelling.dacamp_event_mw` where campaign_code <> 'ONLINE'

						UNION ALL

						SELECT banner, campaign_code, campaign_start_date, campaign_end_date, crn, min(time_utc) as time_utc, 'CONV' as channel_event
						FROM `wx-bq-poc.digital_attribution_modelling.dacamp_activation`
						group by 1,2,3,4,5		
					) a 
					left join
					(
						SELECT distinct crn, segment
						FROM `wx-bq-poc.digital_attribution_modelling.da_segment` 
					) c on a.crn = c.crn
			    )
				
				union all
				
				(
					SELECT a.crn, 'supermarkets' as banner, 'ONLINE' as campaign_code, cast('2099-01-01' as date) as campaign_start_date, cast('2099-01-01' as date) campaign_end_date, time_utc,
					case when segment is null then 'None' else segment end as segment,
					 lead(channel_event) over (partition by a.crn order by time_utc desc) as channel_event_pre,
					 channel_event, 
					 lag(channel_event) over (partition by a.crn order by time_utc desc) as channel_event_post,
					 row_number() over (partition by a.crn order by time_utc desc) as pos
					FROM
					(
						SELECT crn, time_utc, channel_event FROM `wx-bq-poc.digital_attribution_modelling.dacamp_event_mw` where campaign_code = 'ONLINE'

						UNION ALL

						SELECT crn, min(time_utc) as time_utc, 'CONV' as channel_event
						FROM `wx-bq-poc.digital_attribution_modelling.dacamp_online_sales`
						group by 1
					) a 
					left join
					(
						SELECT distinct crn, segment
						FROM `wx-bq-poc.digital_attribution_modelling.da_segment` 
					) c on a.crn = c.crn
				)
				
			)
			where (channel_event_pre <> 'CONV' or channel_event_pre is null) AND not (channel_event = 'CONV' and channel_event_pre is null)
			group by 1,2,3,4,5,6,7,8
	    )

		SELECT banner, segment, campaign_code, campaign_start_date, campaign_end_date, state1, state2, sum(cnt) as cnt
		FROM 
		(
			SELECT banner, segment, campaign_code, campaign_start_date, campaign_end_date, state1, state2, cnt from base
			UNION ALL
			SELECT banner, segment, campaign_code, campaign_start_date, campaign_end_date, state2, state3, cnt from base where state3 = 'NO CONV' AND state2 <> 'CONV'
		)
		group by 1,2,3,4,5,6,7
	)

	SELECT a.banner, a.campaign_code, a.campaign_start_date, a.campaign_end_date, a.segment,  a.state1, a.state2, a.cnt, a.cnt/b.cnt_state1 as prob
	FROM
	(
		(
			SELECT * FROM states
		) a
		left join
		(
			SELECT banner, segment, campaign_code, campaign_start_date, campaign_end_date, state1, sum(cnt) as cnt_state1 
			from states 
			group by 1,2,3,4,5,6
		) b on a.banner = b.banner and a.campaign_code = b.campaign_code and a.campaign_start_date = b.campaign_start_date and a.campaign_end_date = b.campaign_end_date and a.state1 = b.state1 and a.segment = b.segment
	)

);
