---- safari activation (old) ----

drop table loyalty_modeling.sy_safari_activation_new;

create table loyalty_modeling.sy_safari_activation_new as
(
select 
	  -- core fields --
	  crn
	  ,action_timestamp
	  ,convert_timezone('AEST', 'UTC', action_timestamp) as time_utc
	  ,cast(action_timestamp as date) as date
	  ,banner  
	  
	  -- attributes --
	  ,campaign_code
      ,campaign_start_date
      ,offer_type
	  ,action_type
      ,camp_dur_days
	  ,basket_key
	  ,purchase_idx

from (
select 
	  saf002.crn
	  ,case when sup_flag = 'Y'  then 'supermarkets' 
	   		when bws_flag = 'Y'  then 'bws'
            when bigw_flag = 'Y' then 'bigw'
       	    when fco_flag = 'Y'  then 'fuelco'
            when ctx_flag = 'Y'  then 'caltex'  
			else 'other' end as banner
      ,saf002.campaign_code
      ,saf002.campaign_start_date
      ,saf001.offer_type
      ,saf001.camp_dur_days
	  ,saf002.action_type
      ,saf002.action_timestamp
	  ,saf002.basket_key
	  ,null as purchase_idx
from   loyalty_bi_analytics.saf001_campaign_master saf001
inner join   loyalty_bi_analytics.saf002_fact saf002
on     saf001.campaign_code = saf002.campaign_code
and    saf001.campaign_start_date = saf002.campaign_start_date
and    saf001.campaign_name = saf002.campaign_name
where  saf001.activation_flag = 'Y'
and    saf002.action_type = 'ACTIVATE'

union all

select saf002.crn
	  ,case when saf002.division_name = 'SUP'  then 'supermarkets'
       		when saf002.division_name = 'BWS'  then 'bws'
       		when saf002.division_name = 'BIG' then 'bigw'
       		when saf002.division_name = 'FCO'  then 'fuelco'
       		when saf002.division_name = 'CTX'  then 'caltex'  else 'other' end as banner
      ,saf002.campaign_code
      ,saf002.campaign_start_date
      ,saf001.offer_type
      ,saf001.camp_dur_days
	  ,saf002.action_type
      ,saf002.action_timestamp
	  ,saf002.basket_key
	  ,case when saf002.purchase_idx = 1 then 1 else row_number() over (partition by saf002.campaign_name,saf002.division_name, saf002.crn order by action_timestamp) end as purchase_idx
from   loyalty_bi_analytics.saf001_campaign_master saf001
inner join   loyalty_bi_analytics.saf002_fact saf002
on     saf001.campaign_code = saf002.campaign_code
and    saf001.campaign_start_date = saf002.campaign_start_date
and    saf001.campaign_name = saf002.campaign_name
where  saf001.activation_flag = 'N'
and    saf002.action_type = 'PURCHASE'
)
where Date(time_utc) between Date('2019-09-30') and Date('2019-12-31')
);



