SELECT *, concat(banner, '_', campaign_code, '_', cast(campaign_start_date as string), '_', cast(campaign_end_date as string)) as key
FROM 
(
	SELECT a.banner, a.campaign_code, a.campaign_start_date, a.campaign_end_date,
		case when segment is null then 'None' else segment end as segment,
		sum(activation) as activation,
		case when sum(activation_spend) is null then 0 else sum(activation_spend) end as activation_spend
	FROM
	(
		(
		SELECT crn, banner, campaign_code, campaign_start_date, campaign_end_date, min(time_utc) as activation_time, 1 as activation, sum(spend) as activation_spend
		FROM `wx-bq-poc.digital_attribution_modelling.dacamp_activation`
		group by 1,2,3,4,5
		) a
		inner join
		(
			SELECT distinct crn, banner, campaign_code, campaign_start_date, campaign_end_date
			FROM `wx-bq-poc.digital_attribution_modelling.dacamp_event_mw`
		) b on a.crn = b.crn and a.banner = b.banner and a.campaign_code = b.campaign_code and a.campaign_start_date = b.campaign_start_date and a.campaign_end_date = b.campaign_end_date
		left join
		(
			SELECT distinct crn, segment
			FROM `wx-bq-poc.digital_attribution_modelling.da_segment` 
		) c on a.crn = c.crn
	)
	group by 1,2,3,4,5
)