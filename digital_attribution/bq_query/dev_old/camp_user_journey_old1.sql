
--------------------------------------------USER JOURNEY----------------------------------------------------
/* 
Drop table if exists `wx-bq-poc.digital_attribution_modelling.dacamp_event_mw`;
Create table `wx-bq-poc.digital_attribution_modelling.dacamp_event_mw` as 
(
	SELECT a.*, b.crn, b.time_utc, 
	case when SUBSTR(b.campaign_code,1,8) = SUBSTR(a.campaign_code,1,8) then b.channel_event else CONCAT("_OTH_", b.channel_event) end as channel_event, b.campaign_code as event_campaign
	FROM
	(
		(
			select distinct banner, campaign_code, campaign_start_date, campaign_end_date 
			from `wx-bq-poc.digital_attribution_modelling.dacamp_activation`
		) a
		
		inner join 
		(
			SELECT distinct banner, campaign_code, crn, time_utc, date_utc, date, channel_event 
			from `wx-bq-poc.digital_attribution_modelling.dacamp_event`
		) b on a.banner = b.banner and b.date BETWEEN a.campaign_start_date AND a.campaign_end_date
		
		inner join
		(
			SELECT distinct crn, campaign_code, banner from `wx-bq-poc.digital_attribution_modelling.dacamp_event` 
		) c on b.crn = c.crn and SUBSTR(a.campaign_code,1,8) = SUBSTR(c.campaign_code,1,8) and a.banner = c.banner
	)

	UNION ALL
	
	SELECT a.*, b.crn, b.time_utc, b.channel_event
	FROM
	(
		(
			select 'supermarkets' as banner, 'ONLINE' as campaign_code, min(date_utc) as campaign_start_date, max(date_utc) as campaign_end_date 
			from `wx-bq-poc.digital_attribution_modelling.dacamp_online_sales`
			group by 1
		) a
		
		inner join 
		(
			SELECT distinct banner, crn, time_utc, date_utc, date, channel_event 
			from `wx-bq-poc.digital_attribution_modelling.dacamp_event`
		) b on a.banner = b.banner and b.date BETWEEN DATE_SUB(a.campaign_start_date, INTERVAL 7 DAY) AND a.campaign_end_date
	)
);
 */
 
Drop table if exists `wx-bq-poc.digital_attribution_modelling.dacamp_event_mw`;
Create table `wx-bq-poc.digital_attribution_modelling.dacamp_event_mw` as 
(
	SELECT banner, campaign_code, campaign_start_date, campaign_end_date, crn, time_utc, channel_event
	FROM
	(
		SELECT a.*, lead(channel_event) over (partition by a.crn, a.banner, a.campaign_code, a.campaign_start_date, a.campaign_end_date order by a.time_utc desc) as channel_event_pre
		FROM
		(
			SELECT a.*, b.crn, b.time_utc, CONCAT(a.campaign_code, '_', b.channel_event) as channel_event
			FROM
			(
				(
					select distinct banner, campaign_code, campaign_start_date, campaign_end_date 
					from `wx-bq-poc.digital_attribution_modelling.dacamp_activation` 
				) a
				
				inner join 
				(
					SELECT distinct banner, campaign_code, crn, time_utc, date_utc, date, channel_event 
					from `wx-bq-poc.digital_attribution_modelling.dacamp_event`
					where campaign_code is not null 
				) b on a.banner = b.banner 
						and b.date BETWEEN a.campaign_start_date AND a.campaign_end_date
						and SUBSTR(b.campaign_code,1,8) = SUBSTR(a.campaign_code,1,8)
						
				inner join
				(
					SELECT distinct crn, campaign_code, banner from `wx-bq-poc.digital_attribution_modelling.dacamp_event` 
				) c on b.crn = c.crn and SUBSTR(a.campaign_code,1,8) = SUBSTR(c.campaign_code,1,8) and a.banner = c.banner
			)
			
			union all
			
			SELECT a.*, b.crn, b.time_utc, CONCAT(b.campaign_code, '_', b.channel_event) as channel_event
			FROM
			(
				(
					select distinct banner, campaign_code, campaign_start_date, campaign_end_date 
					from `wx-bq-poc.digital_attribution_modelling.dacamp_activation`
				) a
				
				inner join 
				(
					SELECT distinct banner, campaign_code, crn, time_utc, date_utc, date, channel_event 
					from `wx-bq-poc.digital_attribution_modelling.dacamp_event`
					where campaign_code is not null and concat(banner,'_',crn, '_', SUBSTR(campaign_code,1,8)) not in (SELECT distinct concat(banner,'_',crn,'_',SUBSTR(campaign_code,1,8)) from `wx-bq-poc.digital_attribution_modelling.dacamp_activation`)
				) b on a.banner = b.banner 
						and b.date BETWEEN a.campaign_start_date AND a.campaign_end_date
						and SUBSTR(b.campaign_code,1,8) <> SUBSTR(a.campaign_code,1,8) 
				
				inner join
				(
					SELECT distinct crn, campaign_code, banner from `wx-bq-poc.digital_attribution_modelling.dacamp_event` 
				) c on b.crn = c.crn and SUBSTR(a.campaign_code,1,8) = SUBSTR(c.campaign_code,1,8) and a.banner = c.banner
			)

			union all
			
			SELECT a.*, b.crn, b.time_utc, CONCAT("_OTH_", b.channel_event) as channel_event
			FROM
			(
				(
					select distinct banner, campaign_code, campaign_start_date, campaign_end_date 
					from `wx-bq-poc.digital_attribution_modelling.dacamp_activation`
				) a
				
				inner join 
				(
					SELECT distinct banner, campaign_code, crn, time_utc, date_utc, date, channel_event 
					from `wx-bq-poc.digital_attribution_modelling.dacamp_event`
					where campaign_code is null 
				) b on a.banner = b.banner and b.date BETWEEN a.campaign_start_date AND a.campaign_end_date
				
				inner join
				(
					SELECT distinct crn, campaign_code, banner from `wx-bq-poc.digital_attribution_modelling.dacamp_event` 
				) c on b.crn = c.crn and SUBSTR(a.campaign_code,1,8) = SUBSTR(c.campaign_code,1,8) and a.banner = c.banner
			)

		) a 
		
		left join	
		(
			select distinct crn, time_utc as activation_time, banner, campaign_code, campaign_start_date, campaign_end_date
			from `wx-bq-poc.digital_attribution_modelling.dacamp_activation` 
		) b on a.crn = b.crn and a.banner = b.banner and a.campaign_code = b.campaign_code and a.campaign_start_date = b.campaign_start_date and a.campaign_end_date = b.campaign_end_date
    
		where activation_time is null or time_utc <= activation_time
		
		
		union all
		
		
		SELECT a.*, lead(channel_event) over (partition by a.crn, a.banner, a.campaign_code, a.campaign_start_date, a.campaign_end_date order by a.time_utc desc) as channel_event_pre
		FROM
		(
			SELECT a.*, b.crn, b.time_utc, CONCAT(b.campaign_code, '_', b.channel_event) as channel_event
			FROM
			(
				(
					select 'supermarkets' as banner, 'ONLINE' as campaign_code, min(date_utc) as campaign_start_date, max(date_utc) as campaign_end_date 
					from `wx-bq-poc.digital_attribution_modelling.dacamp_online_sales`
					group by 1
				) a
				
				inner join 
				(
					SELECT distinct banner, crn, time_utc, date_utc, date, 
					case when campaign_code is null then '_OTH' else campaign_code end as campaign_code, 
					channel_event 
					from `wx-bq-poc.digital_attribution_modelling.dacamp_event`
				) b on a.banner = b.banner and b.date BETWEEN DATE_SUB(a.campaign_start_date, INTERVAL 7 DAY) AND a.campaign_end_date
			)
		) a 
		
		left join 
		(
			select distinct crn, time_utc as online_time
			from `wx-bq-poc.digital_attribution_modelling.dacamp_activation` 
		) b on a.crn = b.crn 
    
		where online_time is null or time_utc <= online_time
		
		
	) where channel_event_pre <> channel_event or channel_event_pre is null
);





--- user journey data (sankey chart) ---
/* 
DROP TABLE IF EXISTS `wx-bq-poc.digital_attribution_modelling.dacamp_pos_mw_activation`;
CREATE table IF NOT EXISTS `wx-bq-poc.digital_attribution_modelling.dacamp_pos_mw_activation` as 
(
	with base as
	(
		SELECT a.*, 
		row_number() over (partition by a.crn, a.banner, a.campaign_code, a.campaign_start_date, a.campaign_end_date order by time_utc desc) as pos,
		activation_flag, activation_time, activation_spend
		FROM
		(
			SELECT * FROM `wx-bq-poc.digital_attribution_modelling.dacamp_event_mw` where campaign_code <> 'ONLINE'
		) a 

		LEFT JOIN

		(
			SELECT crn, banner, campaign_code, campaign_start_date, campaign_end_date, min(time_utc) as activation_time, 1 as activation_flag, sum(spend) as activation_spend
			FROM `wx-bq-poc.digital_attribution_modelling.dacamp_activation`
			group by 1,2,3,4,5
		) b on a.crn = b.crn and a.banner = b.banner and a.campaign_code = b.campaign_code and a.campaign_start_date = b.campaign_start_date and a.campaign_end_date = b.campaign_end_date
		where time_utc < activation_time or activation_time is null
	)

	SELECT crn, banner, campaign_code, campaign_start_date, campaign_end_date, activation_flag, activation_spend,
		max(pos_1) as pos_1, 
		max(pos_2) as pos_2, 
		max(pos_3) as pos_3, 
		max(pos_4) as pos_4, 
		max(pos_5) as pos_5, 
		max(pos_6) as pos_6, 
		max(pos_7) as pos_7, 
		max(pos_8) as pos_8, 
		max(pos_9) as pos_9, 
		max(pos_10) as pos_10,
		max(pos_11) as pos_11, 
		max(pos_12) as pos_12, 
		max(pos_13) as pos_13, 
		max(pos_14) as pos_14, 
		max(pos_15) as pos_15, 
		max(pos_16) as pos_16, 
		max(pos_17) as pos_17, 
		max(pos_18) as pos_18, 
		max(pos_19) as pos_19, 
		max(pos_20) as pos_20
	FROM
	(
		SELECT crn, banner, campaign_code, campaign_start_date, campaign_end_date, 
			case when activation_flag is null then 0 else 1 end as activation_flag,
			case when activation_spend is null then 0 else activation_spend end as activation_spend,
			case when pos = 1 then channel_event else null end as pos_1,
			case when pos = 2 then channel_event else null end as pos_2,
			case when pos = 3 then channel_event else null end as pos_3,
			case when pos = 4 then channel_event else null end as pos_4,
			case when pos = 5 then channel_event else null end as pos_5,
			case when pos = 6 then channel_event else null end as pos_6,
			case when pos = 7 then channel_event else null end as pos_7,
			case when pos = 8 then channel_event else null end as pos_8,
			case when pos = 9 then channel_event else null end as pos_9,
			case when pos = 10 then channel_event else null end as pos_10,
			case when pos = 11 then channel_event else null end as pos_11,
			case when pos = 12 then channel_event else null end as pos_12,
			case when pos = 13 then channel_event else null end as pos_13,
			case when pos = 14 then channel_event else null end as pos_14,
			case when pos = 15 then channel_event else null end as pos_15,
			case when pos = 16 then channel_event else null end as pos_16,
			case when pos = 17 then channel_event else null end as pos_17,
			case when pos = 18 then channel_event else null end as pos_18,
			case when pos = 19 then channel_event else null end as pos_19,
			case when pos = 20 then channel_event else null end as pos_20
		FROM base
	)
	group by 1,2,3,4,5,6,7
);
 */
