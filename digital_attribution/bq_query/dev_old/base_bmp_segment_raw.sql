drop table if exists `wx-bq-poc.digital_attribution_modelling.da_bmp_segment`;

create table `wx-bq-poc.digital_attribution_modelling.da_bmp_segment` as
(
	SELECT crn, max(affluence) as bmp_segment
	FROM `wx-bq-poc.wx_lty_digital_attribution_dev.redx_loyalty_customer_value_model`
	where Date(pw_end_date_utc) = DATE_SUB(Date(DATE_TO_), INTERVAL 1 DAY)
	group by 1
);