
--------------------------------------------USER JOURNEY----------------------------------------------------

Drop table if exists `wx-bq-poc.digital_attribution_modelling.dacamp_event_mw`;
Create table `wx-bq-poc.digital_attribution_modelling.dacamp_event_mw` as 
(
	with base_act as 
	(
		SELECT a.*
				, b.crn
				, b.date
				, b.time_utc
				, CONCAT(b.campaign_code, '_', b.channel_event) as channel_event
				, b.campaign_code as cc_event
				, d.conv_time
				, d.conv_flag
				, d.spend
				, lead(CONCAT(b.campaign_code, '_', b.channel_event)) over (partition by b.crn, a.banner, a.campaign_code, a.campaign_start_date, a.campaign_end_date order by b.time_utc desc) as channel_event_pre
				, case when d.conv_time is null then a.campaign_end_date else Date(d.conv_time, 'Australia/Sydney') end as end_date 
		FROM
		(
			(
				select distinct banner
							, campaign_code, campaign_start_date, campaign_end_date 
				from `wx-bq-poc.digital_attribution_modelling.dacamp_activation`
			) a
			
			inner join 
			(
				SELECT distinct banner, campaign_code, crn, time_utc, date, channel_event 
				from `wx-bq-poc.digital_attribution_modelling.dacamp_event`
			) b on a.banner = b.banner 
					and b.date BETWEEN a.campaign_start_date AND a.campaign_end_date
					
			inner join
			(
				SELECT distinct crn, campaign_code, banner from `wx-bq-poc.digital_attribution_modelling.dacamp_event` 
			) c on b.crn = c.crn 
					and a.campaign_code = c.campaign_code
					and a.banner = c.banner
			
			left join
			(
				select crn, banner, campaign_code, campaign_start_date, campaign_end_date, min(time_utc) as conv_time, 1 as conv_flag, sum(spend) as spend
				from `wx-bq-poc.digital_attribution_modelling.dacamp_activation`
				group by 1,2,3,4,5
			) d on b.crn = d.crn 
					and a.banner = d.banner 
					and a.campaign_code = d.campaign_code and a.campaign_start_date = d.campaign_start_date and a.campaign_end_date = d.campaign_end_date
		)
		where d.conv_time is null or b.time_utc <= d.conv_time 
	),
	
	base_onl as 
	(
		SELECT a.*
				, b.crn
				, b.date
				, b.time_utc
				, CONCAT(b.campaign_code, '_', b.channel_event) as channel_event
				, b.campaign_code as cc_event
				, c.conv_time
				, c.conv_flag
				, c.spend
				, lead(CONCAT(b.campaign_code, '_', b.channel_event)) over (partition by b.crn, a.banner, a.campaign_code, a.campaign_start_date, a.campaign_end_date order by b.time_utc desc) as channel_event_pre
				, case when c.conv_time is null then a.campaign_end_date else Date(c.conv_time, 'Australia/Sydney') end as end_date 
		FROM
		(
			(
				select 'supermarkets' as banner, 'ONLINE' as campaign_code, min(date_utc) as campaign_start_date, max(date_utc) as campaign_end_date 
				from `wx-bq-poc.digital_attribution_modelling.dacamp_online_sales`
				group by 1
			) a
			
			inner join 
			(
				SELECT distinct banner, crn, time_utc, date, campaign_code, channel_event 
				from `wx-bq-poc.digital_attribution_modelling.dacamp_event`
			) b on a.banner = b.banner and b.date BETWEEN DATE_SUB(a.campaign_start_date, INTERVAL 6 DAY) AND a.campaign_end_date
			
			left join 
			(
				select crn, min(time_utc) as conv_time, 1 as conv_flag, sum(spend) as spend
				from `wx-bq-poc.digital_attribution_modelling.dacamp_online_sales`
				group by 1
			) c on b.crn = c.crn 
		)
		where (c.conv_time is null and b.date between a.campaign_start_date AND a.campaign_end_date) 
				or (c.conv_time is not null and b.time_utc <= c.conv_time and b.date >= date_sub(Date(c.conv_time, "Australia/Sydney"), INTERVAL 6 DAY))
	),

	base as 
	(
		SELECT banner, campaign_code, campaign_start_date, campaign_end_date, crn, time_utc, date, channel_event, conv_time, conv_flag, spend, cc_event
		FROM
		(
			SELECT *
			FROM base_act
			
			UNION ALL
			
			SELECT *
			FROM base_onl
		) 
		where (channel_event_pre <> channel_event or channel_event_pre is null)
				and (
						SUBSTR(cc_event,1,8) = campaign_code -- same campaign events
						or SUBSTR(cc_event,1,8) <> campaign_code and channel_event like '%clk%' -- click events of other campaign
						or ((SUBSTR(cc_event,1,8) in ('CVM-1661') and channel_event like '%open%') or channel_event like '%imp%') and date >= date_sub(end_date, INTERVAL 1 Day) -- imp events of TTL campaigns
					)
	)
	
	
	SELECT banner, campaign_code, campaign_start_date, campaign_end_date, crn, time_utc, channel_event, conv_time, conv_flag, spend
	from
	(
		SELECT a.*, b.campaign_start_date as csd_event, b.campaign_end_date as ced_event, conv_time_event,
				row_number() over (partition by a.banner, a.campaign_code, a.campaign_start_date, a.campaign_end_date, a.crn, a.time_utc, a.channel_event 
									order by abs(DATE_DIFF(a.date, b.campaign_start_date, DAY)) + abs(DATE_DIFF(b.campaign_end_date, a.date, DAY))) as pos
		FROM
		(
			SELECT * from base
			where campaign_code <> 'ONLINE' and channel_event like '%email%' and cc_event not like '%OTH%'
		) a
		inner join 
		(
			SELECT distinct banner, campaign_code, campaign_start_date, campaign_end_date 
			from `wx-bq-poc.digital_attribution_modelling.dacamp_activation_list`
		) b on a.banner = b.banner and a.cc_event = b.campaign_code
		left join
		(
			SELECT crn, banner, campaign_code, campaign_start_date, campaign_end_date, min(time_utc) as conv_time_event 
			from `wx-bq-poc.digital_attribution_modelling.dacamp_activation_list`
			group by 1,2,3,4,5
		) c on b.banner = c.banner and b.campaign_code = c.campaign_code and b.campaign_start_date = c.campaign_start_date and b.campaign_end_date = c.campaign_end_date and a.crn = c.crn
	)
	where pos = 1 
			and ((campaign_code = SUBSTR(cc_event,1,8) and campaign_end_date = ced_event) 
					or (campaign_code <> SUBSTR(cc_event,1,8) and conv_time_event is null and ced_event > campaign_start_date)) 
			
	
	union all
	
 	SELECT banner, campaign_code, campaign_start_date, campaign_end_date, crn, time_utc, channel_event, conv_time, conv_flag, spend
	from base
	where ((campaign_code = 'ONLINE') or cc_event like '%OTH%' 
			or (channel_event not like '%email%' and date BETWEEN campaign_start_date AND campaign_end_date))
	
);


