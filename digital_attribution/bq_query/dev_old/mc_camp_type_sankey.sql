
select distinct 'Total' as segment, banner, campaign_code, campaign_type, campaign_start_date, campaign_end_date, concat('Total', '_', banner, '_', campaign_code, '_', cast(campaign_start_date as string), '_', cast(campaign_end_date as string)) as key 
from `digital_attribution_modelling.dacamp_prod_event_mw`
order by 1,2,3,4,5,6