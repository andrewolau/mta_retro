
--------------------------------------------USER JOURNEY----------------------------------------------------

 
Drop table if exists `wx-bq-poc.digital_attribution_modelling.dacamp_event_mw`;
Create table `wx-bq-poc.digital_attribution_modelling.dacamp_event_mw` as 
(

	SELECT banner, campaign_code, campaign_start_date, campaign_end_date, crn, time_utc, channel_event, conv_time, conv_flag, spend
	FROM
	(
		SELECT a.*, b.conv_time, b.conv_flag, b.spend, 
				lead(channel_event) over (partition by a.crn, a.banner, a.campaign_code, a.campaign_start_date, a.campaign_end_date order by a.time_utc desc) as channel_event_pre,
				case when conv_time is null then a.campaign_end_date else Date(conv_time, 'Australia/Sydney') end as end_date 
		FROM
		(
			SELECT a.*, b.crn, b.date, b.time_utc, CONCAT(a.campaign_code, '_', b.channel_event) as channel_event, b.campaign_code as cc_event
			FROM
			(
				(
					select distinct banner, campaign_code, campaign_start_date, campaign_end_date 
					from `wx-bq-poc.digital_attribution_modelling.dacamp_activation`
				) a
				
				inner join 
				(
					SELECT distinct banner, campaign_code, crn, time_utc, date, channel_event 
					from `wx-bq-poc.digital_attribution_modelling.dacamp_event`
					where campaign_code is not null 
				) b on a.banner = b.banner 
						and b.date BETWEEN a.campaign_start_date AND a.campaign_end_date
						and SUBSTR(b.campaign_code,1,8) = SUBSTR(a.campaign_code,1,8)
						
				inner join
				(
					SELECT distinct crn, campaign_code, banner from `wx-bq-poc.digital_attribution_modelling.dacamp_event` 
				) c on b.crn = c.crn and SUBSTR(a.campaign_code,1,8) = SUBSTR(c.campaign_code,1,8) and a.banner = c.banner
			)
			
			union all
			
			SELECT a.*, b.crn, b.date, b.time_utc, CONCAT(b.campaign_code, '_', b.channel_event) as channel_event, b.campaign_code as cc_event
			FROM
			(
				(
					select distinct banner, campaign_code, campaign_start_date, campaign_end_date 
					from `wx-bq-poc.digital_attribution_modelling.dacamp_activation`
				) a
				
				inner join 
				(
					SELECT distinct banner, campaign_code, crn, time_utc, date, channel_event 
					from `wx-bq-poc.digital_attribution_modelling.dacamp_event`
					where concat(banner,'_',crn, '_', SUBSTR(campaign_code,1,8)) not in (SELECT distinct concat(banner,'_',crn,'_',SUBSTR(campaign_code,1,8)) from `wx-bq-poc.digital_attribution_modelling.dacamp_activation`)
				) b on a.banner = b.banner 
						and b.date BETWEEN a.campaign_start_date AND a.campaign_end_date
						and SUBSTR(b.campaign_code,1,8) <> SUBSTR(a.campaign_code,1,8) 
						
				inner join
				(
					SELECT distinct crn, campaign_code, banner from `wx-bq-poc.digital_attribution_modelling.dacamp_event` 
				) c on b.crn = c.crn and SUBSTR(a.campaign_code,1,8) = SUBSTR(c.campaign_code,1,8) and a.banner = c.banner
			)

			union all
			
			SELECT a.*, b.crn, b.date, b.time_utc, CONCAT("_OTH_", b.channel_event) as channel_event, b.campaign_code as cc_event
			FROM
			(
				(
					select distinct banner, campaign_code, campaign_start_date, campaign_end_date 
					from `wx-bq-poc.digital_attribution_modelling.dacamp_activation`
				) a
				
				inner join 
				(
					SELECT distinct banner, campaign_code, crn, time_utc, date, channel_event 
					from `wx-bq-poc.digital_attribution_modelling.dacamp_event`
					where campaign_code is null 
				) b on a.banner = b.banner 
					and b.date BETWEEN a.campaign_start_date AND a.campaign_end_date
				
				inner join
				(
					SELECT distinct crn, campaign_code, banner from `wx-bq-poc.digital_attribution_modelling.dacamp_event` 
				) c on b.crn = c.crn and SUBSTR(a.campaign_code,1,8) = SUBSTR(c.campaign_code,1,8) and a.banner = c.banner
			)

		) a 
		
		left join
		(
			select crn, banner, campaign_code, campaign_start_date, campaign_end_date, min(time_utc) as conv_time, 1 as conv_flag, sum(spend) as spend
			from `wx-bq-poc.digital_attribution_modelling.dacamp_activation`
			group by 1,2,3,4,5
		) b on a.crn = b.crn and a.banner = b.banner and SUBSTR(a.campaign_code,1,8) = SUBSTR(b.campaign_code,1,8) and a.campaign_start_date = b.campaign_start_date and a.campaign_end_date = b.campaign_end_date
    
		where conv_time is null or time_utc <= conv_time 
		
		
		union all
		
		
		SELECT banner, campaign_code, campaign_start_date, campaign_end_date, crn, date, time_utc, channel_event, cc_event, conv_time, conv_flag, spend,  
				lead(channel_event) over (partition by crn, banner, campaign_code, campaign_start_date, campaign_end_date order by time_utc desc) as channel_event_pre,
				case when conv_time is null then campaign_end_date else Date(conv_time, 'Australia/Sydney') end as end_date 
		FROM
		(
			SELECT a.*, b.crn, b.date, b.time_utc, c.conv_time, c.conv_flag, c.spend, CONCAT(b.campaign_code, '_', b.channel_event) as channel_event, b.campaign_code as cc_event
			FROM
			(
				(
					select 'supermarkets' as banner, 'ONLINE' as campaign_code, min(date_utc) as campaign_start_date, max(date_utc) as campaign_end_date 
					from `wx-bq-poc.digital_attribution_modelling.dacamp_online_sales`
					group by 1
				) a
				
				inner join 
				(
					SELECT distinct banner, crn, time_utc, date, 
					case when campaign_code is null then '_OTH' else campaign_code end as campaign_code, 
					channel_event 
					from `wx-bq-poc.digital_attribution_modelling.dacamp_event`
				) b on a.banner = b.banner
						and b.date BETWEEN DATE_SUB(a.campaign_start_date, INTERVAL 6 DAY) AND a.campaign_end_date
				
				left join 
				(
					select crn, min(time_utc) as conv_time, 1 as conv_flag, sum(spend) as spend
					from `wx-bq-poc.digital_attribution_modelling.dacamp_online_sales`
					group by 1
				) c on b.crn = c.crn 
				
			) 
			where (c.conv_time is null and b.date between a.campaign_start_date AND a.campaign_end_date) 
						or (c.conv_time is not null and b.time_utc <= c.conv_time and b.date >= date_sub(Date(c.conv_time, "Australia/Sydney"), INTERVAL 6 DAY))
		)
	) 
	where (channel_event_pre <> channel_event or channel_event_pre is null)
				and not (SUBSTR(cc_event,1,8) <> SUBSTR(campaign_code,1,8) and channel_event not like '%clk%' and date < date_sub(end_date, INTERVAL 2 Day))
				
);
