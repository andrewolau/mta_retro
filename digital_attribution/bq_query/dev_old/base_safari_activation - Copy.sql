---- safari activation ----
drop table if exists loyalty_modeling.sy_safarievents_final;

create table loyalty_modeling.sy_safarievents_final as
(
	with base as 
	(	
		select *
		, case when (min(open_datetime) OVER (PARTITION BY crn, division_name, campaign_code, campaign_start_date, campaign_duration_weeks)) is not null then True else False end AS opened_crn
	    , case when (min(purchase_datetime) OVER (PARTITION BY crn, division_name, campaign_code, campaign_start_date, campaign_duration_weeks)) is not null then True else False end AS purchased_crn
	    , case when (min(activation_datetime) OVER (PARTITION BY crn, division_name, campaign_code, campaign_start_date, campaign_duration_weeks)) is not null then True else False end AS activated_crn
		, case when (min(redemption_datetime) OVER (PARTITION BY crn, division_name, campaign_code, campaign_start_date, campaign_duration_weeks)) is not null then True else False end AS redeemed_crn
		, case when (min(activation_datetime) OVER (PARTITION BY division_name, campaign_code, campaign_start_date, campaign_duration_weeks)) is not null and campaign_preactivated = False then True else False end AS campaign_activatable
		, case when (sum(tpg_amt) OVER (PARTITION BY division_name, campaign_code, campaign_start_date, campaign_duration_weeks))>0 then True else False end AS campaign_tpg_amt
		from 
		(
			SELECT * from loyalty_bi_analytics.safarievents_final_20200525 where campaign_start_date = '2020-05-27'::date
			
			union all
			
			SELECT * from loyalty_bi_analytics.safarievents_final_20200601 where campaign_start_date = '2020-06-03'::date
			
			union all
			
			SELECT * from loyalty_bi_analytics.safarievents_final_20200608 where campaign_start_date = '2020-06-10'::date
			
			union all
			
			SELECT * from loyalty_bi_analytics.safarievents_final_20200615 where campaign_start_date = '2020-06-17'::date
		)	
			
		where  audience_type = 'T'
	)
	
	select 
		crn
		,case when division_name = 'SUPERS'  then 'supermarkets' 
			when division_name = 'BWS' then 'bws'
			when division_name = 'BIGW'  then 'bigw'
			when division_name = 'FUELCO' then 'fuelco'
			when division_name = 'CALTEX'  then 'caltex'  
			else 'other' end as banner
		,campaign_code
		, case when campaign_code in ('CVM-1661', 'BCV-1076') then True else campaign_ttl end as campaign_ttl
		,campaign_tpg_amt
		,campaign_activatable
		,campaign_start_date
		,DATEADD(DAY, campaign_duration_weeks*7-1, campaign_start_date) as campaign_end_date
		,min(case when campaign_activatable = True then convert_timezone('AEST', 'UTC', activation_datetime) else convert_timezone('AEST', 'UTC', purchase_datetime) end) as time_utc
		,min(case when campaign_activatable = True then Date(activation_datetime) else Date(purchase_datetime) end) as date
		,sum(case when tpg_amt >0 then sales_amt else 0 end) as sales_amt_tpg
		,sum(sales_amt) as sales_amt
	from base
	--where 	
			--opened_crn = True
			--not (campaign_activatable = False and purchased_crn = False) 
			--and not (campaign_activatable = True and activated_crn = False)
			-- and campaign_end_date between date_trunc('week', getdate() + '1 day'::interval) - '7 day'::interval and date_trunc('week', getdate() + '1 day'::interval) - '1 day'::interval
	group by 1,2,3,4,5,6,7,8
);
	
	