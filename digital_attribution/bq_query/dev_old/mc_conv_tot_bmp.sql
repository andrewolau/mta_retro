SELECT a.banner, a.bmp_segment, a.activation,
case when a.activation_spend is null then 0 else a.activation_spend end as activation_spend,
case when b.online is null then 0 else b.online end as online,
case when b.online_spend is null then 0 else b.online_spend end as online_spend
FROM
(
	SELECT a.banner,
		case when bmp_segment is null then 'None' else bmp_segment end as bmp_segment,
		sum(activation) as activation,
		sum(activation_spend) as activation_spend
	FROM
	(
		(
			SELECT crn, banner, date_utc, min(time_utc) as activation_time, 1 as activation, sum(spend) as activation_spend
			FROM `wx-bq-poc.digital_attribution_modelling.da_activation`
			group by 1,2,3
		) a
		inner join
		(
			SELECT distinct crn, banner 
			FROM `wx-bq-poc.digital_attribution_modelling.da_event_mw`
		) b on a.crn = b.crn and a.banner = b.banner
		left join
		(
			SELECT distinct crn, bmp_segment
			FROM `wx-bq-poc.digital_attribution_modelling.da_bmp_segment` 
		) c on a.crn = c.crn
	)
	group by 1,2
) a 

LEFT JOIN
(
	SELECT a.banner, 
		case when bmp_segment is null then 'None' else bmp_segment end as bmp_segment,
		sum(online) as online,
		sum(online_spend) as online_spend
	FROM
	(
		(
			SELECT crn, 'supermarkets' as banner, date_utc, min(time_utc) as online_time, 1 as online, sum(spend) as online_spend
			FROM `wx-bq-poc.digital_attribution_modelling.da_online_sales`
			group by 1,2,3
		) a
		inner join
		(
			SELECT distinct crn, banner 
			FROM `wx-bq-poc.digital_attribution_modelling.da_event_mw`
		) b on a.crn = b.crn and a.banner = b.banner
		left join
		(
			SELECT distinct crn, bmp_segment
			FROM `wx-bq-poc.digital_attribution_modelling.da_bmp_segment` 
		) c on a.crn = c.crn
	)
	group by 1,2
) b on a.banner = b.banner and a.bmp_segment = b.bmp_segment
