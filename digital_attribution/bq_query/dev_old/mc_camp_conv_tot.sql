with onl_start_date as 
(	
	select max(campaign_start_date) from `wx-bq-poc.digital_attribution_modelling.dacamp_prod_event_mw` where campaign_code = 'ONLINE'
),

onl_end_date as 
(
	select max(campaign_end_date) from `wx-bq-poc.digital_attribution_modelling.dacamp_prod_event_mw` where campaign_code = 'ONLINE'
)

SELECT 'Total' as segment, banner, campaign_code, campaign_start_date, campaign_end_date,
	sum(conv_flag) as conv,
	case when sum(spend) is null then 0 else sum(spend) end as spend,
	concat('Total', '_', banner, '_', campaign_code, '_', cast(campaign_start_date as string), '_', cast(campaign_end_date as string)) as key FROM
	(
		SELECT crn, banner, campaign_code, campaign_start_date, campaign_end_date, 
		min(time_utc) as conv_time, 1 as conv_flag, sum(spend) as spend
		FROM `wx-bq-poc.digital_attribution_modelling.dacamp_prod_activation`
		group by 1,2,3,4,5
		
		union all
		
		SELECT crn, 'supermarkets' as banner, 'ONLINE' as campaign_code, 
		(select * from onl_start_date) as campaign_start_date, (select * from onl_end_date) as campaign_end_date, 
		min(time_utc) as conv_time, 1 as conv_flag, sum(spend) as spend
		FROM `wx-bq-poc.digital_attribution_modelling.dacamp_prod_online_sales`
		group by 1
	)
group by 1,2,3,4,5
