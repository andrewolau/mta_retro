drop table if exists `wx-bq-poc.digital_attribution_modelling.dacamp_prod_mc_final_crn`;
create table `wx-bq-poc.digital_attribution_modelling.dacamp_prod_mc_final_crn` as
(
	with onl_start_date as 
	(	
		select max(campaign_start_date) from `wx-bq-poc.digital_attribution_modelling.dacamp_prod_event_mw` where campaign_code = 'ONLINE'
	),

	onl_end_date as 
	(
		select max(campaign_end_date) from `wx-bq-poc.digital_attribution_modelling.dacamp_prod_event_mw` where campaign_code = 'ONLINE'
	)

	SELECT c.crn
		, e.segment_cvm
		, e.segment_lifestage
		, c.banner
		, c.campaign_code
		, c.campaign_type
		, c.campaign_start_date
		, c.campaign_end_date
		, c.tot_spend as total_sales
		, c.spend as inc_sales
		, REPLACE(REPLACE(case when a.channel is not null then a.channel else d.channel end, 'dx', 'wow_web'),'_AlwaysOn_','AlwaysOn_') as channel
		, REPLACE(REPLACE(case when a.medium is not null then a.medium else d.medium end, 'dx', 'wow_web'),'_AlwaysOn_','AlwaysOn_') as medium
		, REPLACE(REPLACE(case when a.event is not null then a.event else d.event end, 'dx', 'wow_web'),'_AlwaysOn_','AlwaysOn_') as event
		, case when a.event_volume is not null then a.event_volume else d.event_volume end as event_volume
		, case when a.event_reach is not null then a.event_reach else d.event_reach end as event_reach
		, case when a.event_converted_crn is not null then a.event_converted_crn else d.event_converted_crn end as event_converted_crn
		, case when a.medium_reach is not null then a.medium_reach else d.medium_reach end as medium_reach
		, case when a.medium_converted_crn is not null then a.medium_converted_crn else d.medium_converted_crn end as medium_converted_crn
		, case when channel_prob_norm is not null then channel_prob_norm / (sum(channel_prob_norm) OVER (PARTITION BY c.crn, b.key)) else 1 end as attributed_conversion
		, case when channel_prob_norm is not null then c.tot_spend * channel_prob_norm / (sum(channel_prob_norm) OVER (PARTITION BY c.crn, b.key)) else c.tot_spend end as attributed_total_sales
		, case when channel_prob_norm is not null then c.spend * channel_prob_norm / (sum(channel_prob_norm) OVER (PARTITION BY c.crn, b.key)) else c.spend end as attributed_inc_sales
	FROM
	(
		SELECT 
			concat('Total', '_', banner, '_', campaign_code, '_', cast(campaign_start_date as string), '_', cast(campaign_end_date as string)) as key
			, banner
			, crn
			, campaign_code
			, campaign_start_date
			, campaign_end_date
			, campaign_type
			, case when sum(tot_spend) is null then 0 else sum(tot_spend) end as tot_spend
			, case when sum(spend) is null then 0 else sum(spend) end as spend
		FROM
		(
			SELECT crn
				, banner
				, campaign_code
				, campaign_start_date
				, campaign_end_date
				, campaign_type
				, min(time_utc) as conv_time
				, sum(tot_spend) as tot_spend
				, sum(spend) as spend
			FROM `wx-bq-poc.digital_attribution_modelling.dacamp_prod_activation`
			group by 1,2,3,4,5,6
			
			union all
			
			SELECT crn
				, 'supermarkets' as banner
				, 'ONLINE' as campaign_code
				, (select * from onl_start_date) as campaign_start_date
				, (select * from onl_end_date) as campaign_end_date
				, 'ONLINE' as campaign_type
				, min(time_utc) as conv_time
				, sum(tot_spend) as tot_spend
				, sum(spend) as tot_spend
			FROM `wx-bq-poc.digital_attribution_modelling.dacamp_prod_online_sales`
			group by 1
		)
		group by 1,2,3,4,5,6,7
	) c 
	
	left join
	(		
		SELECT 
			concat('Total', '_', banner, '_', campaign_code, '_', cast(campaign_start_date as string), '_', cast(campaign_end_date as string)) as key
			, crn 
			, channel_event 
			, count(*) as event_volume
		FROM `wx-bq-poc.digital_attribution_modelling.dacamp_prod_event_mw` 
		group by 1,2,3
		order by 1,2,3
	) b on b.key = c.key and b.crn = c.crn
	
	left join 
	(
		select * from `wx-bq-poc.digital_attribution_modelling.dacamp_prod_mc_final`
	) a on a.key = b.key and a.event = b.channel_event 
	
	inner join
	(
		select key
			, max(channel) as channel
			, max(medium) as medium
			, max(event) as event
			, max(event_volume) as event_volume
			, max(event_reach) as event_reach
			, max(event_converted_crn) as event_converted_crn
			, max(medium_reach) as medium_reach
			, max(medium_converted_crn) as medium_converted_crn
		from `wx-bq-poc.digital_attribution_modelling.dacamp_prod_mc_final`
		where (campaign_code = 'ONLINE' and event like '%dx_view%')
				or (campaign_code <> 'ONLINE' and event like '%email_open%' and SUBSTR(event,1,8) = campaign_code)
		group by 1
	) d on c.key = d.key
	
	left join
	(
		SELECT crn, max(macro_segment_curr) as segment_cvm, max(lifestage) as segment_lifestage
		FROM `wx-bq-poc.wx_lty_digital_attribution_dev.redx_loyalty_customer_value_model`
		where Date(pw_end_date_utc) = (select max(Date(pw_end_date_utc)) from `wx-bq-poc.wx_lty_digital_attribution_dev.redx_loyalty_customer_value_model`)
		group by 1
	) e on c.crn = e.crn
	
);
