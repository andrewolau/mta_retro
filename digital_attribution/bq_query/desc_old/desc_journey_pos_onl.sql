
SELECT * FROM
(
SELECT banner, pos, count(*) as volume, count(distinct crn) as unique_crn,  'Full journey' as journey_type
FROM
(
	SELECT a.crn, ref_dt, time_utc, a.banner, channel_event, row_number() over (partition by a.crn, ref_dt, a.banner order by time_utc) as pos
	FROM
	(
		SELECT distinct crn, ref_dt, time_utc, banner,
			case 
			when channel_event like 'dx_view%' then 'dx_view' 
			when channel_event like 'dx_clk%' then 'dx_clk'
			else channel_event end as channel_event
		FROM `wx-bq-poc.digital_attribution_modelling.da_event_mw`

	) a 
	
	INNER JOIN

	(
		SELECT crn, date_utc, 'supermarkets' as banner, min(time_utc) as online_spend_time, 1 as online_flag
		FROM `wx-bq-poc.digital_attribution_modelling.da_online_sales`
		group by 1,2,3
	) b on a.crn = b.crn and a.ref_dt = b.date_utc and a.banner = b.banner
	where time_utc < online_spend_time or online_spend_time is null
)
group by 1,2
order by 1,2
)

UNION ALL

SELECT * FROM
(
SELECT banner, pos, count(*) as volume, count(distinct crn) as unique_crn,  'Deduped journey' as journey_type
FROM
(
	SELECT a.crn, ref_dt, time_utc, a.banner, channel_event, row_number() over (partition by a.crn, ref_dt, a.banner order by time_utc) as pos
	FROM
	(
		SELECT crn, ref_dt, min(time_utc) as time_utc, banner,
			case 
			when channel_event like 'dx_view%' then 'dx_view' 
			when channel_event like 'dx_clk%' then 'dx_clk'
			else channel_event end as channel_event
		FROM `wx-bq-poc.digital_attribution_modelling.da_event_mw`
		group by 1,2,4,5
	) a 
	
	INNER JOIN

	(
		SELECT crn, date_utc, 'supermarkets' as banner, min(time_utc) as online_spend_time, 1 as online_flag
		FROM `wx-bq-poc.digital_attribution_modelling.da_online_sales`
		group by 1,2,3
	) b on a.crn = b.crn and a.ref_dt = b.date_utc and a.banner = b.banner
	where time_utc < online_spend_time or online_spend_time is null
)
group by 1,2
order by 1,2
);

