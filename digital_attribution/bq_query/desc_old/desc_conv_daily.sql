SELECT conv_type, banner, date_utc, volume, unique_crn,
	case when spend is null then 0 else spend end as spend,
	case when spend_per_conv is null then 0 else spend_per_conv end as spend_per_conv
FROM 
(
	SELECT 'activation' as conv_type, banner, date_utc,
	count(*) as volume, 
	count(distinct crn) as unique_crn,
	sum(spend) as spend,
	sum(spend)/count(*) as spend_per_conv
	FROM `wx-bq-poc.digital_attribution_modelling.da_activation` 
	group by 1,2,3

	union all

	SELECT  'online_purchase' as conv_type, 'supermarkets' as banner, date_utc,
	count(*) as volume, 
	count(distinct crn) as unique_crn,
	sum(spend) as spend,
	sum(spend)/count(*) as spend_per_conv
	FROM `wx-bq-poc.digital_attribution_modelling.da_online_sales` 
	group by 1,2,3
)
order by 1,2,3;
