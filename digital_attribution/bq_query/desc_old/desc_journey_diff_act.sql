
SELECT * FROM 
(
SELECT banner, diff, count(*) as volume, count(distinct crn) as unique_crn
FROM
(
	SELECT a.crn, ref_dt, timestamp_diff(activation_time, time_utc, day) as diff, a.banner, channel_event, row_number() over (partition by a.crn, ref_dt, a.banner order by time_utc) as pos
	FROM
	(
		SELECT distinct crn, ref_dt, time_utc, banner,
			case 
			when channel_event like 'dx_view%' then 'dx_view' 
			when channel_event like 'dx_clk%' then 'dx_clk'
			else channel_event end as channel_event
		FROM `wx-bq-poc.digital_attribution_modelling.da_event_mw`
	) a 

	INNER JOIN

	(
		SELECT crn, date_utc, banner, min(time_utc) as activation_time, 1 as activation_flag
		FROM `wx-bq-poc.digital_attribution_modelling.da_activation`
		group by 1,2,3
	) b on a.crn = b.crn and a.ref_dt = b.date_utc and a.banner = b.banner
	where time_utc < activation_time or activation_time is null
)
where pos = 1
group by 1,2
order by 1,2
)
