SELECT banner, date_utc, dx_event, count(*) as vol, count(distinct crn) as unique_crn, count(distinct session_id) as unique_session
FROM (
	SELECT distinct crn, time_utc, Date(time_utc) as date_utc, banner,
		case	
		when event_name in ('page view', 'search view') then CONCAT('dx_view - ', event_name)
		when event_name = 'click view' then CONCAT('dx_clk'," - ",lower(REPLACE(json_extract(attributes, "$.asset_type[0]"), "\"", ""))) 
		else event_name end as dx_event,
		json_extract(attributes, "$.udo_tealium_session_id[0]") as session_id
	FROM `wx-bq-poc.wx_lty_digital_attribution_dev.eventstore_events` 
	where channel = 'dx' and event_name <> 'purchase'
)
where date_utc BETWEEN DATE_SUB(DATE_SUB(Date(DATE_TO_), INTERVAL 27 DAY), INTERVAL 14 DAY) AND Date(DATE_TO_)
group by 1,2,3
order by 1,2,3
;