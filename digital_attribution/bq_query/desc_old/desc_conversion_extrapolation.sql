SELECT * FROM
(
SELECT a.conv_type, a.banner, b.conv/a.paths_conv as extrapolation_rate
FROM  
(
SELECT *
FROM
(
	with base as
	(
		SELECT concat(a.crn, cast(ref_dt as string)) as key, a.crn, ref_dt, time_utc, a.banner, channel_event, activation_time, case when activation_flag is null then 0 else 1 end as activation_flag
		FROM
		(
			SELECT crn, ref_dt, banner, min(time_utc) as time_utc, channel_event
			FROM
			(
				SELECT crn, ref_dt, time_utc, banner,
					case 
					when channel_event like 'dx_view%' then 'dx_view' 
					when channel_event like 'dx_clk%' then 'dx_clk'
					else channel_event end as channel_event
				FROM `wx-bq-poc.digital_attribution_modelling.da_event_mw`
			)
			group by 1,2,3,5
		) a 

		LEFT JOIN
		(
			SELECT crn, date_utc, banner, min(time_utc) as activation_time, 1 as activation_flag
			FROM `wx-bq-poc.digital_attribution_modelling.da_activation`
			group by 1,2,3
		) b on a.crn = b.crn and a.ref_dt = b.date_utc and a.banner = b.banner
		where time_utc < activation_time or activation_time is null
	)

	SELECT 'activation' as conv_type, a.*, b.paths, case when c.paths_conv is null then 0 else c.paths_conv end as paths_conv FROM
	(
	  (SELECT banner, count(*) as volume, count(distinct crn) as unique_crn  
		FROM 
		(
			SELECT crn, banner, date_utc, 
			case 
				when channel_event like 'dx_view%' then 'dx_view' 
				when channel_event like 'dx_clk%' then 'dx_clk'
				else channel_event end as channel_event
			FROM `wx-bq-poc.digital_attribution_modelling.da_event` 
		)
		group by 1) a 
		inner join
		(SELECT banner, count(distinct key) as paths from base group by 1) b on a.banner = b.banner
		left join
		(SELECT banner, count(distinct key) as paths_conv from base where activation_flag = 1 group by 1) c on a.banner = c.banner
	)
	order by 1,2
) 

UNION ALL 

SELECT * 
FROM
(
	with base as
	(
		SELECT concat(a.crn, cast(ref_dt as string)) as key, a.crn, ref_dt, time_utc, a.banner, channel_event, case when online_flag is null then 0 else 1 end as online_flag
		FROM
		(
			SELECT crn, ref_dt, banner, min(time_utc) as time_utc, channel_event
			FROM
			(
				SELECT crn, ref_dt, time_utc, banner,
					case 
					when channel_event like 'dx_view%' then 'dx_view' 
					when channel_event like 'dx_clk%' then 'dx_clk'
					else channel_event end as channel_event
				FROM `wx-bq-poc.digital_attribution_modelling.da_event_mw`
			)
			group by 1,2,3,5
		) a 

		LEFT JOIN
		(
			SELECT crn, date_utc, 'supermarkets' as banner, min(time_utc) as online_time, 1 as online_flag
			FROM `wx-bq-poc.digital_attribution_modelling.da_online_sales`
			group by 1,2,3
		) b on a.crn = b.crn and a.ref_dt = b.date_utc and a.banner = b.banner
		where time_utc < online_time or online_time is null
	)

	SELECT 'online' as conv_type, a.*, b.paths, case when c.paths_conv is null then 0 else c.paths_conv end as paths_conv FROM
	(
	  (SELECT banner, count(*) as volume, count(distinct crn) as unique_crn  
		FROM 
		(
			SELECT crn, banner, date_utc, 
			case 
				when channel_event like 'dx_view%' then 'dx_view' 
				when channel_event like 'dx_clk%' then 'dx_clk'
				else channel_event end as channel_event
			FROM `wx-bq-poc.digital_attribution_modelling.da_event` 
		)
		group by 1) a 
	  inner join
		(SELECT banner, count(distinct key) as paths from base group by 1) b on a.banner = b.banner 
		left join
		(SELECT banner, count(distinct key) as paths_conv from base where online_flag = 1 group by 1) c on a.banner = c.banner 
	)
	order by 1,2
)


) a 

LEFT JOIN

(
SELECT 'activation' as conv_type, a.banner, sum(activation) as conv
FROM
(
	(
		SELECT crn, banner, date_utc, min(time_utc) as activation_time, 1 as activation, sum(spend) as activation_spend
		FROM `wx-bq-poc.digital_attribution_modelling.da_activation`
		group by 1,2,3
	) a
	inner join
	(
		SELECT distinct crn, banner 
		FROM `wx-bq-poc.digital_attribution_modelling.da_event_mw`
	) b on a.crn = b.crn and a.banner = b.banner
)
group by 1,2

UNION ALL

SELECT 'online' as conv_type, a.banner, sum(online) as conv
FROM
(
	(
		SELECT crn, 'supermarkets' as banner, date_utc, min(time_utc) as online_time, 1 as online, sum(spend) as online_spend
		FROM `wx-bq-poc.digital_attribution_modelling.da_online_sales`
		group by 1,2,3
	) a
	inner join
	(
		SELECT distinct crn, banner 
		FROM `wx-bq-poc.digital_attribution_modelling.da_event_mw`
	) b on a.crn = b.crn and a.banner = b.banner
)
group by 1,2
) b on a.conv_type = b.conv_type and a.banner=b.banner
)
where extrapolation_rate is not null order by 1,2