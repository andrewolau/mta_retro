select * from
(
	select 'activation' as conv_type, * from `digital_attribution_modelling.da_markov_trans_matrix_activation`
	union all
	select 'online' as conv_type, * from `digital_attribution_modelling.da_markov_trans_matrix_online`
)