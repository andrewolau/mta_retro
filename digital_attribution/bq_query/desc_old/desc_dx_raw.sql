SELECT a.*, b.dx_view_cnt, c.dx_clk_cnt, d.online_purchase_volume, d.unique_crn_purchased, d.online_spend, 
d.unique_crn_purchased/a.unique_crn as conv_rate_crn,
d.avg_spend_per_transaction 
FROM 
(
	(
		SELECT banner, count(distinct crn) as unique_crn, count(distinct session_id) as unique_session
		FROM (
			SELECT crn, Date(time_utc) as date_utc, banner, json_extract(attributes, "$.udo_tealium_session_id[0]") as session_id
			FROM `wx-bq-poc.wx_lty_digital_attribution_dev.eventstore_events` 
			where channel = 'dx'
		)
		where date_utc BETWEEN DATE_SUB(DATE_SUB(Date(DATE_TO_), INTERVAL 27 DAY), INTERVAL 14 DAY) AND Date(DATE_TO_)
		group by 1
	) a 

	INNER JOIN

	(
		SELECT banner, count(*) as dx_view_cnt
		FROM 
		(
			SELECT crn, time_utc, Date(time_utc) as date_utc, banner
			FROM `wx-bq-poc.wx_lty_digital_attribution_dev.eventstore_events` 
			where channel = 'dx' and event_name in ('page view', 'search view')
		)
		where date_utc BETWEEN DATE_SUB(DATE_SUB(Date(DATE_TO_), INTERVAL 27 DAY), INTERVAL 14 DAY) AND Date(DATE_TO_)
		group by 1
	) b on a.banner = b.banner
	
	INNER JOIN

	(
		SELECT banner, count(*) as dx_clk_cnt
		FROM 
		(
			SELECT crn, time_utc, Date(time_utc) as date_utc, banner
			FROM `wx-bq-poc.wx_lty_digital_attribution_dev.eventstore_events` 
			where channel = 'dx' and event_name in ('click view')
		)
		where date_utc BETWEEN DATE_SUB(DATE_SUB(Date(DATE_TO_), INTERVAL 27 DAY), INTERVAL 14 DAY) AND Date(DATE_TO_)
		group by 1
	) c on a.banner = c.banner

	INNER JOIN

	(
		SELECT 'supermarkets' as banner, count(*) as online_purchase_volume, count(distinct crn) as unique_crn_purchased, sum(spend) as online_spend, sum(spend)/count(*) as avg_spend_per_transaction
		FROM `wx-bq-poc.digital_attribution_modelling.da_online_sales` 
	) d on a.banner = d.banner
);


