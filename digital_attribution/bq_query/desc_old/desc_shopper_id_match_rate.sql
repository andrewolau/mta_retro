SELECT udo_tealium_datasource, count(distinct t3.crn)/count(distinct t2.shopperid) as shopper_id_match_rate
FROM `wx-bq-poc.wx_lty_digital_attribution_dev.tealium_events` t1
INNER JOIN `wx-bq-poc.wx_lty_digital_attribution_dev.redx_loyalty_mor_smkt_str_shopperonecard` t2
ON t1.udo_user_profile_id = cast(t2.shopperid as string)
INNER JOIN `wx-bq-poc.wx_lty_digital_attribution_dev.redx_loyalty_lylty_card_detail` t3
ON t2.onecardnumber = t3.lylty_card_nbr
WHERE udo_user_profile_id is not null
 group by 1 order by 2 desc