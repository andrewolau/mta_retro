SELECT * FROM
(
	SELECT banner, date_utc, channel_event, count(*) as volume, count(distinct crn) as unique_crn 
	FROM 
	(
		SELECT crn, banner, date_utc, 
		case 
			when channel_event like 'dx_view%' then 'dx_view' 
			when channel_event like 'dx_clk%' then 'dx_clk'
			else channel_event end as channel_event
		FROM `wx-bq-poc.digital_attribution_modelling.da_event` 
	)
	group by 1,2,3

	union all 

	SELECT banner, date_utc, 'activation' as channel_event, count(*) as volume, count(distinct crn) as unique_crn
	FROM `wx-bq-poc.digital_attribution_modelling.da_activation` 
	group by 1,2,3

	union all

	SELECT 'supermarkets' as banner, date_utc, 'online_purchase' as channel_event, count(*) as volume, count(distinct crn) as unique_crn
	FROM `wx-bq-poc.digital_attribution_modelling.da_online_sales` 
	group by 1,2,3
)
order by 1,2,3
;



