drop table if exists `wx-bq-poc.digital_attribution_modelling.dacamp_prod_eventstore_view_snapshot_test_v_new`;
CREATE table `wx-bq-poc.digital_attribution_modelling.dacamp_prod_eventstore_view_snapshot_test_v_new` as 
(
	
	SELECT *		
	FROM
	(
		-- email events
		SELECT distinct 
			crn
			, time_utc
			, date
			, case when json_extract(attributes, "$.campaign_code[0]") like '%ENG%' then 'supermarkets' else banner end as banner
			, 'email' as channel
			, case when event_name = 'email open' then 'open' else 'clk' end as event
			, REPLACE(json_extract(attributes, "$.campaign_code[0]"), "\"", "") as campaign_code
			, CAST(REPLACE(json_extract(attributes, "$.campaign_or_control_start_date[0]"), "\"", "") AS DATE) as campaign_start_date
			, CAST(REPLACE(json_extract(attributes, "$.campaign_or_control_end_date[0]"), "\"", "") AS DATE) as campaign_end_date
			, cast(null as string) as attributes
		FROM `wx-bq-poc.wx_lty_digital_attribution_dev.eventstore_events`
		where channel like '%email%' and event_name in ('email open', 'email click')
		
		union all
		
		-- oap events 
		SELECT distinct 
			crn
			, time_utc
			, date
			, banner
			, 'rw_web_oap' as channel
			, 'clk' as event_name
			,REPLACE(json_extract(attributes, "$.udo_campaign_code[0]"), "\"", "") as campaign_code
			,CAST(REPLACE(json_extract(attributes, "$.udo_campaign_start[0]"), "\"", "") as date) as campaign_start_date
			,CAST(REPLACE(json_extract(attributes, "$.udo_campaign_end[0]"), "\"", "") as date) as campaign_end_date
			,cast(null as string) as attributes
		FROM `wx-bq-poc.wx_lty_digital_attribution_dev.eventstore_events`
		where channel like '%oap%' and event_name like '%click%'
		
		union all

		-- webside events from Tealium --
		SELECT distinct 
			crn
			, time_utc
			, date
			, banner
			, 'wow_web' as channel
			, case when event_name in ('page view','search view') then 'view'
					else 'clk' end as event
			, cast(null as string) as campaign_code 
			, cast(null as date) as campaign_start_date
			, cast(null as date) as campaign_end_date
			, CONCAT('tealium_event=',tealium_event,';url=',url) as attributes
		FROM
		(
			SELECT *, rank() over (partition by crn, session_id, event_name, url order by time_utc asc) as event_rank
			FROM 
			(
				SELECT *
					, json_extract(attributes, "$.udo_tealium_session_id[0]") as session_id
					, REPLACE(json_extract(attributes, "$.dom_url[0]"), "\"", "") as url
					, REPLACE(json_extract(attributes, "$.udo_tealium_event[0]"), "\"", "") as tealium_event,
				FROM `wx-bq-poc.wx_lty_digital_attribution_dev.eventstore_events` 
				where channel = 'dx' and event_name in ('page view', 'search view', 'click view')
			)
		)
		where event_rank = 1 
		
		union all
		
		-- google events - search --
		SELECT distinct
			crn
			, time_utc
			, date
			, case 
				when regexp_contains(paid_search_campaign, r'CAT-|CNA-|CVM-|ENG-|LCP-|NCP-|OSP-|FSC-|ONL-') then 'supermarkets'
				when regexp_contains(paid_search_campaign, r'BCV-|BLC-|BCT-|BSP-|BOL-|BEV-|NBA-') then 'bws'
				when regexp_contains(paid_search_campaign, r'WCV-|WLC-|WCT-|WSP-|WOL-|WEV-|WEN-') then 'bigw'
				when regexp_contains(paid_search_campaign, r'CCV-|CEN-|CCD-') then 'caltex'
				when regexp_contains(paid_search_campaign, r'FCO-') then 'fuelco'
				else 'supermarkets' end as banner
			, 'sem' as channel
			, case 
				when lower(paid_search_campaign) like '%brand%' then 'brand_clk'
				when regexp_contains(paid_search_campaign, r'(?i)generic|content|competitor') then 'generic_clk'
				when regexp_contains(paid_search_campaign, r'(?i)gmail') then 'gmail_clk'
				when paid_search_campaign is null then 'shopping_clk'
				else 'other_clk' end as event_name
			, CASE WHEN ARRAY_LENGTH(REGEXP_EXTRACT_ALL(paid_search_campaign, r"[A-Z]+-\d\d\d\d")) >0 
					THEN REGEXP_EXTRACT_ALL(paid_search_campaign, r"[A-Z]+-\d\d\d\d")[OFFSET(0)] 
					else null end as campaign_code
			, cast(null as date) as campaign_start_date
			, cast(null as date) as campaign_end_date
			, CONCAT('paid_search_campaign=',paid_search_campaign,';paid_search_keyword=',paid_search_keyword) as attributes
		FROM
		(
			SELECT *
					, REPLACE(json_extract(attributes, "$.paid_search_campaign[0]"),'\"','') as paid_search_campaign
					, REPLACE(json_extract(attributes, "$.paid_search_keyword[0]"),'\"','') as paid_search_keyword
			FROM `wx-bq-poc.wx_lty_digital_attribution_dev.eventstore_events` 
			where channel = 'sem'
		)
		
		
		union all
		
		-- google events - non-search --
		SELECT distinct
			crn
			, time_utc
			, date
            , banner
-- 			, case 
-- 				when regexp_contains(placement, r'CAT-|CNA-|CVM-|ENG-|LCP-|NCP-|OSP-|FSC-|ONL-') then 'supermarkets'
-- 				when regexp_contains(placement, r'BCV-|BLC-|BCT-|BSP-|BOL-|BEV-|NBA-|BWS-') then 'bws'
-- 				when regexp_contains(placement, r'WCV-|WLC-|WCT-|WSP-|WOL-|WEV-|WEN-|BIGW-') then 'bigw'
-- 				when regexp_contains(placement, r'CCV-|CEN-|CCD-') then 'caltex'
-- 				when regexp_contains(placement, r'FCO-') then 'fuelco'
-- 				else 'supermarkets' end as banner
			, case
				when regexp_contains(placement, r'(?i)youtube|yt') then 'youtube'
				when regexp_contains(placement, r'(?i)gmail') then 'gmail'
				when regexp_contains(placement, r'(?i)audio') then 'audio'
				when regexp_contains(placement, r'(?i)video|vdo') then 'video'
				when regexp_contains(placement, r'(?i)scroll|native|weibo|display') or regexp_contains(placement, r'\d{2,}[xX]\d{2,}') then 'display'
				else 'google_other' end as channel 
			, case when event_name like '%impression%' then 'imp' else 'clk' end as event_name
			, CASE WHEN ARRAY_LENGTH(REGEXP_EXTRACT_ALL(placement, r"[A-Z]+-\d\d\d\d")) >0 
					THEN REGEXP_EXTRACT_ALL(placement, r"[A-Z]+-\d\d\d\d")[OFFSET(0)] 
					else null end as campaign_code
			, cast(null as date) as campaign_start_date
			, cast(null as date) as campaign_end_date
			, CONCAT('placement=',placement,';placment_id=',a.placement_id) as attributes
		FROM
		(
			SELECT *
					, REPLACE(json_extract(attributes, "$.placement_id[0]"), '\"','') as placement_id
					, REPLACE(json_extract(attributes, "$.campaign_id[0]"), '\"','') as campaign_id
			FROM `wx-bq-poc.wx_lty_digital_attribution_dev.eventstore_events` 
			where event_name like '%google%' and channel <> 'sem'
		) a
		LEFT JOIN 
		(
			SELECT placement_id, campaign_id, max(placement) as placement
			FROM `wx-bq-poc.wx_lty_digital_attribution_dev.googlex_placements`
			group by 1,2
		) b on a.placement_id = b.placement_id and a.campaign_id = b.campaign_id
		
		
		union all
		
		-- rewards app offer events from Tealium --
		SELECT distinct 
			crn
			, time_utc
			, date
			, banner
			, 'rw_app' as channel
			, case when event_name = 'in-app impression' then 'imp' else 'clk' end as event_name
			, campaign_code 
			, cast(null as date) as campaign_start_date
			, cast(null as date) as campaign_end_date
			, CONCAT('tealium_event=',tealium_event,';event_label=',event_label) as attributes
		FROM
		(
			SELECT crn, time_utc, date,
				banner, 
				channel,
				event_name,
				case when tealium_event like '%product%' and banner = 'supermarkets' then 'CVM-1661' 
					 when tealium_event like '%product%' and banner = 'BWS' then 'BCV-1076' 
					else campaign_code end as campaign_code, 
				rank() over (partition by crn, session_id, event_name, tealium_event, campaign_code order by time_utc asc) as event_rank,
				tealium_event,
				event_label
			FROM 
			(
				SELECT *, 
					REPLACE(json_extract(attributes, "$.udo_offer_code[0]"), "\"", "") as campaign_code,
					json_extract(attributes, "$.udo_tealium_session_id[0]") as session_id,
					REPLACE(json_extract(attributes, "$.udo_tealium_event[0]"), "\"", "") as tealium_event,
					REPLACE(json_extract(attributes, "$.udo_event_label[0]"), "\"", "") as event_label
				FROM `wx-bq-poc.wx_lty_digital_attribution_dev.eventstore_events` 
				where channel = 'rewards app' and event_name in ('in-app impression', 'in-app click')
			)
		)
		where event_rank = 1 and ARRAY_LENGTH(REGEXP_EXTRACT_ALL(campaign_code, r"[A-Z]+-\d\d\d\d")) >0
		
		
		union all
		
		-- reward app push notification from swrve --	
		select distinct
			crn
			, client_time as time_utc
			, date(client_time,"Australia/Sydney") as date
            , banner
-- 			, case 
-- 				when regexp_contains(campaign_code, r'CAT-|CNA-|CVM-|ENG-|LCP-|NCP-|OSP-|FSC-|ONL-') then 'supermarkets'
-- 				when regexp_contains(campaign_code, r'BCV-|BLC-|BCT-|BSP-|BOL-|BEV-|NBA-|BWS-') then 'bws'
-- 				when regexp_contains(campaign_code, r'WCV-|WLC-|WCT-|WSP-|WOL-|WEV-|WEN-|BIGW-') then 'bigw'
-- 				when regexp_contains(campaign_code, r'CCV-|CEN-|CCD-') then 'caltex'
-- 				when regexp_contains(campaign_code, r'FCO-') then 'fuelco'
-- 				else 'supermarkets' end as banner
			, 'rw_app' as channel
			, case when parameters_action_type = 'influenced' then 'push_imp' else 'push_clk' end as event
			,campaign_code
			,cast(SUBSTR(campaign_start_date,1,10) as date) as campaign_start_date
			,cast(null as date) as campaign_end_date
			,'' as attributes
		from 
		(
			SELECT distinct user, client_time, parameters_action_type, type, parameters_name
					, case when ARRAY_LENGTH(split(parameters_name,'-')) >=2 and ARRAY_LENGTH(split(parameters_name,'.')) >=4 then split(split(parameters_name,'-')[offset(1)],'.')[offset(0)] 
							else cast(parameters_id as string) end as parameters_id
			FROM `wx-bq-poc.wx_lty_digital_attribution_dev.swrve_events`
			where type in ('event', 'generic_campaign_event')
					and (parameters_action_type = 'influenced' or parameters_name like '%engaged%')
					and game = 31752
					and date(client_time,"Australia/Sydney") >= '2020-01-01'
		) swrve
		inner join 
		(
			select user, max(attributes_swrve_external_user_id) as attributes_swrve_external_user_id
			from `wx-bq-poc.wx_lty_digital_attribution_dev.swrve_events`
			where type = 'user'
					and game = 31752 --Production
					and attributes_swrve_external_user_id is not null
			group by 1
		) hash_crn on swrve.user = hash_crn.user
		inner join 
		(
				select crn_enc, max(crn) as crn 
				from `wx-bq-poc.loyalty.customer_encrypted_detail` 
				where crn is not NULL and crn <> '0'
        group by 1
			
		) m on hash_crn.attributes_swrve_external_user_id = m.crn_enc
		inner join 
		(
			select distinct
					tracking_id
					,case when ARRAY_LENGTH(REGEXP_EXTRACT_ALL(campaign_name, r"[A-Z]+-\d\d\d\d")) > 0 then REGEXP_EXTRACT_ALL(campaign_name, r"[A-Z]+-\d\d\d\d")[offset(0)]
							else cast(null as string) end as campaign_code
					,case when ARRAY_LENGTH(split(campaign_description,'|')) >=2 then split(campaign_description,'|')[offset(0)] end as campaign_start_date
					,row_number() over (partition by tracking_id order by campaign_name) as _rank
					from `wx-bq-poc.wx_lty_digital_attribution_dev.swrve_custom_campaign_events` 
		) dc on swrve.parameters_id = dc.tracking_id 
					and _rank = 1
					and ARRAY_LENGTH(REGEXP_EXTRACT_ALL(campaign_code, r"[A-Z]+-\d\d\d\d")) >0
	)
	where date >= Date('2020-01-01')
);

