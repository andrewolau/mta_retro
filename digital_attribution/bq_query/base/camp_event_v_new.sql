--------------------------------------------DA EVENTS----------------------------------------------------
DROP TABLE IF EXISTS `wx-bq-poc.digital_attribution_modelling.dacamp_prod_event_v_new_20210405`;
DROP TABLE IF EXISTS `wx-bq-poc.digital_attribution_modelling.dacamp_prod_online_sales_v_new_20210405`;
DROP TABLE IF EXISTS `wx-bq-poc.digital_attribution_modelling.dacamp_prod_activation_v_new_20210405`;

--- activation sales ---
create table `wx-bq-poc.digital_attribution_modelling.dacamp_prod_activation_v_new_20210405` as
(
	SELECT a.*, b.spend
	FROM
	(
		select crn
			, case when time_utc is null then TIMESTAMP(campaign_end_date) else time_utc end as time_utc
			, case when date is null then campaign_end_date else date end as date
			, banner
			, campaign_code
			, campaign_start_date
			, campaign_end_date
			, campaign_end_date_real
			, campaign_type
			, tot_spend
		FROM
		(
			SELECT crn
				, time_utc
				, date
				, banner
				, campaign_code
				, campaign_start_date
				, case when DATE_DIFF(campaign_end_date, DATE_ADD(DATE_TRUNC(CAST('2021-04-05' as date), week(Monday)), INTERVAL 6 DAY), DAY)>0 
					then DATE_ADD(DATE_TRUNC(CAST('2021-04-05' as date), week(Monday)), INTERVAL 6 DAY) else campaign_end_date end as campaign_end_date
				, campaign_end_date as campaign_end_date_real
				, case when campaign_ttl is false then 'BTL' else 'TTL' end as campaign_type
				, case when campaign_tpg_amt is false then cast(sales_amt as numeric) else cast(sales_amt_tpg as numeric) end as tot_spend
			FROM `wx-bq-poc.digital_attribution_modelling.safarievents_v_new_20210405`
		)
		where banner = 'supermarkets'
		--		and date <= DATE_ADD(DATE_TRUNC('2021-04-05', week(Monday)), INTERVAL 6 DAY)
	) a
	inner join
	(
	    SELECT crn, campaign_code, cast(campaign_start_date as date) as campaign_start_date, sum(inc_sales) as spend
		FROM `Attribution_Safari_GT_output.Safari_crn_level_game_theory_output_safari_full_promo_halo_post_current`
		where fw_start_date = CAST('2021-04-05' as date) and pph = 'promo'
		group by 1,2,3
		
		union all
		
		SELECT cast(crn as string), campaign_code, CAST(campaign_start_date as date) as campaign_start_date, sum(inc_sales) as spend
		FROM `Attribution_Safari_Matching.ws_2b_current_v_new_20210405`
		where CAST(fw_start_date as date) = CAST('2021-04-05' as date) and open_flag = 1
		group by 1,2,3
		
		
		union all
		
		SELECT cast(crn as string), campaign_code, CAST(campaign_start_date as date) as campaign_start_date, sum(inc_sales) as spend
		FROM `Attribution_Safari_Matching.ttl_crn_level_store`
		where CAST(fw_start_date as date) = CAST('2021-04-05' as date) and open_flag = 1 and campaign_code not like 'W%'
		group by 1,2,3	 
		
	) b on a.crn = b.crn and a.campaign_code = b.campaign_code and a.campaign_start_date = b.campaign_start_date
	where banner in ('supermarkets')
	
	union all
	
	SELECT a.*, b.spend
	FROM
	(
		select crn
			, case when time_utc is null then TIMESTAMP(campaign_end_date) else time_utc end as time_utc
			, case when date is null then campaign_end_date else date end as date
			, banner
			, campaign_code
			, campaign_start_date
			, campaign_end_date
			, campaign_end_date_real
			, campaign_type
			, tot_spend
		FROM
		(
			SELECT crn
				, time_utc
				, date
				, banner
				, campaign_code
				, campaign_start_date
				, case when DATE_DIFF(campaign_end_date, DATE_ADD(DATE_TRUNC(CAST('2021-04-05' as date), week(Monday)), INTERVAL 6 DAY), DAY)>0 
					then DATE_ADD(DATE_TRUNC(CAST('2021-04-05' as date), week(Monday)), INTERVAL 6 DAY) else campaign_end_date end as campaign_end_date
				, campaign_end_date as campaign_end_date_real
				, case when campaign_ttl is false then 'BTL' else 'TTL' end as campaign_type
				, case when campaign_tpg_amt is false then cast(sales_amt as numeric) else cast(sales_amt_tpg as numeric) end as tot_spend
			FROM `wx-bq-poc.digital_attribution_modelling.safarievents_v_new_20210405`
			
		)
		where banner = 'bigw'
		--		and date <= DATE_ADD(DATE_TRUNC('2021-04-05', week(Monday)), INTERVAL 6 DAY)
	) a
	inner join
	(
		
		SELECT crn, campaign_code, cast(campaign_start_date as date) as campaign_start_date, sum(inc_sales) as spend
		FROM `Attribution_Safari_GT_output.Safari_crn_level_game_theory_output_safari_full_promo_halo_post_current_big_w`
		where fw_start_date = CAST('2021-04-05' as date) and pph = 'promo'
		group by 1,2,3
		
		union all
		
		SELECT cast(crn as string), campaign_code, CAST(campaign_start_date as date) as campaign_start_date, sum(inc_sales) as spend
		FROM `Attribution_Safari_Matching.ttl_crn_level_store`
		where CAST(fw_start_date as date) = CAST('2021-04-05' as date) and open_flag = 1
		group by 1,2,3	 

	) b on a.crn = b.crn and a.campaign_code = b.campaign_code and a.campaign_start_date = b.campaign_start_date
	where banner in ('bigw')
);



--- online sales ---
create table `wx-bq-poc.digital_attribution_modelling.dacamp_prod_online_sales_v_new_20210405` as
(
	with max_date as 
	(
		select max(campaign_end_date) as date from `wx-bq-poc.digital_attribution_modelling.dacamp_prod_activation_v_new_20210405`
	)
	
	select crn, start_txn_time as time_utc, date
		, sum(tot_amt_incld_gst-ass.tot_wow_dollar_incld_gst) as tot_spend
		, sum(tot_amt_incld_gst-ass.tot_wow_dollar_incld_gst) as spend
	from
	(
		select lylty_card_nbr, start_txn_time, Date(start_txn_time, "Australia/Sydney") as date, division_nbr, prod_nbr, tot_amt_incld_gst, tot_wow_dollar_incld_gst 
		from loyalty.article_sales_summary 
		where division_nbr in (1005,1030) and checkout_nbr=100 and void_flag <> 'Y'
	) ass
   inner join 
   (
		select prod_nbr, division_nbr 
		from 
		(
			select *, rank() over (partition by prod_nbr, division_nbr order by last_update_date desc) as date_rank 
			from `wx-bq-poc.wx_lty_digital_attribution_dev.redx_loyalty_article_master`
		)
		where date_rank=1
	) am on ass.prod_nbr=am.prod_nbr and ass.division_nbr=am.division_nbr
   inner join 
   (
		select lylty_card_nbr,crn 
		from 
		(
			select *,rank() over (partition by crn order by last_update_date desc) as date_rank 
			from `wx-bq-poc.wx_lty_digital_attribution_dev.redx_loyalty_lylty_card_detail`
		)
		where date_rank=1 and lylty_card_status=1 and crn is not NULL and crn <> '0'
	) as lcd on ass.lylty_card_nbr=lcd.lylty_card_nbr
	where date BETWEEN DATE_TRUNC((select date from max_date), week(Monday)) AND DATE_ADD(DATE_TRUNC((select date from max_date), week(Monday)), INTERVAL 6 DAY)
	group by 1,2,3
);


--- timestamped campaign level event ---

CREATE table `wx-bq-poc.digital_attribution_modelling.dacamp_prod_event_v_new_20210405` as 
(

	with max_date as 
	(
		select max(campaign_end_date) as date from `wx-bq-poc.digital_attribution_modelling.dacamp_prod_activation_v_new_20210405`
	),
	
	min_date as 
	(
		select case when DATE_DIFF(min_date, min_camp_date, day) < 0 then min_date else min_camp_date end as date
		from
		(
			select DATE_TRUNC((select date from max_date), week(Monday)) as min_date, min(campaign_start_date) as min_camp_date 
			from `wx-bq-poc.digital_attribution_modelling.dacamp_prod_activation_v_new_20210405`
		)
	)
	
	SELECT * FROM
	(
		SELECT distinct crn, time_utc, Date(time_utc, "Australia/Sydney") as date
			, banner
			, channel
			, CONCAT(channel, '_', event) as channel_event
			, CASE WHEN campaign_code is null then '_AlwaysOn' else campaign_code end as campaign_code
			, campaign_start_date
			, campaign_end_date
		FROM  `wx-bq-poc.digital_attribution_modelling.dacamp_prod_eventstore_view_snapshot_test_v_new`
	)
	where banner in ('supermarkets','bws','bigw') 
			and channel_event <> 'other' 
			and date BETWEEN DATE_SUB((select date from min_date), INTERVAL 7 day) AND DATE_ADD(DATE_TRUNC((select date from max_date), week(Monday)), INTERVAL 6 DAY)
);

