SELECT distinct channel_event as Event, 
REGEXP_REPLACE(channel_event, r"_open|_imp|_clk|_view","") as Medium,
REGEXP_REPLACE(channel_event, r"_open|_imp|_clk|_view|_LIA|_shopping|_generic|_brand|_img|_vdo","") as Channel
FROM `wx-bq-poc.digital_attribution_modelling.datiger_prod_event_mw`
order by 1