
--------------------------------------------DA EVENTS----------------------------------------------------
Drop table if exists `wx-bq-poc.digital_attribution_modelling.datiger_prod_event_mw`;
DROP TABLE IF EXISTS `wx-bq-poc.digital_attribution_modelling.datiger_prod_event`;
DROP TABLE IF EXISTS `wx-bq-poc.digital_attribution_modelling.datiger_prod_redeemer`;

CREATE table `wx-bq-poc.digital_attribution_modelling.datiger_prod_redeemer` as 
select distinct 'supermarkets' as banner
				, crn
				, concat('OSP-2373','_',offer_nbr) as campaign_code
				, CAST(campaign_start_date as date) as campaign_start_date
				, DATE_ADD(CAST(campaign_start_date as date), interval 6 day) as campaign_end_date
				, case when sum(cast(redeem_cnt as numeric)) > 0 then 1 else 0 end as conv_flag
				, case when sum(cast(redeem_cnt as numeric)) > 0 then min(TIMESTAMP(DATE_ADD(CAST(campaign_start_date as date), interval 7 day))) else cast(null as timestamp) end as conv_time
				, case when sum(cast(redeem_cnt as numeric)) > 0 then sum(CAST(tpg_inc_avg as numeric)) else 0 end as spend
			from `wx-bq-poc.digital_attribution_modelling.sy_TIGER_TRAP_INCSALES`
			group by 1,2,3,4,5;
			

CREATE table `wx-bq-poc.digital_attribution_modelling.datiger_prod_event` as 
(

	with min_date as 
	(
		select max(cast(campaign_start_date as date)) as date 
		from `wx-bq-poc.digital_attribution_modelling.sy_TIGER_TRAP_INCSALES`
	)
	
	SELECT a.* FROM
	(
		(
			SELECT distinct crn, time_utc, Date(time_utc, "Australia/Sydney") as date
				, banner
				, channel
				, CONCAT(channel, '_', event) as channel_event
				, CASE WHEN campaign_code is null then '_AlwaysOn' else campaign_code end as campaign_code
				, campaign_start_date
				, campaign_end_date
			FROM  `wx-bq-poc.digital_attribution_modelling.dacamp_prod_eventstore_view_snapshot`
			where (campaign_code like '%OSP-2373%' or campaign_code is null)
					and banner in ('supermarkets')
					and date BETWEEN (select date from min_date) AND DATE_ADD((select date from min_date), INTERVAL 6 DAY)
		) a
		inner join
		(
			SELECT distinct crn from `wx-bq-poc.digital_attribution_modelling.sy_TIGER_TRAP_INCSALES`
		) b on a.crn = b.crn
	)
	where campaign_code <> '_AlwaysOn'
);


Create table `wx-bq-poc.digital_attribution_modelling.datiger_prod_event_mw` as 
(	
	with base_act as 
	(
		SELECT b.banner
				, d.campaign_code
				, 'Tiger' as campaign_type
				, d.campaign_start_date
				, d.campaign_end_date 
				, b.crn
				, b.date
				, b.time_utc
				, CONCAT(d.campaign_code, '_', b.channel_event) as channel_event
				, lead(time_utc) over (partition by b.crn, b.banner, d.campaign_code, d.campaign_start_date, d.campaign_end_date order by b.time_utc desc) as time_utc_pre
				, lead(CONCAT(d.campaign_code, '_', b.channel_event)) over (partition by b.crn, b.banner, d.campaign_code, d.campaign_start_date, d.campaign_end_date order by b.time_utc desc) as channel_event_pre
				, d.campaign_end_date as end_date 
		FROM
		(
			SELECT distinct banner, crn, time_utc, date, channel_event 
			from `wx-bq-poc.digital_attribution_modelling.datiger_prod_event`
		) b 	
		inner join
		(
			select distinct crn, campaign_code, campaign_start_date, campaign_end_date
			from `wx-bq-poc.digital_attribution_modelling.datiger_prod_redeemer`
		) d on b.crn = d.crn 
	)
	
	
	SELECT banner, a.campaign_code, campaign_type, campaign_start_date, campaign_end_date, a.crn, time_utc, date, channel_event, conv_time, conv_flag, case when spend is null then 0 else spend end as spend
	from
	(
		select * 
		from base_act 
	) a
	left join
	(
		select distinct crn, campaign_code, conv_flag, conv_time, spend 
				from `wx-bq-poc.digital_attribution_modelling.datiger_prod_redeemer`
				where conv_flag > 0
	) b on a.campaign_code = b.campaign_code and a.crn = b.crn
);


