/* DROP TABLE IF EXISTS `digital_attribution_modelling.datiger_prod_trans_matrix`;
CREATE TABLE `digital_attribution_modelling.datiger_prod_trans_matrix` as
(
	with states as 
	(
		with base as 
		(
			SELECT banner, campaign_code, campaign_start_date, campaign_end_date,
				case when channel_event_pre is null then 'START' else channel_event_pre end as state1,
				channel_event as state2,
				case when channel_event_post is null then 'NO CONV' else channel_event_post end as state3,
				count(*) as cnt,
				sum(spend) as spend
			FROM
			(
				SELECT a.crn, a.banner, a.campaign_code, a.campaign_start_date, a.campaign_end_date, time_utc,
				 lead(channel_event) over (partition by a.crn, a.banner, a.campaign_code, a.campaign_start_date, a.campaign_end_date order by time_utc desc) as channel_event_pre,
				 channel_event, 
				 lag(channel_event) over (partition by a.crn, a.banner, a.campaign_code, a.campaign_start_date, a.campaign_end_date order by time_utc desc) as channel_event_post,
				 row_number() over (partition by a.crn, a.banner, a.campaign_code, a.campaign_start_date, a.campaign_end_date order by time_utc desc) as pos,
				 case when spend is null then 0 else spend end as spend
				FROM
				(
					SELECT banner, campaign_code, campaign_start_date, campaign_end_date, crn, time_utc, channel_event, spend
					FROM `wx-bq-poc.digital_attribution_modelling.datiger_prod_event_mw` 

					UNION ALL

					SELECT banner, campaign_code, campaign_start_date, campaign_end_date, crn, min(conv_time) as time_utc, 'CONV' as channel_event, min(spend) as spend
					FROM `wx-bq-poc.digital_attribution_modelling.datiger_prod_event_mw`
					group by 1,2,3,4,5	
				) a 
			)
			where (channel_event_pre <> 'CONV' or channel_event_pre is null) AND not (channel_event = 'CONV' and channel_event_pre is null)
			group by 1,2,3,4,5,6,7
	    )

		SELECT banner, campaign_code, campaign_start_date, campaign_end_date, state1, state2, sum(cnt) as cnt, sum(spend) as spend
		FROM 
		(
			SELECT banner, campaign_code, campaign_start_date, campaign_end_date, state1, state2, cnt, spend from base
			UNION ALL
			SELECT banner, campaign_code, campaign_start_date, campaign_end_date, state2, state3, cnt, spend from base where state3 = 'NO CONV' AND state2 <> 'CONV'
		)
		group by 1,2,3,4,5,6
	)

	SELECT 'Total' as segment, a.banner, a.campaign_code, a.campaign_start_date, a.campaign_end_date, a.state1, a.state2, a.cnt, a.spend as spend, a.spend/(b.spend_state1+1e-6) as prob, a.cnt/b.cnt_state1 as prob_cnt
	FROM
	(
		(
			SELECT * FROM states
		) a
		left join
		(
			SELECT banner, campaign_code, campaign_start_date, campaign_end_date, state1, sum(cnt) as cnt_state1, sum(spend) as spend_state1
			from states 
			group by 1,2,3,4,5
		) b on a.banner = b.banner and a.campaign_code = b.campaign_code and a.campaign_start_date = b.campaign_start_date and a.campaign_end_date = b.campaign_end_date and a.state1 = b.state1 
	)

); */


DROP TABLE IF EXISTS `digital_attribution_modelling.datiger_prod_trans_matrix`;
CREATE TABLE `digital_attribution_modelling.datiger_prod_trans_matrix` as
(
	with states as 
	(
		with base as 
		(
			
			SELECT banner, campaign_code, campaign_start_date, campaign_end_date,
				case when channel_event_pre is null then 'START' else channel_event_pre end as state1,
				channel_event as state2,
				case when channel_event_post is null then 'NO CONV' else channel_event_post end as state3,
				count(*) as cnt,
				sum(spend) as spend
			FROM
			(
				(
					SELECT a.crn, a.banner, a.campaign_code, a.campaign_start_date, a.campaign_end_date, time_utc,
					 lead(channel_event) over (partition by a.crn, a.banner, a.campaign_code, a.campaign_start_date, a.campaign_end_date order by time_utc desc) as channel_event_pre,
					 channel_event, 
					 lag(channel_event) over (partition by a.crn, a.banner, a.campaign_code, a.campaign_start_date, a.campaign_end_date order by time_utc desc) as channel_event_post,
					 row_number() over (partition by a.crn, a.banner, a.campaign_code, a.campaign_start_date, a.campaign_end_date order by time_utc desc) as pos,
					 case when spend is null then 0 else spend end as spend
					FROM
					(
						SELECT banner, campaign_code, campaign_start_date, campaign_end_date, crn, time_utc, channel_event 
						FROM `wx-bq-poc.digital_attribution_modelling.datiger_prod_event_mw` 

						UNION ALL

						SELECT banner, campaign_code, campaign_start_date, campaign_end_date, crn, conv_time as time_utc, 'CONV' as channel_event
						FROM `wx-bq-poc.digital_attribution_modelling.datiger_prod_redeemer`
						where conv_flag = 1
					) a 
					left join 
					(
						SELECT crn, banner, campaign_code, campaign_start_date, campaign_end_date, spend
						FROM `wx-bq-poc.digital_attribution_modelling.datiger_prod_redeemer`
						where conv_flag = 1
					) b on a.crn = b.crn and a.banner = b.banner and a.campaign_code = b.campaign_code and a.campaign_start_date = b.campaign_start_date and a.campaign_end_date = b.campaign_end_date
					
				)	

			)
			
			where (channel_event_pre <> 'CONV' or channel_event_pre is null) AND not (channel_event = 'CONV' and channel_event_pre is null)
			group by 1,2,3,4,5,6,7
	    )

		SELECT banner, campaign_code, campaign_start_date, campaign_end_date, state1, state2, sum(cnt) as cnt, sum(spend) as spend
		FROM 
		(
			SELECT banner, campaign_code, campaign_start_date, campaign_end_date, state1, state2, cnt, spend from base
			UNION ALL
			SELECT banner, campaign_code, campaign_start_date, campaign_end_date, state2, state3, cnt, spend from base where state3 = 'NO CONV' AND state2 <> 'CONV'
		)
		group by 1,2,3,4,5,6
	)

	SELECT 'Total' as segment, a.banner, a.campaign_code, a.campaign_start_date, a.campaign_end_date, a.state1, a.state2, a.cnt, a.spend as spend, a.spend/(b.spend_state1+1e-6) as prob, a.cnt/b.cnt_state1 as prob_cnt
	FROM
	(
		(
			SELECT * FROM states
		) a
		left join
		(
			SELECT banner, campaign_code, campaign_start_date, campaign_end_date, state1, sum(cnt) as cnt_state1, sum(spend) as spend_state1
			from states 
			group by 1,2,3,4,5
		) b on a.banner = b.banner and a.campaign_code = b.campaign_code and a.campaign_start_date = b.campaign_start_date and a.campaign_end_date = b.campaign_end_date and a.state1 = b.state1 
	)

);




