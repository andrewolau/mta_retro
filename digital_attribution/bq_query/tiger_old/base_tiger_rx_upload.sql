drop table if exists loyalty_modeling.datiger_prod_mc_final_agg_temp;
create table loyalty_modeling.datiger_prod_mc_final_agg_temp
(
campaign_code varchar(250) encode zstd,
campaign_start_date date encode zstd,
offer_nbr varchar(250) encode zstd,
channel varchar(250) encode zstd,
volume  numeric(12,1) encode zstd,
reach  numeric(12,1) encode zstd,
channel_prob_norm numeric(12,10) encode zstd
)
diststyle all;