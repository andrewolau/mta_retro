drop table if exists `wx-bq-poc.digital_attribution_modelling.datiger_prod_mc_final_agg`;
create table `wx-bq-poc.digital_attribution_modelling.datiger_prod_mc_final_agg` as
(
	SELECT  
		SUBSTR(campaign_code,1,8) as campaign_code
		,campaign_start_date
		,SUBSTR(campaign_code,10,16) as offer_nbr
		,REPLACE(medium, CONCAT(campaign_code,'_'), '') as channel
		,sum(event_volume) as volume
		,avg(medium_reach) as reach
		,sum(channel_prob_norm) as channel_prob_norm
	FROM `wx-bq-poc.digital_attribution_modelling.datiger_prod_mc_final`
	group by 1,2,3,4
)
;

select * from `wx-bq-poc.digital_attribution_modelling.datiger_prod_mc_final_agg`;