drop table if exists `wx-bq-poc.digital_attribution_modelling.datiger_prod_mc_final_crn`;
create table `wx-bq-poc.digital_attribution_modelling.datiger_prod_mc_final_crn` as
(

	SELECT c.crn
		, e.segment_cvm
		, e.segment_lifestage
		, f.segment_marketable
		, c.banner
		, c.campaign_code
		, 'Tiger' as campaign_type
		, c.campaign_start_date
		, c.campaign_end_date
		, case when channel is null and segment_marketable = 'Y' then 'email' when channel is null and segment_marketable = 'N' then 'rw_app' else channel end as channel
		, case when medium is null and segment_marketable = 'Y' then 'email' when medium is null and segment_marketable = 'N' then 'rw_app' else medium end as medium	 
		, case when event is null and segment_marketable = 'Y' then 'email_open' when event is null and segment_marketable = 'N' then 'rw_app_imp' else event end as event
		, c.spend as inc_sales
		, case when channel_prob_norm is not null then c.spend * channel_prob_norm / (sum(channel_prob_norm) OVER (PARTITION BY c.crn, a.key)) 
				WHEN channel_prob_norm is null and channel_event is not null then 0 
				 else c.spend end as attributed_inc_sales
	FROM
	(
		SELECT concat('Total', '_', banner, '_', campaign_code, '_', campaign_start_date, '_', campaign_end_date) as key, *
		FROM `wx-bq-poc.digital_attribution_modelling.datiger_prod_redeemer`
		where conv_flag = 1
		order by crn, key
	) c 
	
	left join
	(		
		SELECT distinct 
			concat('Total', '_', banner, '_', campaign_code, '_', cast(campaign_start_date as string), '_', cast(campaign_end_date as string)) as key
			, crn 
			, channel_event 
		FROM `wx-bq-poc.digital_attribution_modelling.datiger_prod_event_mw` 
	) b on b.key = c.key and b.crn = c.crn
	
	left join 
	(
		select * from `wx-bq-poc.digital_attribution_modelling.datiger_prod_mc_final`
	) a on a.key = b.key and a.event = b.channel_event
	
	left join
	(
		SELECT crn, max(macro_segment_curr) as segment_cvm, max(lifestage) as segment_lifestage
		FROM `wx-bq-poc.wx_lty_digital_attribution_dev.redx_loyalty_customer_value_model`
		where Date(pw_end_date_utc) = (select max(Date(pw_end_date_utc)) from `wx-bq-poc.wx_lty_digital_attribution_dev.redx_loyalty_customer_value_model`)
		group by 1
	) e on c.crn = e.crn
	
	left join
	(
		SELECT crn, max(marketable) as segment_marketable
		FROM `wx-bq-poc.digital_attribution_modelling.marketable_crn`
		group by 1
	) f on c.crn = f.crn
	
	order by c.campaign_code, crn, channel_event
);

select * from `wx-bq-poc.digital_attribution_modelling.datiger_prod_mc_final_crn`;
