DELETE FROM loyalty_modeling.datiger_prod_mc_final_agg using loyalty_modeling.datiger_prod_mc_final_agg_temp
where loyalty_modeling.datiger_prod_mc_final_agg.campaign_code = loyalty_modeling.datiger_prod_mc_final_agg_temp.campaign_code 
		and loyalty_modeling.datiger_prod_mc_final_agg.campaign_start_date = loyalty_modeling.datiger_prod_mc_final_agg_temp.campaign_start_date;

INSERT INTO loyalty_modeling.datiger_prod_mc_final_agg SELECT * FROM loyalty_modeling.datiger_prod_mc_final_agg_temp;

--------

DROP TABLE IF EXISTS loyalty_modeling.TIGER_TRAP_INCSALES_DA;

CREATE TABLE loyalty_modeling.TIGER_TRAP_INCSALES_DA AS

With UC_CRN as (
SELECT DISTINCT
CAMPAIGN_CODE,
CAMPAIGN_START_DATE,
TARGET_CRN,
UC_CRN,
OFFER_NBR,
MATCH,
UC_TPG_SALES,
L4W_UC_TPG_SALES AS AVG_UC_TPG_SALES,
UC_BASKET_SALES
FROM loyalty_bi_analytics.uc_spend_lkp_NJ_TEMP
),

POC2 AS (
SELECT 
*,  
CASE WHEN offer_type LIKE 'Dollars%' THEN NVL(bskt_sales_prom,0) - NVL(cost,0) ELSE bskt_sales_prom END AS bskt_sales_prom_no_adj
FROM loyalty_bi_analytics.tiger_campaign_crn_tpg_details
WHERE campaign_audience_type = 'T'
),

-- THIS CTE CONTAINS CRN AND UC_CRN LEVEL TPG AND BASKET SALES BY CAMPAIGN AND OFFER.
TARGET_RESULTS_BASKET AS (
SELECT DISTINCT 
	A.campaign_code, 
	'Tiger Manual' as campaign_name,  
	A.campaign_start_date, 
	A.crn as Target_crn,
	bskt_sales_prom_no_adj+SUM(CASE WHEN offer_type LIKE 'Dollars%' THEN NVL(cost,0) ELSE 0 END) AS TARGET_BASKET_SALES
FROM POC2 A
WHERE A.campaign_audience_type = 'T'
GROUP BY 1,2,3,4, a.bskt_sales_prom_no_adj
),

TARGET_RESULTS_TPG AS (
SELECT DISTINCT 
	A.campaign_code, 
	'Tiger Manual' as campaign_name, 
	A.campaign_start_date, 
	A.crn as Target_crn,
--	A.SUPPLIER, -- NEW
	A.OFFER_NBR,
	A.OFFER_DESC,
	SUM(A.REDEEMED_TIMES) AS Redeem_times,
	SUM(tpg_sales_L4W) AS tpg_sales_L4W,
	SUM(tpg_sales_prom) TARGET_TPG_SALES
FROM POC2 A
WHERE A.campaign_audience_type = 'T'
GROUP BY 1,2,3,4,5,6, offer_type
),

COMBINED AS (
SELECT DISTINCT
	A.campaign_code, 
	A.campaign_name, 
	A.campaign_start_date, 
	A.Target_crn,
	B.OFFER_NBR,
	B.OFFER_DESC,
	B.Redeem_times,
	A.TARGET_BASKET_SALES,
	B.TARGET_TPG_SALES,
	B.tpg_sales_L4W
--	CASE WHEN B.tpg_sales_L4W = 0 THEN NULL ELSE B.tpg_sales_L4W END AS avg_tpg_sales_L4W
FROM TARGET_RESULTS_BASKET A
	INNER JOIN TARGET_RESULTS_TPG B
		ON A.Target_crn = B.Target_crn
		AND A.CAMPAIGN_CODE = B.CAMPAIGN_CODE
		AND A.CAMPAIGN_START_DATE = B.CAMPAIGN_START_DATE
),

TARGET_RESULTS AS (
SELECT DISTINCT 
	A.campaign_code, 
	A.campaign_name, 
	A.campaign_start_date, 
	A.Target_crn,
	B.uc_crn,
	A.offer_nbr, 
	A.offer_desc, 
	A.Redeem_times,
	A.TARGET_TPG_SALES AS TARGET_TPG_SALES,
	B.UC_TPG_SALES,
	A.TARGET_BASKET_SALES,
	B.UC_BASKET_SALES,
	B.MATCH,
	--A.TARGET_TPG_SALES - (B.UC_TPG_SALES * NULLIF((A.avg_tpg_sales_L4W/AVG_UC_TPG_SALES),0)) AS TPG_INC,
	A.TARGET_TPG_SALES - B.UC_TPG_SALES AS NADJ_TPG_INC,
	A.TARGET_BASKET_SALES - B.UC_BASKET_SALES AS BASKET_INC,
	A.tpg_sales_L4W AS Target_tpg_SALES_L4W,
	B.AVG_UC_TPG_SALES AS UC_tpg_SALES_L4W
FROM COMBINED A
	LEFT JOIN UC_CRN B 
		ON A.Target_crn = B.TARGET_CRN
		AND A.CAMPAIGN_CODE = B.CAMPAIGN_CODE
		AND A.CAMPAIGN_START_DATE = B.CAMPAIGN_START_DATE
		AND A.OFFER_NBR = B.OFFER_NBR
),

Offer_Redeem_Agg as (
Select distinct 
	CAMPAIGN_CODE,
	CAMPAIGN_START_DATE,
	'TARGET' AS GRP,
	target_crn as crn,
	offer_nbr,
	target_tpg_sales as promo_tpg_sales, 
	target_tpg_sales_l4w as L4W_tpg_sales
From TARGET_RESULTS a
Where uc_crn is not null

union

Select distinct 
	CAMPAIGN_CODE,
	CAMPAIGN_START_DATE,
	'UC' AS GRP,
	uc_crn,
	offer_nbr,
	uc_tpg_sales,
	uc_tpg_sales_l4w
From TARGET_RESULTS a
Where uc_crn is not null
),

Offer_agg as (
Select Distinct 
	*,
	TARGET_promo_tpg_sales - (uc_promo_tpg_sales * ((TARGET_L4W_AVG_tpg_sales/TARGET_CRN_COUNT)/(NULLIF(UC_L4W_AVG_tpg_sales,0)/UC_CRN_COUNT))) AS TPG_INC_SALES,
	(TARGET_promo_tpg_sales - (uc_promo_tpg_sales * ((TARGET_L4W_AVG_tpg_sales/TARGET_CRN_COUNT)/(NULLIF(UC_L4W_AVG_tpg_sales,0)/UC_CRN_COUNT))))/target_crn_count AS TPG_INC_AVG	
From  (
			Select Distinct
				A.CAMPAIGN_CODE,
				A.CAMPAIGN_START_DATE,
				A.offer_nbr,
				count(distinct CASE WHEN A.GRP = 'TARGET' THEN A.crn END) as TARGET_CRN_COUNT,
				count(distinct CASE WHEN A.GRP = 'UC' THEN A.crn END) as UC_CRN_COUNT,
				sum(CASE WHEN A.GRP = 'TARGET' THEN A.promo_tpg_sales END) as TARGET_promo_tpg_sales,
				sum(CASE WHEN A.GRP = 'UC' THEN A.promo_tpg_sales END) as UC_promo_tpg_sales,
				sum(CASE WHEN A.GRP = 'TARGET' THEN A.L4W_tpg_sales END)/4 as TARGET_L4W_AVG_tpg_sales,
				sum(CASE WHEN A.GRP = 'UC' THEN A.L4W_tpg_sales END)/4 as UC_L4W_AVG_tpg_sales
			From Offer_Redeem_Agg A
			Group by 1,2,3
			) A
)

SELECT A.*, channel, volume, reach, channel_prob_norm*TPG_INC_AVG as ATTR_TPG_INC_AVG
FROM
(
SELECT DISTINCT
	A.campaign_code, 
	A.campaign_name, 
	A.campaign_start_date, 
	A.offer_nbr, 
	A.offer_desc, 
	--b.TPG_INC_AVG,
	AVG(CASE WHEN b.TPG_INC_AVG IS NULL THEN A.NADJ_TPG_INC ELSE b.TPG_INC_AVG END) AS TPG_INC_AVG,
	SUM(A.TARGET_TPG_SALES) AS TARGET_TPG_SALES,
	SUM(A.TARGET_BASKET_SALES) AS TARGET_BASKET_SALES,
	AVG(A.BASKET_INC) AS BASKET_INC_AVG,
	SUM(A.Target_tpg_SALES_L4W)/4 AS tpg_sales_L4W,
	SUM(A.REDEEM_TIMES) AS REDEEM_TIMES,
	COUNT(DISTINCT CASE WHEN A.uc_crn IS NOT NULL THEN A.TARGET_CRN END) AS MATCHED_CRN
FROM TARGET_RESULTS A
	LEFT JOIN Offer_agg B
		ON A.offer_nbr = B.offer_nbr
		AND a.campaign_code = b.campaign_code
		AND a.campaign_start_date = b.campaign_start_date  
GROUP BY 1,2,3,4,5
ORDER BY 1,3
) A
LEFT JOIN
(
SELECT * FROM loyalty_modeling.datiger_prod_mc_final_agg
) B ON A.campaign_code = B.campaign_code AND A.campaign_start_date = B.campaign_start_date AND A.offer_nbr = B.offer_nbr
INNER JOIN
(
SELECT distinct campaign_code, campaign_start_date FROM loyalty_modeling.datiger_prod_mc_final_agg
) C ON A.campaign_code = C.campaign_code and A.campaign_start_date = C.campaign_start_date
;

grant select on loyalty_modeling.TIGER_TRAP_INCSALES_DA to public;