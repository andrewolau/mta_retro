drop table if exists `wx-bq-poc.digital_attribution_modelling.dacamp_prod_mc_final_crn_v_old_20210405`;
create table `wx-bq-poc.digital_attribution_modelling.dacamp_prod_mc_final_crn_v_old_20210405` as
(
	with onl_start_date as 
	(	
		select max(campaign_start_date) from `wx-bq-poc.digital_attribution_modelling.dacamp_prod_event_mw_v_old_20210405` where campaign_code = 'ONLINE'
	),

	onl_end_date as 
	(
		select max(campaign_end_date) from `wx-bq-poc.digital_attribution_modelling.dacamp_prod_event_mw_v_old_20210405` where campaign_code = 'ONLINE'
	)

	SELECT distinct DATE_TRUNC(DATE_ADD((select * from onl_end_date),interval 0 day), week(Monday)) as week
		, c.crn
		, e.segment_cvm
		, e.segment_lifestage
		, f.segment_marketable
		, c.banner
		, c.campaign_code
		, c.campaign_type
		, c.campaign_start_date
		, c.campaign_end_date
		, c.tot_spend as total_sales
		, c.spend as inc_sales
		, channel_event
		, channel_prob_norm
		, case when a.channel is not null then a.channel else d.channel end as channel
		, case when a.medium is not null then a.medium else d.medium end as medium
		, case when a.event is not null then a.event else d.event end as event
		, case when channel_prob_norm is not null then channel_prob_norm / (sum(channel_prob_norm) OVER (PARTITION BY c.crn, b.key)) 
				when channel_prob_norm is null and channel_event is not null then 0 
				else 1 end as attributed_conversion
		, case when channel_prob_norm is not null then c.tot_spend * channel_prob_norm / (sum(channel_prob_norm) OVER (PARTITION BY c.crn, b.key)) 
				when  channel_prob_norm is null and channel_event is not null then 0 
				else c.tot_spend end as attributed_total_sales
		, case when channel_prob_norm is not null then c.spend * channel_prob_norm / (sum(channel_prob_norm) OVER (PARTITION BY c.crn, b.key)) 
				WHEN channel_prob_norm is null and channel_event is not null then 0 
				 else c.spend end as attributed_inc_sales
	FROM
	(
		SELECT 
			concat('Total', '_', banner, '_', campaign_code, '_', cast(campaign_start_date as string), '_', cast(campaign_end_date as string)) as key
			, banner
			, crn
			, campaign_code
			, campaign_start_date
			, campaign_end_date
			, campaign_end_date_real
			, campaign_type
			, case when sum(tot_spend) is null then 0 else sum(tot_spend) end as tot_spend
			, case when sum(spend) is null then 0 else sum(spend) end as spend
		FROM
		(
			SELECT crn
				, banner
				, campaign_code
				, campaign_start_date
				, campaign_end_date
				, campaign_end_date_real
				, campaign_type
				, min(time_utc) as conv_time
				, sum(tot_spend) as tot_spend
				, sum(spend) as spend
			FROM `wx-bq-poc.digital_attribution_modelling.dacamp_prod_activation_v_old_20210405`
			group by 1,2,3,4,5,6,7
			
			union all
			
			SELECT crn
				, 'supermarkets' as banner
				, 'ONLINE' as campaign_code
				, (select * from onl_start_date) as campaign_start_date
				, (select * from onl_end_date) as campaign_end_date
				, (select * from onl_end_date) as campaign_end_date_real
				, 'ONLINE' as campaign_type
				, min(time_utc) as conv_time
				, sum(tot_spend) as tot_spend
				, sum(spend) as tot_spend
			FROM `wx-bq-poc.digital_attribution_modelling.dacamp_prod_online_sales_v_old_20210405`
			group by 1
		)
		group by 1,2,3,4,5,6,7,8
	) c 
	
	left join
	(		
		SELECT 
			concat('Total', '_', banner, '_', campaign_code, '_', cast(campaign_start_date as string), '_', cast(campaign_end_date as string)) as key
			, crn 
			, channel_event 
			, count(*) as event_volume
		FROM `wx-bq-poc.digital_attribution_modelling.dacamp_prod_event_mw_v_old_20210405` 
		group by 1,2,3
		order by 1,2,3
	) b on b.key = c.key and b.crn = c.crn
	
	left join 
	(
		select * from `wx-bq-poc.digital_attribution_modelling.dacamp_prod_mc_final_v_old_20210405`
	) a on a.key = b.key and a.event = b.channel_event 
	
	left join
	(
		SELECT a1.key
		, CASE WHEN channel is null and campaign_code not like '%NM%' then concat(SUBSTR(campaign_code,1,9), '_email') 
				WHEN channel is null and campaign_code like '%NM%' then concat(SUBSTR(campaign_code,1,9), '_rw_app') 
				else channel end as channel
		, CASE WHEN medium is null and campaign_code not like '%NM%' then concat(SUBSTR(campaign_code,1,9), '_email') 
				WHEN medium is null and campaign_code like '%NM%' then concat(SUBSTR(campaign_code,1,9), '_rw_app') 
				else medium end as medium
		,  CASE WHEN event is null and campaign_code not like '%NM%' then concat(SUBSTR(campaign_code,1,9), '_email_open') 
				WHEN event is null and campaign_code like '%NM%' then concat(SUBSTR(campaign_code,1,9), '_rw_app_imp') 
				else event end as event
		, event_volume
		, event_reach
		, event_converted_crn
		, medium_reach
		, medium_converted_crn
		FROM
		(
			select distinct key, campaign_code from `wx-bq-poc.digital_attribution_modelling.dacamp_prod_mc_final_v_old_20210405`
		) a1
		left join
		(	SELECT key
				, channel
				, medium
				, event
				, event_volume
				, event_reach
				, event_converted_crn
				, medium_reach
				, medium_converted_crn
			from `wx-bq-poc.digital_attribution_modelling.dacamp_prod_mc_final_v_old_20210405`
			where (campaign_code = 'ONLINE' and event like '%wow_web_view%')
				or (campaign_code like '%NM%' and regexp_contains(event, r'(?i)rw_app_imp') and substr(event,1,9) = substr(campaign_code,1,9))
        or (campaign_code not like '%NM%' and campaign_code <> 'ONLINE' and regexp_contains(event, r'(?i)email_open') and substr(event,1,9) = substr(campaign_code,1,9))
		) a2 on a1.key = a2.key
	) d on c.key = d.key
	
	left join
	(
		SELECT crn, max(macro_segment_curr) as segment_cvm, max(lifestage) as segment_lifestage
		FROM `wx-bq-poc.wx_lty_digital_attribution_dev.redx_loyalty_customer_value_model`
		where Date(pw_end_date_utc) = (select max(Date(pw_end_date_utc)) from `wx-bq-poc.wx_lty_digital_attribution_dev.redx_loyalty_customer_value_model`)
		group by 1
	) e on c.crn = e.crn
	
	left join
	(
		SELECT crn, max(marketable) as segment_marketable
		FROM `wx-bq-poc.digital_attribution_modelling.marketable_crn_v_old_20210405`
		group by 1
	) f on c.crn = f.crn
	
);


drop table if exists `wx-bq-poc.digital_attribution_modelling.dacamp_prod_mc_final_crn_temp_v_old_20210405`;
create table `wx-bq-poc.digital_attribution_modelling.dacamp_prod_mc_final_crn_temp_v_old_20210405` as
(
	SELECT *
	FROM `digital_attribution_modelling.dacamp_prod_mc_final_crn_v_old_20210405` 
);


DROP TABLE IF EXISTS `digital_attribution_modelling.sy_rw_app_ws_safarievent_v_old_20210405`;

CREATE TABLE `digital_attribution_modelling.sy_rw_app_ws_safarievent_v_old_20210405` as
(
	SELECT *, 'now' as load_date 
	from `loyalty_bi_analytics__prod_v3.safarievents_final_20210405` 
	where campaign_code = 'CVM-1661' and cast(fw_start_date as date) > cast(campaign_start_date as date)
);


DROP TABLE IF EXISTS `digital_attribution_modelling.sy_rw_app_ws_target_v_old_20210405`;

CREATE TABLE `digital_attribution_modelling.sy_rw_app_ws_target_v_old_20210405` as
(
  SELECT distinct b.*
  FROM
  (
  SELECT distinct 
    crn
    , DATE_TRUNC(Date(post_time, "Australia/Sydney"), week(Wednesday)) as pw_start_date
	, DATE_TRUNC(Date(post_time, "Australia/Sydney"), week(Monday)) as fw_start_date
  FROM `wx-bq-poc.wx_lty_digital_attribution_dev.tealium_rewards_app`
  where Date(post_time)>=Date('2020-06-29')
  ) a
  inner join
  (
	SELECT fw_start_date, crn, campaign_start_date, count(distinct purchase_datetime) as visit, sum(sales_amt) as spend 
	FROM `wx-bq-poc.digital_attribution_modelling.sy_rw_app_ws_safarievent_v_old_20210405` 
	where open_datetime is null and purchase_datetime is not null and campaign_code = 'CVM-1661'
	group by 1,2,3
  ) b on a.crn = b.crn and a.pw_start_date = b.campaign_start_date and a.fw_start_date = b.fw_start_date
  
  where b.spend > 0
);


DROP TABLE IF EXISTS `digital_attribution_modelling.sy_rw_app_ws_control_v_old_20210405`;

CREATE TABLE `digital_attribution_modelling.sy_rw_app_ws_control_v_old_20210405` as
(
  SELECT distinct b.*
  FROM
  (
		  SELECT distinct a.crn, Date_add(a.pw_start_date, interval 7 day) as pw_start_date, Date_add(a.fw_start_date, interval 7 day) as fw_start_date

		  FROM
		  (
		  SELECT distinct 
				crn
				, DATE_TRUNC(Date(post_time, "Australia/Sydney"), week(Wednesday)) as pw_start_date
				, DATE_TRUNC(Date(post_time, "Australia/Sydney"), week(Monday)) as fw_start_date
			  FROM `wx-bq-poc.wx_lty_digital_attribution_dev.tealium_rewards_app`
			  where Date(post_time)>=Date('2020-06-29')
		  ) a
		  
		  left join
		  (
			SELECT distinct 
			crn
			, DATE_TRUNC(Date(post_time, "Australia/Sydney"), week(Wednesday)) as pw_start_date
			, DATE_TRUNC(Date(post_time, "Australia/Sydney"), week(Monday)) as fw_start_date
			FROM `wx-bq-poc.wx_lty_digital_attribution_dev.tealium_rewards_app`
			where Date(post_time)>=Date('2020-06-29')
		  ) b on a.crn = b.crn and Date_add(a.pw_start_date, interval 7 day) = b.pw_start_date and Date_add(a.fw_start_date, interval 7 day) = b.fw_start_date
		  where b.crn is null
	) a
	
	inner join
	(
		SELECT fw_start_date, crn, campaign_start_date, count(distinct purchase_datetime) as visit, sum(sales_amt) as spend 
		FROM `wx-bq-poc.digital_attribution_modelling.sy_rw_app_ws_safarievent_v_old_20210405` 
		where open_datetime is null and purchase_datetime is not null and campaign_code = 'CVM-1661'
		group by 1,2,3
	) b on a.crn = b.crn and a.pw_start_date = b.campaign_start_date and a.fw_start_date = b.fw_start_date
  
  where b.spend > 0
);


DROP TABLE IF EXISTS `digital_attribution_modelling.sy_rw_app_ws_v_old_20210405`;
CREATE TABLE `digital_attribution_modelling.sy_rw_app_ws_v_old_20210405` as
(
select t.fw_start_date, t.campaign_start_date, target * (spend_per_target - spend_per_control) as ss
from
(
SELECT fw_start_date, campaign_start_date, count(distinct crn) as target, sum(spend)/count(distinct crn) as spend_per_target, 
from `digital_attribution_modelling.sy_rw_app_ws_target_v_old_20210405`
group by 1,2
) t
inner join
(
SELECT fw_start_date, campaign_start_date, count(distinct crn) as control, sum(spend)/count(distinct crn) as spend_per_control, 
from `digital_attribution_modelling.sy_rw_app_ws_control_v_old_20210405`
group by 1,2
) c on t.fw_start_date = c.fw_start_date and t.campaign_start_date = c.campaign_start_date
order by 1,2
);