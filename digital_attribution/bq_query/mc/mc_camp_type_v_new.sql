/* 
select distinct segment, banner, campaign_code, campaign_start_date, campaign_end_date, concat(segment, '_', banner, '_', campaign_code, '_', cast(campaign_start_date as string), '_', cast(campaign_end_date as string)) as key 
from `digital_attribution_modelling.dacamp_prod_trans_matrix`
where segment = 'Total' 
order by 1,2,3,4,5; 
*/


select distinct 'Total' as segment, banner, campaign_code, campaign_type, campaign_start_date, campaign_end_date, concat('Total', '_', banner, '_', campaign_code, '_', cast(campaign_start_date as string), '_', cast(campaign_end_date as string)) as key 
from `digital_attribution_modelling.dacamp_prod_event_mw_v_new_20210405`
order by 1,2,3,4,5,6


