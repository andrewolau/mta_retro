/* 
select distinct segment, banner, campaign_code, campaign_start_date, campaign_end_date, concat(segment, '_', banner, '_', campaign_code, '_', cast(campaign_start_date as string), '_', cast(campaign_end_date as string)) as key 
from `digital_attribution_modelling.dacamp_prod_trans_matrix`
where segment = 'Total' 
order by 1,2,3,4,5; 
*/


select distinct 'Total' as segment, a.banner, a.campaign_code, a.campaign_type, a.campaign_start_date, b.campaign_end_date_real as campaign_end_date, concat('Total', '_', a.banner, '_', a.campaign_code, '_', cast(a.campaign_start_date as string), '_', cast(b.campaign_end_date as string)) as key 
from 
(select * from `digital_attribution_modelling.dacamp_prod_event_mw`) a
inner join
(
	SELECT distinct banner, campaign_code, campaign_start_date, campaign_end_date, campaign_end_date_real
	from `digital_attribution_modelling.dacamp_prod_activation`
) b on a.banner = b.banner and a.campaign_code = b.campaign_code and a.campaign_start_date = b.campaign_start_date and a.campaign_end_date = b.campaign_end_date
order by 1,2,3,4,5,6


