select
	DATE_TRUNC(DATE_ADD(_FW_START_DATE_, interval 7 day), week(Saturday)) as load_date
	, a.week as fw_start_date
	, a.banner as division
	, a.campaign_code
	, a.campaign_type
	, a.campaign_start_date
	, b.campaign_end_date_real as campaign_end_date
	, a.crn
	, c.uc_crn
	, 'N' as control
	, 'N' as tpg_control
	, a.segment_cvm as cvm
	, a.segment_lifestage as lifestage
	, a.segment_marketable as marketable
	, a.total_sales as tot_amt_incld_gst_promo
	, a.total_sales / (1.095) as tot_amt_excld_gst_promo
	, case when a.channel is null then CONCAT(a.campaign_code,'_email') else a.channel end as channel
	, case when a.medium is null then CONCAT(a.campaign_code,'_email') else a.medium end as medium
	, case when a.event is null then CONCAT(a.campaign_code,'_email_open') else a.event end as event
	, a.attributed_conversion
	, a.attributed_total_sales
	, a.attributed_inc_sales
from 
(
	SELECT * FROM `wx-bq-poc.digital_attribution_modelling.dacamp_prod_mc_final_crn`
) a
inner join
(
	SELECT distinct banner, campaign_code, campaign_start_date, campaign_end_date, campaign_end_date_real
	from `digital_attribution_modelling.dacamp_prod_activation`
) b on a.banner = b.banner and a.campaign_code = b.campaign_code and a.campaign_start_date = b.campaign_start_date and a.campaign_end_date = b.campaign_end_date
left join
(
	SELECT distinct fw_start_date, crn, campaign_code, campaign_start_date, max(uc_crn) as uc_crn
	FROM `Attribution_Safari_Matching.safari_store`
	where fw_start_date = _FW_START_DATE_
	group by 1,2,3,4
) c on a.crn = c.crn and a.campaign_code = c.campaign_code and a.campaign_start_date = c.campaign_start_date

union all

select 
	DATE_TRUNC(DATE_ADD(_FW_START_DATE_, interval 7 day), week(Saturday)) as load_date
	, fw_start_date
	, 'supermarkets' as division
	, cast(null as string) as campaign_code
	, cast(null as string) as campaign_type
	, cast(null as date) as campaign_start_date
	, cast(null as date) as campaign_end_date
	, uc_crn as crn
	, uc_crn as uc_crn
	, 'Y' as control
	, 'N' as tpg_control
	, segment_cvm as cvm
	, segment_lifestage as lifestage
	, segment_marketable as marketable
	, max(control_spend) as tot_amt_incld_gst_promo
	, max(control_spend) / (1.095) as tot_amt_excld_gst_promo
	, cast(null as string)
	, cast(null as string)
	, cast(null as string)
	, 0 as attributed_conversion
	, 0 as attributed_total_sales
	, 0 as attributed_inc_sales
from 
(
	SELECT distinct fw_start_date, uc_crn, control_spend
	FROM `Attribution_Safari_Matching.safari_store`
	where fw_start_date = _FW_START_DATE_
) a
left join
(
	SELECT crn, max(macro_segment_curr) as segment_cvm, max(lifestage) as segment_lifestage
	FROM `wx-bq-poc.wx_lty_digital_attribution_dev.redx_loyalty_customer_value_model`
	where Date(pw_end_date_utc) = (select max(Date(pw_end_date_utc)) from `wx-bq-poc.wx_lty_digital_attribution_dev.redx_loyalty_customer_value_model`)
	group by 1
) b on a.uc_crn = b.crn
left join
(
	SELECT crn, max(marketable) as segment_marketable
	FROM `wx-bq-poc.digital_attribution_modelling.marketable_crn`
	group by 1
) c on a.uc_crn = c.crn
group by 1,2,3,4,5,6,7,8,9,10,11,12,13,14

/* union all

select 
	DATE_TRUNC(DATE_ADD(_FW_START_DATE_, interval 7 day), week(Saturday)) as load_date
	, fw_start_date
	, 'supermarkets' as division
	, campaign_code as campaign_code
	, cast(null as string) as campaign_type
	, campaign_start_date as campaign_start_date
	, cast(null as date) as campaign_end_date
	, a.crn as crn
	, tpg_crn as uc_crn
	, 'Y' as control
	, 'Y' as tpg_control
	, segment_cvm as cvm
	, segment_lifestage as lifestage
	, segment_marketable as marketable
	, max(control_spend) as tot_amt_incld_gst_promo
	, max(control_spend) / (1.095) as tot_amt_excld_gst_promo
	, cast(null as string)
	, cast(null as string)
	, cast(null as string)
	, 0 as attributed_conversion
	, 0 as attributed_total_sales
	, 0 as attributed_inc_sales
from 
(
	SELECT distinct fw_start_date, crn, tpg_crn, campaign_start_date, campaign_code, control_spend
	FROM `Attribution_Safari_Matching.safari_store`
	where fw_start_date = _FW_START_DATE_
) a
left join
(
	SELECT crn, max(macro_segment_curr) as segment_cvm, max(lifestage) as segment_lifestage
	FROM `wx-bq-poc.wx_lty_digital_attribution_dev.redx_loyalty_customer_value_model`
	where Date(pw_end_date_utc) = (select max(Date(pw_end_date_utc)) from `wx-bq-poc.wx_lty_digital_attribution_dev.redx_loyalty_customer_value_model`)
	group by 1
) b on a.crn = b.crn
left join
(
	SELECT crn, max(marketable) as segment_marketable
	FROM `wx-bq-poc.digital_attribution_modelling.marketable_crn`
	group by 1
) c on a.crn = c.crn
group by 1,2,3,4,5,6,7,8,9,10,11,12,13,14 */

;