drop table if exists `wx-bq-poc.digital_attribution_modelling.dacamp_prod_mc_final_crn_backfill`;
create table `wx-bq-poc.digital_attribution_modelling.dacamp_prod_mc_final_crn_backfill` as
(
	SELECT *
	FROM `xueyuan.dacamp_prod_mc_union_backfill`

);

select
	DATE_TRUNC(current_date(), week(Monday)) as load_date
	, a.week as fw_start_date
	, a.banner as division
	, a.campaign_code
	, a.campaign_type
	, a.campaign_start_date
	, a.campaign_end_date as campaign_end_date
	, a.crn
	, cast(null as string) as uc_crn
	, 'N' as control
	, 'N' as tpg_control
	, a.segment_cvm as cvm
	, a.segment_lifestage as lifestage
	, a.segment_marketable as marketable
	, a.total_sales as tot_amt_incld_gst_promo
	, a.total_sales / (1.095) as tot_amt_excld_gst_promo
	, a.channel
	, a.medium
	, a.event
	, a.attributed_conversion
	, a.attributed_total_sales
	, a.attributed_inc_sales
from 
(
	SELECT * FROM `wx-bq-poc.digital_attribution_modelling.dacamp_prod_mc_final_crn_backfill`
) a ;