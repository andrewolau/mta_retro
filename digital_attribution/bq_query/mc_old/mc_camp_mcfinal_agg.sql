drop table if exists `wx-bq-poc.digital_attribution_modelling.dacamp_prod_mc_final_agg`;
create table `wx-bq-poc.digital_attribution_modelling.dacamp_prod_mc_final_agg` as
(
with base as
(
	select 'Total' as segment
		, banner
		, campaign_code
		, campaign_type
		, campaign_start_date
		, campaign_end_date
		, channel
		, medium
		, event
		, count(distinct crn) as event_converted_crn
		, sum(attributed_conversion) as attributed_redemption
		, sum(attributed_total_sales) as attributed_total_sales
		, sum(attributed_inc_sales) as attributed_inc_sales
	from `wx-bq-poc.digital_attribution_modelling.dacamp_prod_mc_final_crn`
	group by 1,2,3,4,5,6,7,8,9

	union all

	select case when segment_cvm is null then 'CVM - Unknown' else concat('CVM - ', segment_cvm) end as segment
		, banner
		, campaign_code
		, campaign_type
		, campaign_start_date
		, campaign_end_date
		, channel
		, medium
		, event
		, count(distinct crn) as event_converted_crn
		, sum(attributed_conversion) as attributed_redemption
		, sum(attributed_total_sales) as attributed_total_sales
		, sum(attributed_inc_sales) as attributed_inc_sales
	from `wx-bq-poc.digital_attribution_modelling.dacamp_prod_mc_final_crn`
	group by 1,2,3,4,5,6,7,8,9

	union all

	select case when segment_lifestage is null then 'Lifestage - Unknown' else concat('Lifestage - ', segment_lifestage) end as segment
		, banner
		, campaign_code
		, campaign_type
		, campaign_start_date
		, campaign_end_date
		, channel
		, medium
		, event
		, count(distinct crn) as event_converted_crn
		, sum(attributed_conversion) as attributed_redemption
		, sum(attributed_total_sales) as attributed_total_sales
		, sum(attributed_inc_sales) as attributed_inc_sales
	from `wx-bq-poc.digital_attribution_modelling.dacamp_prod_mc_final_crn`
	group by 1,2,3,4,5,6,7,8,9
	
	union all

	select case when segment_marketable is null then 'Marketable - Unknown' else concat('Marketable - ', segment_marketable) end as segment
		, banner
		, campaign_code
		, campaign_type
		, campaign_start_date
		, campaign_end_date
		, channel
		, medium
		, event
		, count(distinct crn) as event_converted_crn
		, sum(attributed_conversion) as attributed_redemption
		, sum(attributed_total_sales) as attributed_total_sales
		, sum(attributed_inc_sales) as attributed_inc_sales
	from `wx-bq-poc.digital_attribution_modelling.dacamp_prod_mc_final_crn`
	group by 1,2,3,4,5,6,7,8,9
),

base_medium as
(
		select 'Total' as segment
		, banner
		, campaign_code
		, campaign_type
		, campaign_start_date
		, campaign_end_date
		, channel
		, medium
		, count(distinct crn) as medium_converted_crn
	from `wx-bq-poc.digital_attribution_modelling.dacamp_prod_mc_final_crn`
	group by 1,2,3,4,5,6,7,8

	union all

	select case when segment_cvm is null then 'CVM - Unknown' else concat('CVM - ', segment_cvm) end as segment
		, banner
		, campaign_code
		, campaign_type
		, campaign_start_date
		, campaign_end_date
		, channel
		, medium
		, count(distinct crn) as medium_converted_crn
	from `wx-bq-poc.digital_attribution_modelling.dacamp_prod_mc_final_crn`
	group by 1,2,3,4,5,6,7,8

	union all

	select case when segment_lifestage is null then 'Lifestage - Unknown' else concat('Lifestage - ', segment_lifestage) end as segment
		, banner
		, campaign_code
		, campaign_type
		, campaign_start_date
		, campaign_end_date
		, channel
		, medium
		, count(distinct crn) as medium_converted_crn
	from `wx-bq-poc.digital_attribution_modelling.dacamp_prod_mc_final_crn`
	group by 1,2,3,4,5,6,7,8
	
	union all

	select case when segment_marketable is null then 'Marketable - Unknown' else concat('Marketable - ', segment_marketable) end as segment
		, banner
		, campaign_code
		, campaign_type
		, campaign_start_date
		, campaign_end_date
		, channel
		, medium
		, count(distinct crn) as medium_converted_crn
	from `wx-bq-poc.digital_attribution_modelling.dacamp_prod_mc_final_crn`
	group by 1,2,3,4,5,6,7,8
),


base_vol as
(
	SELECT 'Total' as segment
		, banner
		, campaign_code
		, campaign_type
		, campaign_start_date
		, campaign_end_date
		, channel_event as event
		, count(*) as event_volume
		, count(distinct crn) as event_reach 
	from `wx-bq-poc.digital_attribution_modelling.dacamp_prod_event_mw`
	group by 1,2,3,4,5,6,7

	union all

	select case when segment_cvm is null then 'CVM - Unknown' else concat('CVM - ', segment_cvm) end as segment
		, banner
		, campaign_code
		, campaign_type
		, campaign_start_date
		, campaign_end_date
		, channel_event as event
		, count(*) as event_volume
		, count(distinct a.crn) as event_reach
	from 
	(select * from `wx-bq-poc.digital_attribution_modelling.dacamp_prod_event_mw`) a
	left join
	(SELECT crn, max(macro_segment_curr) as segment_cvm, max(lifestage) as segment_lifestage
		FROM `wx-bq-poc.wx_lty_digital_attribution_dev.redx_loyalty_customer_value_model`
		where Date(pw_end_date_utc) = (select max(Date(pw_end_date_utc)) from `wx-bq-poc.wx_lty_digital_attribution_dev.redx_loyalty_customer_value_model`)
		group by 1
	) b on a.crn = b.crn
	group by 1,2,3,4,5,6,7

	union all

	select case when segment_lifestage is null then 'Lifestage - Unknown' else concat('Lifestage - ', segment_lifestage) end as segment
		, banner
		, campaign_code
		, campaign_type
		, campaign_start_date
		, campaign_end_date
		, channel_event as event
		, count(*) as event_volume
		, count(distinct a.crn) as event_reach 
	from 
	(select * from `wx-bq-poc.digital_attribution_modelling.dacamp_prod_event_mw`) a
	left join
	(SELECT crn, max(macro_segment_curr) as segment_cvm, max(lifestage) as segment_lifestage
		FROM `wx-bq-poc.wx_lty_digital_attribution_dev.redx_loyalty_customer_value_model`
		where Date(pw_end_date_utc) = (select max(Date(pw_end_date_utc)) from `wx-bq-poc.wx_lty_digital_attribution_dev.redx_loyalty_customer_value_model`)
		group by 1
	) b on a.crn = b.crn
	group by 1,2,3,4,5,6,7
	
	union all

	select case when segment_marketable is null then 'Marketable - Unknown' else concat('Marketable - ', segment_marketable) end as segment
		, banner
		, campaign_code
		, campaign_type
		, campaign_start_date
		, campaign_end_date
		, channel_event as event
		, count(*) as event_volume
		, count(distinct a.crn) as event_reach 
	from 
	(select * from `wx-bq-poc.digital_attribution_modelling.dacamp_prod_event_mw`) a
	left join
	(SELECT crn, max(marketable) as segment_marketable
		FROM `wx-bq-poc.digital_attribution_modelling.marketable_crn`
		group by 1
	) b on a.crn = b.crn
	group by 1,2,3,4,5,6,7
),


base_vol_medium as
(
	SELECT 'Total' as segment
		, banner
		, campaign_code
		, campaign_type
		, campaign_start_date
		, campaign_end_date
		, REGEXP_REPLACE(channel_event, r"_open|_imp|_clk|_view","") as medium
		, count(distinct crn) as medium_reach 
	from `wx-bq-poc.digital_attribution_modelling.dacamp_prod_event_mw`
	group by 1,2,3,4,5,6,7

	union all

	select case when segment_cvm is null then 'CVM - Unknown' else concat('CVM - ', segment_cvm) end as segment
		, banner
		, campaign_code
		, campaign_type
		, campaign_start_date
		, campaign_end_date
		, REGEXP_REPLACE(channel_event, r"_open|_imp|_clk|_view","") as medium
		, count(distinct a.crn) as medium_reach
	from 
	(select * from `wx-bq-poc.digital_attribution_modelling.dacamp_prod_event_mw`) a
	left join
	(SELECT crn, max(macro_segment_curr) as segment_cvm, max(lifestage) as segment_lifestage
		FROM `wx-bq-poc.wx_lty_digital_attribution_dev.redx_loyalty_customer_value_model`
		where Date(pw_end_date_utc) = (select max(Date(pw_end_date_utc)) from `wx-bq-poc.wx_lty_digital_attribution_dev.redx_loyalty_customer_value_model`)
		group by 1
	) b on a.crn = b.crn
	group by 1,2,3,4,5,6,7

	union all

	select case when segment_lifestage is null then 'Lifestage - Unknown' else concat('Lifestage - ', segment_lifestage) end as segment
		, banner
		, campaign_code
		, campaign_type
		, campaign_start_date
		, campaign_end_date
		, REGEXP_REPLACE(channel_event, r"_open|_imp|_clk|_view","") as medium
		, count(distinct a.crn) as medium_reach 
	from 
	(select * from `wx-bq-poc.digital_attribution_modelling.dacamp_prod_event_mw`) a
	left join
	(SELECT crn, max(macro_segment_curr) as segment_cvm, max(lifestage) as segment_lifestage
		FROM `wx-bq-poc.wx_lty_digital_attribution_dev.redx_loyalty_customer_value_model`
		where Date(pw_end_date_utc) = (select max(Date(pw_end_date_utc)) from `wx-bq-poc.wx_lty_digital_attribution_dev.redx_loyalty_customer_value_model`)
		group by 1
	) b on a.crn = b.crn
	group by 1,2,3,4,5,6,7
	
	union all

	select case when segment_marketable is null then 'Marketable - Unknown' else concat('Marketable - ', segment_marketable) end as segment
		, banner
		, campaign_code
		, campaign_type
		, campaign_start_date
		, campaign_end_date
		, REGEXP_REPLACE(channel_event, r"_open|_imp|_clk|_view","") as medium
		, count(distinct a.crn) as medium_reach 
	from 
	(select * from `wx-bq-poc.digital_attribution_modelling.dacamp_prod_event_mw`) a
	left join
	(SELECT crn, max(marketable) as segment_marketable
		FROM `wx-bq-poc.digital_attribution_modelling.marketable_crn`
		group by 1
	) b on a.crn = b.crn
	group by 1,2,3,4,5,6,7
)


SELECT a.segment, a.banner, a.campaign_code, a.campaign_type, a.campaign_start_date, a.campaign_end_date
	, REPLACE(REPLACE(a.channel, 'dx', 'wow_web'),'_AlwaysOn_','AlwaysOn_') as channel
	, REPLACE(REPLACE(a.medium, 'dx', 'wow_web'),'_AlwaysOn_','AlwaysOn_') as medium
	, REPLACE(REPLACE(a.event, 'dx', 'wow_web'),'_AlwaysOn_','AlwaysOn_') as event
	, c.event_volume, c.event_reach, a.event_converted_crn,  d.medium_reach, b.medium_converted_crn, a.attributed_redemption, a.attributed_total_sales, a.attributed_inc_sales
FROM
(SELECT * FROM base) a
left join
(SELECT * FROM base_medium) b on a.segment = b.segment and a.banner = b.banner and a.campaign_code = b.campaign_code and a.campaign_start_date = b.campaign_start_date and a.campaign_end_date = b.campaign_end_date and a.medium = b.medium
left join
(SELECT * FROM base_vol) c on c.segment = a.segment and c.banner = a.banner and c.campaign_code = a.campaign_code and c.campaign_start_date = a.campaign_start_date and c.campaign_end_date = a.campaign_end_date and c.event = a.event
left join
(SELECT * FROM base_vol_medium) d on a.segment = d.segment and a.banner = d.banner and a.campaign_code = d.campaign_code and a.campaign_start_date = d.campaign_start_date and a.campaign_end_date = d.campaign_end_date and a.medium = d.medium
);

select a.segment
	, a.banner
	, a.campaign_code
	, a.campaign_type
	, a.campaign_start_date
	, b.campaign_end_date_real as campaign_end_date
	, a.channel
	, a.medium
	, a.event
	, a.event_volume
	, a.event_reach
	, a.event_converted_crn
	, a.medium_reach
	, a.medium_converted_crn
	, a.attributed_total_sales
	, a.attributed_inc_sales
from 
(
SELECT * FROM `wx-bq-poc.digital_attribution_modelling.dacamp_prod_mc_final_agg`
where not (campaign_code <> 'CVM-1661' and channel like '%CVM-1661%')
) a 
inner join
(
	SELECT distinct banner, campaign_code, campaign_start_date, campaign_end_date, campaign_end_date_real
	from `digital_attribution_modelling.dacamp_prod_activation`
) b on a.banner = b.banner and a.campaign_code = b.campaign_code and a.campaign_start_date = b.campaign_start_date and a.campaign_end_date = b.campaign_end_date
;

