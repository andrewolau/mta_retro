with base0 as 
(
SELECT 'Total' as segment
		, *
	from `wx-bq-poc.digital_attribution_modelling.dacamp_prod_event_mw`

	union all

	select case when segment_cvm is null then 'CVM - Unknown' else concat('CVM - ', segment_cvm) end as segment
		, a.*
	from 
	(select * from `wx-bq-poc.digital_attribution_modelling.dacamp_prod_event_mw`) a
	left join
	(SELECT crn, max(macro_segment_curr) as segment_cvm, max(lifestage) as segment_lifestage
		FROM `wx-bq-poc.wx_lty_digital_attribution_dev.redx_loyalty_customer_value_model`
		where Date(pw_end_date_utc) = (select max(Date(pw_end_date_utc)) from `wx-bq-poc.wx_lty_digital_attribution_dev.redx_loyalty_customer_value_model`)
		group by 1
	) b on a.crn = b.crn

	union all

	select case when segment_lifestage is null then 'Lifestage - Unknown' else concat('Lifestage - ', segment_lifestage) end as segment
		, a.*
	from 
	(select * from `wx-bq-poc.digital_attribution_modelling.dacamp_prod_event_mw`) a
	left join
	(SELECT crn, max(macro_segment_curr) as segment_cvm, max(lifestage) as segment_lifestage
		FROM `wx-bq-poc.wx_lty_digital_attribution_dev.redx_loyalty_customer_value_model`
		where Date(pw_end_date_utc) = (select max(Date(pw_end_date_utc)) from `wx-bq-poc.wx_lty_digital_attribution_dev.redx_loyalty_customer_value_model`)
		group by 1
	) b on a.crn = b.crn
	
	union all

	select case when segment_marketable is null then 'Marketable - Unknown' else concat('Marketable - ', segment_marketable) end as segment
		, a.*
	from 
	(select * from `wx-bq-poc.digital_attribution_modelling.dacamp_prod_event_mw`) a
	left join
	(
		SELECT crn, max(marketable) as segment_marketable
		FROM `wx-bq-poc.digital_attribution_modelling.marketable_crn`
		group by 1
	) b on a.crn = b.crn
)

SELECT a.segment, a.banner, a.campaign_code, a.campaign_start_date, b.campaign_end_date_real as campaign_end_date, a.day_to_convert, a.volume 
FROM
(
SELECT segment, banner, campaign_code, campaign_start_date, campaign_end_date, TIMESTAMP_DIFF(conv_time, min_time_utc, day) as day_to_convert, count(distinct crn) as volume 
FROM 
(select *, min(time_utc) OVER (PARTITION BY segment, crn, banner, campaign_code, campaign_start_date, campaign_end_date) as min_time_utc
from base0 where conv_flag = 1)
group by 1,2,3,4,5,6
order by 1,2,3,4,5,6
) a
inner join
(
	SELECT distinct banner, campaign_code, campaign_start_date, campaign_end_date, campaign_end_date_real
	from `digital_attribution_modelling.dacamp_prod_activation`
) b on a.banner = b.banner and a.campaign_code = b.campaign_code and a.campaign_start_date = b.campaign_start_date and a.campaign_end_date = b.campaign_end_date
where day_to_convert >= 0;