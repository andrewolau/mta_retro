SELECT a.*, c.uc_crn
FROM
(
SELECT DATE_TRUNC(DATE_ADD(CAST('2021-05-10' as date), INTERVAL 9 DAY), week(Tuesday)) as load_date, fw_start_date, crn, 'supermarkets' as division, campaign_code, cast(campaign_start_date as date) as campaign_start_date, sum(inc_sales) as inc_sales
FROM `Attribution_Safari_GT_output.Safari_crn_level_game_theory_output_safari_full_promo_halo_post_current`
where fw_start_date = CAST('2021-05-10' as date) and pph <> 'promo' and inc_sales is not null and campaign_code is not null
group by 1,2,3,4,5,6

union all

SELECT DATE_TRUNC(DATE_ADD(CAST('2021-05-10' as date), INTERVAL 9 DAY), week(Tuesday)) as load_date, fw_start_date, crn, 'bigw' as division, campaign_code, cast(campaign_start_date as date) as campaign_start_date, sum(inc_sales) as inc_sales
FROM `Attribution_Safari_GT_output.Safari_crn_level_game_theory_output_safari_full_promo_halo_post_current_big_w`
where fw_start_date = CAST('2021-05-10' as date) and pph <> 'promo' and inc_sales is not null and campaign_code is not null
group by 1,2,3,4,5,6
) a		
	
left join
(
	SELECT distinct fw_start_date, crn, campaign_code, campaign_start_date, max(uc_crn) as uc_crn
	FROM `Attribution_Safari_Matching.safari_store`
	where fw_start_date = CAST('2021-05-10' as date)
	group by 1,2,3,4
) c on a.crn = c.crn and a.campaign_code = c.campaign_code and a.campaign_start_date = c.campaign_start_date