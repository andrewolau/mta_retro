select a.segment
	, a.banner
	, a.campaign_code
	, a.campaign_type
	, a.campaign_start_date
	, b.campaign_end_date_real as campaign_end_date
	, a.channel
	, a.medium
	, a.event
	, a.event_volume
	, a.event_reach
	, a.event_converted_crn
	, a.medium_reach
	, a.medium_converted_crn
	, a.attributed_total_sales
	, a.attributed_inc_sales
from 
(
select * 
from `wx-bq-poc.digital_attribution_modelling.dacamp_prod_mc_final_agg`
where channel like '%CVM-1661%' and campaign_code <> 'CVM-1661'
) a
inner join
(
	SELECT distinct banner, campaign_code, campaign_start_date, campaign_end_date, campaign_end_date_real
	from `digital_attribution_modelling.dacamp_prod_activation`
) b on a.banner = b.banner and a.campaign_code = b.campaign_code and a.campaign_start_date = b.campaign_start_date and a.campaign_end_date = b.campaign_end_date
;