with base as
(
	SELECT *
		, REGEXP_REPLACE(channel_event, r"_open|_imp|_clk|_view","") as Medium
		, concat('Total', '_', banner, '_', campaign_code, '_', cast(campaign_start_date as string), '_', cast(campaign_end_date as string)) as key
	FROM `wx-bq-poc.digital_attribution_modelling.dacamp_prod_event_mw` 
)

SELECT a.*, b.paths_medium, b.paths_conv_medium
FROM
(
	SELECT 'Total' as segment
		, banner
		, campaign_code
		, campaign_start_date
		, campaign_end_date
		, channel_event as Event
		, Medium
		, key
		, count(*) as volume
		, count(distinct crn) as paths
		, count(distinct case when conv_flag = 1 then crn else null end) as paths_conv
	FROM base
	group by 1,2,3,4,5,6,7,8
	order by 1,2,3,4,5,6,7,8
) a
inner join
(
	SELECT key, Medium
		, count(distinct crn) as paths_medium
		, count(distinct case when conv_flag = 1 then crn else null end) as paths_conv_medium
	from base
	group by 1,2
) b on a.key = b.key and a.Medium = b.medium
