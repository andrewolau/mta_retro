drop table if exists `wx-bq-poc.digital_attribution_modelling.dacamp_prod_mc_final_crn`;
create table `wx-bq-poc.digital_attribution_modelling.dacamp_prod_mc_final_crn` as
(
	SELECT *
	FROM `digital_attribution_modelling.dacamp_prod_mc_union_fb`

);

select
	DATE_TRUNC(DATE_ADD(CAST('2021-05-10' as date), INTERVAL 9 DAY), week(Tuesday)) as load_date
	, a.week as fw_start_date
	, a.banner as division
	, a.campaign_code
	, a.campaign_type
	, a.campaign_start_date
	, b.campaign_end_date_real as campaign_end_date
	, a.crn
	, c.uc_crn
	, 'N' as control
	, 'N' as tpg_control
	, a.segment_cvm as cvm
	, a.segment_lifestage as lifestage
	, a.segment_marketable as marketable
	, a.total_sales as tot_amt_incld_gst_promo
	, a.total_sales / (1.095) as tot_amt_excld_gst_promo
	, a.channel
	, a.medium
	, a.event
	, a.attributed_conversion
	, a.attributed_total_sales
	, a.attributed_inc_sales
from 
(
	SELECT * FROM `wx-bq-poc.digital_attribution_modelling.dacamp_prod_mc_final_crn`
) a
inner join
(
	SELECT distinct banner, campaign_code, campaign_start_date, campaign_end_date, campaign_end_date_real
	from `digital_attribution_modelling.dacamp_prod_activation`
) b on a.banner = b.banner and a.campaign_code = b.campaign_code and a.campaign_start_date = b.campaign_start_date and a.campaign_end_date = b.campaign_end_date
left join
(
	SELECT distinct fw_start_date, crn, campaign_code, campaign_start_date, max(uc_crn) as uc_crn
	FROM `Attribution_Safari_Matching.safari_store`
	where fw_start_date = CAST('2021-05-10' as date)
	group by 1,2,3,4
) c on a.crn = c.crn and a.campaign_code = c.campaign_code and a.campaign_start_date = c.campaign_start_date

union all

select 
	DATE_TRUNC(DATE_ADD(CAST('2021-05-10' as date), INTERVAL 9 DAY), week(Tuesday)) as load_date
	, fw_start_date
	, 'supermarkets' as division
	, cast(null as string) as campaign_code
	, cast(null as string) as campaign_type
	, cast(null as date) as campaign_start_date
	, cast(null as date) as campaign_end_date
	, uc_crn as crn
	, uc_crn as uc_crn
	, 'Y' as control
	, 'N' as tpg_control
	, segment_cvm as cvm
	, segment_lifestage as lifestage
	, segment_marketable as marketable
	, max(control_spend) as tot_amt_incld_gst_promo
	, max(control_spend) / (1.095) as tot_amt_excld_gst_promo
	, cast(null as string)
	, cast(null as string)
	, cast(null as string)
	, 0 as attributed_conversion
	, 0 as attributed_total_sales
	, 0 as attributed_inc_sales
from 
(
	SELECT distinct fw_start_date, uc_crn, control_spend
	FROM `Attribution_Safari_Matching.safari_store`
	where fw_start_date = CAST('2021-05-10' as date)
) a
left join
(
	SELECT crn, max(macro_segment_curr) as segment_cvm, max(lifestage) as segment_lifestage
	FROM `wx-bq-poc.wx_lty_digital_attribution_dev.redx_loyalty_customer_value_model`
	where Date(pw_end_date_utc) = (select max(Date(pw_end_date_utc)) from `wx-bq-poc.wx_lty_digital_attribution_dev.redx_loyalty_customer_value_model`)
	group by 1
) b on a.uc_crn = b.crn
left join
(
	SELECT crn, max(marketable) as segment_marketable
	FROM `wx-bq-poc.digital_attribution_modelling.marketable_crn`
	group by 1
) c on a.uc_crn = c.crn
group by 1,2,3,4,5,6,7,8,9,10,11,12,13,14

-- union all

-- select 
-- 	DATE_TRUNC(current_date(), week(Tuesday)) as load_date
-- 	, fw_start_date
-- 	, 'supermarkets' as division
-- 	, campaign_code as campaign_code
-- 	, cast(null as string) as campaign_type
-- 	, campaign_start_date as campaign_start_date
-- 	, cast(null as date) as campaign_end_date
-- 	, a.crn as crn
-- 	, tpg_crn as uc_crn
-- 	, 'Y' as control
-- 	, 'Y' as tpg_control
-- 	, segment_cvm as cvm
-- 	, segment_lifestage as lifestage
-- 	, segment_marketable as marketable
-- 	, max(control_spend) as tot_amt_incld_gst_promo
-- 	, max(control_spend) / (1.095) as tot_amt_excld_gst_promo
-- 	, cast(null as string)
-- 	, cast(null as string)
-- 	, cast(null as string)
-- 	, 0 as attributed_conversion
-- 	, 0 as attributed_total_sales
-- 	, 0 as attributed_inc_sales
-- from 
-- (
-- 	SELECT distinct fw_start_date, crn, tpg_crn, campaign_start_date, campaign_code, control_spend
-- 	FROM `Attribution_Safari_Matching.safari_store`
-- 	where fw_start_date = CAST('2021-05-10' as date)
-- ) a
-- left join
-- (
-- 	SELECT crn, max(macro_segment_curr) as segment_cvm, max(lifestage) as segment_lifestage
-- 	FROM `wx-bq-poc.wx_lty_digital_attribution_dev.redx_loyalty_customer_value_model`
-- 	where Date(pw_end_date_utc) = (select max(Date(pw_end_date_utc)) from `wx-bq-poc.wx_lty_digital_attribution_dev.redx_loyalty_customer_value_model`)
-- 	group by 1
-- ) b on a.crn = b.crn
-- left join
-- (
-- 	SELECT crn, max(marketable) as segment_marketable
-- 	FROM `wx-bq-poc.digital_attribution_modelling.marketable_crn`
-- 	group by 1
-- ) c on a.crn = c.crn
-- group by 1,2,3,4,5,6,7,8,9,10,11,12,13,14

/* union all

select 
	DATE_TRUNC(current_date(), week(Tuesday)) as load_date
	, financial_week_start as fw_start_date
	, 'supermarkets' as division
	, 'SEM-SHOP' as campaign_code
	, cast(null as string) as campaign_type
	, cast(null as date) as campaign_start_date
	, cast(null as date) as campaign_end_date
	, cast(null as string)  as crn
	, cast(null as string)  as uc_crn
	, cast(null as string)  as control
	, cast(null as string)  as cvm
	, cast(null as string)  as lifestage
	, cast(null as string)  as marketable
	, 0 as tot_amt_incld_gst_promo
	, 0 as tot_amt_excld_gst_promo
	, cast(null as string)
	, cast(null as string)
	, cast(null as string)
	, 0 as attributed_conversion
	, 0 as attributed_total_sales
	, total_inc_sales as attributed_inc_sales
	
from `digital_attribution_modelling.sem_incremental_sales_new`
where financial_week_start >= '2020-11-01'

union all

select 
	DATE_TRUNC(current_date(), week(Tuesday)) as load_date
	, financial_month_start as fw_start_date
	, 'supermarkets' as division
	, 'DXM-SHOP' as campaign_code
	, cast(null as string) as campaign_type
	, cast(null as date) as campaign_start_date
	, cast(null as date) as campaign_end_date
	, cast(null as string)  as crn
	, cast(null as string)  as uc_crn
	, cast(null as string)  as control
	, cast(null as string)  as cvm
	, cast(null as string)  as lifestage
	, cast(null as string)  as marketable
	, 0 as tot_amt_incld_gst_promo
	, 0 as tot_amt_excld_gst_promo
	, cast(null as string)
	, cast(null as string)
	, cast(null as string)
	, 0 as attributed_conversion
	, 0 as attributed_total_sales
	, total_dx_sales as attributed_inc_sales
	
from `digital_attribution_modelling.dx_incremental_sales_new`
where financial_month_start >= '2020-11-01'


union all

select 
	DATE_TRUNC(current_date(), week(Tuesday)) as load_date
	, financial_month_start as fw_start_date
	, 'supermarkets' as division
	, 'DXM-SHOP-ROBIS' as campaign_code
	, cast(null as string) as campaign_type
	, cast(null as date) as campaign_start_date
	, cast(null as date) as campaign_end_date
	, cast(null as string)  as crn
	, cast(null as string)  as uc_crn
	, cast(null as string)  as control
	, cast(null as string)  as cvm
	, cast(null as string)  as lifestage
	, cast(null as string)  as marketable
	, 0 as tot_amt_incld_gst_promo
	, 0 as tot_amt_excld_gst_promo
	, cast(null as string)
	, cast(null as string)
	, cast(null as string)
	, 0 as attributed_conversion
	, 0 as attributed_total_sales
	, robis_net_inc_sales as attributed_inc_sales
	
from `digital_attribution_modelling.dx_incremental_sales_new`
where financial_month_start >= '2020-11-01' */

/* union all

select 
	DATE_TRUNC(current_date(), week(Tuesday)) as load_date
	, '2020-10-05' as fw_start_date
	, 'supermarkets' as division
	, 'DXM-SHOP' as campaign_code
	, cast(null as string) as campaign_type
	, cast(null as date) as campaign_start_date
	, cast(null as date) as campaign_end_date
	, cast(null as string)  as crn
	, cast(null as string)  as uc_crn
	, cast(null as string)  as control
	, cast(null as string)  as cvm
	, cast(null as string)  as lifestage
	, cast(null as string)  as marketable
	, 0 as tot_amt_incld_gst_promo
	, 0 as tot_amt_excld_gst_promo
	, cast(null as string)
	, cast(null as string)
	, cast(null as string)
	, 0 as attributed_conversion
	, 0 as attributed_total_sales
	, 6.40782775520811E7 as attributed_inc_sales
	 */

/* union all

select 
	DATE_TRUNC(current_date(), week(Tuesday)) as load_date
	, '2020-10-05' as fw_start_date
	, 'supermarkets' as division
	, 'DXM-SHOP-ROBIS-ONL' as campaign_code
	, cast(null as string) as campaign_type
	, cast(null as date) as campaign_start_date
	, cast(null as date) as campaign_end_date
	, cast(null as string)  as crn
	, cast(null as string)  as uc_crn
	, cast(null as string)  as control
	, cast(null as string)  as cvm
	, cast(null as string)  as lifestage
	, cast(null as string)  as marketable
	, 0 as tot_amt_incld_gst_promo
	, 0 as tot_amt_excld_gst_promo
	, cast(null as string)
	, cast(null as string)
	, cast(null as string)
	, 0 as attributed_conversion
	, 0 as attributed_total_sales
	, 1158462.03 as attributed_inc_sales	
		
union all


select 
	DATE_TRUNC(current_date(), week(Tuesday)) as load_date
	, '2020-10-05' as fw_start_date
	, 'supermarkets' as division
	, 'DXM-SHOP-ROBIS-WAPP' as campaign_code
	, cast(null as string) as campaign_type
	, cast(null as date) as campaign_start_date
	, cast(null as date) as campaign_end_date
	, cast(null as string)  as crn
	, cast(null as string)  as uc_crn
	, cast(null as string)  as control
	, cast(null as string)  as cvm
	, cast(null as string)  as lifestage
	, cast(null as string)  as marketable
	, 0 as tot_amt_incld_gst_promo
	, 0 as tot_amt_excld_gst_promo
	, cast(null as string)
	, cast(null as string)
	, cast(null as string)
	, 0 as attributed_conversion
	, 0 as attributed_total_sales
	, 719282.54 as attributed_inc_sales	
 */


/* union all

select 
	DATE_TRUNC(current_date(), week(Tuesday)) as load_date
	, Date('2020-10-05') as fw_start_date
	, 'bigw' as division
	, a.campaign_code as campaign_code
	, 'BTL' as campaign_type
	, cast(a.campaign_start_date as date) as campaign_start_date
	, date_add(cast(a.campaign_start_date as date), interval 6 day) as campaign_end_date
	, a.crn as crn
	, uc_crn as uc_crn
	, 'N' as control
	, segment_cvm as cvm
	, segment_lifestage as lifestage
	, segment_marketable as marketable
	, 0 as tot_amt_incld_gst_promo
	, 0 as tot_amt_excld_gst_promo
	, 'CNA-0003_SBW_email'
	, 'CNA-0003_SBW_email'
	, 'CNA-0003_SBW_email_open'
	, 1 as attributed_conversion
	, inc_sales as attributed_total_sales
	, inc_sales as attributed_inc_sales 
	from 
	(
		SELECT *
		FROM `Attribution_Safari_GT_output.Safari_crn_level_game_theory_output_safari_full_promo_halo_post_current`
		where fw_start_date = '2020-10-05' and pph = 'promo' and campaign_code = 'CNA-0003_SBW' and campaign_start_date = '2020-10-05'
	) a 
	left join
	(
		SELECT distinct fw_start_date, crn, campaign_code, campaign_start_date, max(uc_crn) as uc_crn
		FROM `Attribution_Safari_Matching.safari_store`
		where fw_start_date = '2020-10-05'
		group by 1,2,3,4
	) b on a.crn = b.crn and a.campaign_code = b.campaign_code and cast(a.campaign_start_date as date) = b.campaign_start_date
	left join
	(
		SELECT crn, max(macro_segment_curr) as segment_cvm, max(lifestage) as segment_lifestage
		FROM `wx-bq-poc.wx_lty_digital_attribution_dev.redx_loyalty_customer_value_model`
		where Date(pw_end_date_utc) = (select max(Date(pw_end_date_utc)) from `wx-bq-poc.wx_lty_digital_attribution_dev.redx_loyalty_customer_value_model`)
		group by 1
	) c on a.crn = c.crn
	left join
	(
		SELECT crn, max(marketable) as segment_marketable
		FROM `wx-bq-poc.digital_attribution_modelling.marketable_crn`
		group by 1
	) d on a.crn = d.crn
	
	 */
	
/* union all 

select 
	DATE_TRUNC(current_date(), week(Tuesday)) as load_date
	, DATE_ADD(fw_start_date, INTERVAL -7 DAY) as fw_start_date
	, 'supermarkets' as division
	, 'CVM-1661E' as campaign_code
	, 'TTL' as campaign_type
	, campaign_start_date
	, DATE_ADD(campaign_start_date, INTERVAL 6 day) AS campaign_end_date
	, cast(null as string)  as crn
	, cast(null as string)  as uc_crn
	, cast(null as string)  as control
	, cast(null as string)  as cvm
	, cast(null as string)  as lifestage
	, cast(null as string)  as marketable
	, 0 as tot_amt_incld_gst_promo
	, 0 as tot_amt_excld_gst_promo
	, cast(null as string)
	, cast(null as string)
	, cast(null as string)
	, 0 as attributed_conversion
	, 0 as attributed_total_sales
	, ss as attributed_inc_sales
	
from `digital_attribution_modelling.sy_rw_app_ws`


union all

SELECT 
	DATE_TRUNC(current_date(), week(Tuesday)) as load_date
	, fw_start_date
	, 'supermarkets' as division
	, 'CVM-1661' as campaign_code
	, 'TTL' as campaign_type
	, cast(campaign_week as date) as campaign_start_date
	, date_add(cast(campaign_week as date), interval 6 day) as campaign_end_date
	, cast(null as string)  as crn
	, cast(null as string)  as uc_crn
	, cast(null as string)  as control
	, cast(null as string)  as cvm
	, cast(null as string)  as lifestage
	, cast(null as string)  as marketable
	, 0 as tot_amt_incld_gst_promo
	, 0 as tot_amt_excld_gst_promo
	, 'CVM-1661_FB'
	, 'CVM-1661_FB'
	, 'CVM-1661_FB'
	, 0 as attributed_conversion
	, 0 as attributed_total_sales
	, FB_exclusive_amount as attributed_inc_sales
 FROM `wx-bq-poc.digital_attribution_modelling.dacamp_fb_exclusive_monthly`
 where date(fw_start_date) >= Date('2020-09-07')
 */

;