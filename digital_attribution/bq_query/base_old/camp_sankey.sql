
DROP TABLE IF EXISTS `wx-bq-poc.digital_attribution_modelling.dacamp_prod_sankey`;
CREATE table IF NOT EXISTS `wx-bq-poc.digital_attribution_modelling.dacamp_prod_sankey` as 
(
	with base as
	(
		SELECT banner
			, campaign_code
			, campaign_type
			, campaign_start_date
			, campaign_end_date
			, crn
			, REPLACE(case when channel_event like '%AlwaysOn%' then SUBSTR(channel_event, 11, CHAR_LENGTH(channel_event)) else SUBSTR(channel_event, 10, CHAR_LENGTH(channel_event)) end, 'dx', 'wow_web') as channel_event
			, conv_flag
			, spend
			, row_number() over (partition by banner, campaign_code, campaign_start_date, campaign_end_date, crn order by time_utc desc) as pos
		FROM `wx-bq-poc.digital_attribution_modelling.dacamp_prod_event_mw`
	)

	SELECT a.key
		, a.banner
		, a.campaign_code
		, a.campaign_type
		, a.campaign_start_date
		, b.campaign_end_date_real as campaign_end_date
		, a.crn
		, a.conv_flag
		, a.spend,
		a.pos_1, 
		a.pos_2, 
		a.pos_3, 
		a.pos_4, 
		a.pos_5, 
		a.pos_6, 
		a.pos_7, 
		a.pos_8, 
		a.pos_9, 
		a.pos_10,
		a.pos_11, 
		a.pos_12, 
		a.pos_13, 
		a.pos_14, 
		a.pos_15, 
		a.pos_16, 
		a.pos_17, 
		a.pos_18, 
		a.pos_19, 
		a.pos_20
	FROM
	(
	SELECT concat('Total', '_', banner, '_', campaign_code, '_', cast(campaign_start_date as string), '_', cast(campaign_end_date as string)) as key 
			, banner
			, campaign_code
			, campaign_type
			, campaign_start_date
			, campaign_end_date
			, crn
			, conv_flag
			, spend,
		max(pos_1) as pos_1, 
		max(pos_2) as pos_2, 
		max(pos_3) as pos_3, 
		max(pos_4) as pos_4, 
		max(pos_5) as pos_5, 
		max(pos_6) as pos_6, 
		max(pos_7) as pos_7, 
		max(pos_8) as pos_8, 
		max(pos_9) as pos_9, 
		max(pos_10) as pos_10,
		max(pos_11) as pos_11, 
		max(pos_12) as pos_12, 
		max(pos_13) as pos_13, 
		max(pos_14) as pos_14, 
		max(pos_15) as pos_15, 
		max(pos_16) as pos_16, 
		max(pos_17) as pos_17, 
		max(pos_18) as pos_18, 
		max(pos_19) as pos_19, 
		max(pos_20) as pos_20
	FROM
	(
		SELECT banner
			, campaign_code
			, campaign_type
			, campaign_start_date
			, campaign_end_date
			, crn,
			case when conv_flag is null then 0 else 1 end as conv_flag,
			case when spend is null then 0 else spend end as spend,
			case when pos = 1 then channel_event else null end as pos_1,
			case when pos = 2 then channel_event else null end as pos_2,
			case when pos = 3 then channel_event else null end as pos_3,
			case when pos = 4 then channel_event else null end as pos_4,
			case when pos = 5 then channel_event else null end as pos_5,
			case when pos = 6 then channel_event else null end as pos_6,
			case when pos = 7 then channel_event else null end as pos_7,
			case when pos = 8 then channel_event else null end as pos_8,
			case when pos = 9 then channel_event else null end as pos_9,
			case when pos = 10 then channel_event else null end as pos_10,
			case when pos = 11 then channel_event else null end as pos_11,
			case when pos = 12 then channel_event else null end as pos_12,
			case when pos = 13 then channel_event else null end as pos_13,
			case when pos = 14 then channel_event else null end as pos_14,
			case when pos = 15 then channel_event else null end as pos_15,
			case when pos = 16 then channel_event else null end as pos_16,
			case when pos = 17 then channel_event else null end as pos_17,
			case when pos = 18 then channel_event else null end as pos_18,
			case when pos = 19 then channel_event else null end as pos_19,
			case when pos = 20 then channel_event else null end as pos_20
		FROM base
	)
	group by 1,2,3,4,5,6,7,8,9
	) a
	inner join
	(
		SELECT distinct banner, campaign_code, campaign_start_date, campaign_end_date, campaign_end_date_real
		from `digital_attribution_modelling.dacamp_prod_activation`
	) b on a.banner = b.banner and a.campaign_code = b.campaign_code and a.campaign_start_date = b.campaign_start_date and a.campaign_end_date = b.campaign_end_date
);