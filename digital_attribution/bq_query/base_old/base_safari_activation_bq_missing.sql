
---- safari activation ----
drop table if exists `digital_attribution_modelling.safarievents`;
create table `digital_attribution_modelling.safarievents` as
(
	select 
		crn
		,case when division_name = 'SUPERS'  then 'supermarkets' 
			when division_name = 'BWS' then 'bws'
			when division_name = 'BIGW'  then 'bigw'
			when division_name = 'FUELCO' then 'fuelco'
			when division_name = 'CALTEX'  then 'caltex'  
			else 'other' end as banner
		, campaign_code
		, False as campaign_ttl
		,campaign_tpg_amt
		,campaign_activatable
		,campaign_start_date
		,DATE_ADD(campaign_start_date, interval campaign_duration_weeks*7-1 day) as campaign_end_date
		,min(case when campaign_activatable = True then activation_datetime else purchase_datetime end) as time_utc
		,min(case when campaign_activatable = True then Date(activation_datetime,'Australia/Sydney') else Date(purchase_datetime, 'Australia/Sydney') end) as date
		,0 as sales_amt_tpg
		,0 as sales_amt
	from 
	(
		SELECT a.*, b.campaign_activatable, c.campaign_tpg_amt
		FROM
		(
			select crn
			, division_name
			, campaign_code
			, CAST(campaign_ttl as boolean) as campaign_ttl
			, CAST(campaign_start_date as date) as campaign_start_date
			, CAST(campaign_duration_weeks as int64) as campaign_duration_weeks
			, TIMESTAMP(SUBSTR(CAST(activation_datetime as string),1,19),'Australia/Sydney') as activation_datetime
			, TIMESTAMP(SUBSTR(CAST(purchase_datetime as string),1,19), 'Australia/Sydney') as purchase_datetime
			, CAST(sales_amt as numeric) as sales_amt
			, CAST(tpg_amt as numeric) as tpg_amt
			from `wx-bq-poc.loyalty_bi_analytics.safarievents_final_missing`
		) a
		
		inner join 
		(
			SELECT division_name, campaign_code, CAST(campaign_start_date as date) as campaign_start_date
			, CAST(campaign_duration_weeks as int64) as campaign_duration_weeks, case when min(activation_datetime) is not null then True else False end as campaign_activatable
			from `wx-bq-poc.loyalty_bi_analytics.safarievents_final_missing`
			group by 1,2,3,4
		) b on a.division_name = b.division_name and a.campaign_code = b.campaign_code and a.campaign_start_date = b.campaign_start_date and a.campaign_duration_weeks = b.campaign_duration_weeks
		
		inner join
		(
			SELECT division_name, campaign_code, CAST(campaign_start_date as date) as campaign_start_date
			, CAST(campaign_duration_weeks as int64) as campaign_duration_weeks, case when sum(cast(tpg_amt as numeric)) > 0 then True else False end as campaign_tpg_amt
			from `wx-bq-poc.loyalty_bi_analytics.safarievents_final_missing`
			group by 1,2,3,4
		) c on a.division_name = c.division_name and a.campaign_code = c.campaign_code and a.campaign_start_date = c.campaign_start_date and a.campaign_duration_weeks = c.campaign_duration_weeks
	)
	group by 1,2,3,4,5,6,7,8
);

	
---- marketable ----
drop table if exists `digital_attribution_modelling.marketable_crn`;

create table `digital_attribution_modelling.marketable_crn` as
(
	select crn, edm_marketable_flag as marketable
	from `loyalty_bi_analytics.saf_edm_marketable_snapshot`
	where fw_start_date = (select max(fw_start_date) from `loyalty_bi_analytics.saf_edm_marketable_snapshot`)
);
	
	