drop table if exists `digital_attribution_modelling.sy_sem_inc_sales_base`;
create table `digital_attribution_modelling.sy_sem_inc_sales_base` as
(
SELECT distinct
			crn
			, time_utc
			, date
			, case 
				when regexp_contains(paid_search_campaign, r'CAT-|CNA-|CVM-|ENG-|LCP-|NCP-|OSP-|FSC-|ONL-') then 'supermarkets'
				when regexp_contains(paid_search_campaign, r'BCV-|BLC-|BCT-|BSP-|BOL-|BEV-|NBA-') then 'bws'
				when regexp_contains(paid_search_campaign, r'WCV-|WLC-|WCT-|WSP-|WOL-|WEV-|WEN-') then 'bigw'
				when regexp_contains(paid_search_campaign, r'CCV-|CEN-|CCD-') then 'caltex'
				when regexp_contains(paid_search_campaign, r'FCO-') then 'fuelco'
				else 'supermarkets' end as banner
			, 'sem' as channel
			, case 
				when lower(paid_search_campaign) like '%brand%' then 'brand_clk'
				when regexp_contains(paid_search_campaign, r'(?i)generic|content|competitor') then 'generic_clk'
				when paid_search_campaign is null then 'shopping_clk'
				else 'other_clk' end as event_name
			, CASE WHEN ARRAY_LENGTH(REGEXP_EXTRACT_ALL(paid_search_campaign, r"[A-Z]+-\d\d\d\d")) >0 
					THEN REGEXP_EXTRACT_ALL(paid_search_campaign, r"[A-Z]+-\d\d\d\d")[OFFSET(0)] 
					else null end as campaign_code
			, cast(null as date) as campaign_start_date
			, cast(null as date) as campaign_end_date
			, CONCAT('paid_search_campaign=',paid_search_campaign,';paid_search_keyword=',paid_search_keyword) as attributes
		FROM
		(
			SELECT *
					, REPLACE(json_extract(attributes, "$.paid_search_campaign[0]"),'\"','') as paid_search_campaign
					, REPLACE(json_extract(attributes, "$.paid_search_keyword[0]"),'\"','') as paid_search_keyword
			FROM `wx-bq-poc.wx_lty_digital_attribution_dev.eventstore_events` 
			where channel = 'sem' and Date(time_utc) between Date('2020-09-07') and Date('2020-10-04')
		)
);


drop table if exists `digital_attribution_modelling.sy_sem_inc_sales_exposure`;
create table `digital_attribution_modelling.sy_sem_inc_sales_exposure` as
(
	SELECT distinct date, crn from `digital_attribution_modelling.sy_sem_inc_sales_base`
);



drop table if exists `digital_attribution_modelling.sy_match_google_crn_test`;
create table `digital_attribution_modelling.sy_match_google_crn_test` as (
with crn_base as (
select
    crn_enc,
    user_id,
    event_time,
    rank() over (partition by user_id order by event_time) as date_rank
  from (
  select
    user_id,
    split(split(other_data, 'u1=')[offset(1)], ';')[offset(0)] as crn_enc,
    event_time
  from `wx-bq-poc.wx_lty_digital_attribution_dev.googlex_activity`
  where
  other_data like '%u1=%'
  and user_id <> '0'  -- over 3m records with user_id = '0'
  ) t1
  where length(crn_enc) = 64
)
select
  t3.crn,
  t2.user_id,
  t2.crn_enc,
  case when date_rank = 1 then 1 else 0 end as initial_flag,
  effective_ts,
  timestamp_micros(expiry_ts) as expiry_ts,
  extract(date from effective_ts) as effective_date,
  extract(date from timestamp_micros(expiry_ts)) as expiry_date
from (
  select
    user_id,
    crn_enc,
    date_rank,
    event_time as effective_ts,
    lead(UNIX_MICROS(event_time)) over (
      partition by user_id order by date_rank
    ) - 1 as expiry_ts  -- subtract 1 microsecond
  from (
    select *,
      lag(crn_enc) over (partition by user_id order by date_rank) as lag_crn_enc
    from crn_base
    ) t1
  where
    lag_crn_enc <> crn_enc
    or date_rank = 1
    or lag_crn_enc is null
) t2
inner join
  `wx-bq-poc.wx_lty_digital_attribution_dev.redx_loyalty_customer_encrypted_detail` t3
  on t2.crn_enc = t3.crn_enc
  and t3.target_sys_name = 'ET'  -- exact target matches only
);



drop table if exists `digital_attribution_modelling.sy_sem_productGroup`;
create table `digital_attribution_modelling.sy_sem_productGroup` as
(
SELECT productGroupId, productGroup
        , REPLACE(concat(productCategoryLevel1,productCategoryLevel2,productCategoryLevel3,productCategoryLevel4),'Everything else','') as pc
        , REPLACE(concat(productTypeLevel1,productTypeLevel2,productTypeLevel3), 'Everything else','') as pt
        , REPLACE(productId, 'Everything else','') as productId
    from
    (
		select distinct SUBSTR(productGroupId,7,17) as productGroupId, productGroup
				, CASE WHEN REGEXP_CONTAINS(productGroup, 'CATEGORY_LEVEL_1') then SPLIT(SPLIT(productGroup,'CATEGORY_LEVEL_1 = \"')[OFFSET(1)], '\"')[OFFSET(0)] else '' end as productCategoryLevel1
				, CASE WHEN REGEXP_CONTAINS(productGroup, 'CATEGORY_LEVEL_2') then SPLIT(SPLIT(productGroup,'CATEGORY_LEVEL_2 = \"')[OFFSET(1)], '\"')[OFFSET(0)] else '' end as productCategoryLevel2
				, CASE WHEN REGEXP_CONTAINS(productGroup, 'CATEGORY_LEVEL_3') then SPLIT(SPLIT(productGroup,'CATEGORY_LEVEL_3 = \"')[OFFSET(1)], '\"')[OFFSET(0)] else '' end as productCategoryLevel3
				, CASE WHEN REGEXP_CONTAINS(productGroup, 'CATEGORY_LEVEL_4') then SPLIT(SPLIT(productGroup,'CATEGORY_LEVEL_4 = \"')[OFFSET(1)], '\"')[OFFSET(0)] else '' end as productCategoryLevel4
				, CASE WHEN REGEXP_CONTAINS(productGroup, 'PRODUCT_TYPE_LEVEL_1') then SPLIT(SPLIT(productGroup,'PRODUCT_TYPE_LEVEL_1 = \"')[OFFSET(1)], '\"')[OFFSET(0)] else '' end as productTypeLevel1
				, CASE WHEN REGEXP_CONTAINS(productGroup, 'PRODUCT_TYPE_LEVEL_2') then SPLIT(SPLIT(productGroup,'PRODUCT_TYPE_LEVEL_2 = \"')[OFFSET(1)], '\"')[OFFSET(0)] else '' end as productTypeLevel2
				, CASE WHEN REGEXP_CONTAINS(productGroup, 'PRODUCT_TYPE_LEVEL_3') then SPLIT(SPLIT(productGroup,'PRODUCT_TYPE_LEVEL_3 = \"')[OFFSET(1)], '\"')[OFFSET(0)] else '' end as productTypeLevel3
				, CASE WHEN REGEXP_CONTAINS(productGroup, 'PRODUCT_ID') then SPLIT(SPLIT(productGroup,'PRODUCT_ID = \"')[OFFSET(1)], '\"')[OFFSET(0)] else '' end as productId
		from `wr-weeklysaver-poc-2018.sa360_transfer.p_ProductGroup_21700000001518015`
	 
    )
);


drop table if exists `digital_attribution_modelling.sy_sem_productAdvertised`;
create table `digital_attribution_modelling.sy_sem_productAdvertised` as
(
select (REPLACE(concat(productCategoryLevel1,productCategoryLevel2,productCategoryLevel3,productCategoryLevel4),'Everything else','')) as pc
        , (REPLACE(concat(productTypeLevel1,productTypeLevel2,productTypeLevel3), 'Everything else','')) as pt
        , productId
		from 
    (
      SELECT distinct 
      CASE WHEN productCategoryLevel1 is not null then productCategoryLevel1 else '' end as productCategoryLevel1
				, CASE WHEN productCategoryLevel2 is not null then productCategoryLevel2 else '' end as productCategoryLevel2
        , CASE WHEN productCategoryLevel3 is not null then productCategoryLevel3 else '' end as productCategoryLevel3
        , CASE WHEN productCategoryLevel4 is not null then productCategoryLevel4 else '' end as productCategoryLevel4
        , CASE WHEN productTypeLevel1 is not null then productTypeLevel1 else '' end as productTypeLevel1
        , CASE WHEN productTypeLevel2 is not null then productTypeLevel2 else '' end as productTypeLevel2
        , CASE WHEN productTypeLevel3 is not null then productTypeLevel3 else '' end as productTypeLevel3
				, CASE WHEN productId is not null then productId else '' end as productId
      from `wr-weeklysaver-poc-2018.sa360_transfer.p_ProductAdvertised_21700000001518015`
    )
);



drop table if exists `digital_attribution_modelling.sy_sem_productGroup_all`;
create table `digital_attribution_modelling.sy_sem_productGroup_all` as
select distinct productGroupId, productGroup, productId
from `digital_attribution_modelling.sy_sem_productGroup` where productId <> ''

union distinct

select distinct a.productGroupId, a.productGroup, b.productId
from
(
  SELECT * FROM `digital_attribution_modelling.sy_sem_productGroup` where pc <> '' and productId = ''
) a 
cross join
(
  SELECT * FROM `digital_attribution_modelling.sy_sem_productAdvertised` where pc <> ''
) b 
where REGEXP_CONTAINS(b.pc, a.pc)

union distinct

select distinct a.productGroupId, a.productGroup, b.productId
from
(
  SELECT * FROM `digital_attribution_modelling.sy_sem_productGroup` where pt <> '' and productId = ''
) a 
cross join
(
  SELECT * FROM `digital_attribution_modelling.sy_sem_productAdvertised` where pt <> ''
) b 
where REGEXP_CONTAINS(b.pt, a.pt);




drop table if exists `digital_attribution_modelling.sy_sem_prod_clk`;
create table `digital_attribution_modelling.sy_sem_prod_clk` as
(
	SELECT b.crn, a.user_id, event_time, productGroupId, productGroup, c.productId as prod_nbr, am.article_name
	from
	(
		SELECT user_id, event_time, segment_value_1 FROM `wx-bq-poc.wx_lty_digital_attribution_dev.googlex_click`
		where user_id <> '0' and segment_value_1 is not null and Date(event_time) between (select min(Date(time_utc)) from `digital_attribution_modelling.sy_sem_inc_sales_base`) and (select max(Date(time_utc)) from `digital_attribution_modelling.sy_sem_inc_sales_base`)
	) a

	inner join 
	(
		select distinct * 
		from `digital_attribution_modelling.sy_match_google_crn_test`
	) b
	on a.user_id = b.user_id
		and (
			(
			a.event_time >= b.effective_ts
			and (
			  b.expiry_ts is null
			  or
			  a.event_time < b.expiry_ts
			  )
			)
			or
			(
			a.event_time < b.effective_ts
			and b.initial_flag = 1
			)
		)

	inner join
	(
		select distinct productGroupId, productGroup, productId
		from `digital_attribution_modelling.sy_sem_productGroup_all`
	) c on a.segment_value_1 = c.productGroupId
	
  
	left join
	(
		select SPLIT(prod_nbr,'-')[OFFSET(0)] as prod_nbr, max(article_name) as article_name
		from 
		(
			select *,rank() over (partition by prod_nbr, division_nbr order by last_update_date desc) as date_rank 
			from `wx-bq-poc.wx_lty_digital_attribution_dev.redx_loyalty_article_master`
		)
		where date_rank=1
	group by 1
	) am on c.productId=am.prod_nbr
);



drop table if exists `digital_attribution_modelling.sy_sem_prod_spend`;
create table `digital_attribution_modelling.sy_sem_prod_spend` as
(
	select a.crn, ass.start_txn_time, SPLIT(ass.prod_nbr,'-')[OFFSET(0)] as prod_nbr, ass.checkout_type, sum(tot_amt_incld_gst-ass.tot_wow_dollar_incld_gst) as spend
	from
	(
		select distinct crn
		from `digital_attribution_modelling.sy_sem_prod_clk`
	) a 
	
	inner join 
	(
		select lylty_card_nbr,crn 
		from 
		(
			select *,rank() over (partition by crn order by last_update_date desc) as date_rank 
			from `wx-bq-poc.wx_lty_digital_attribution_dev.redx_loyalty_lylty_card_detail` 
			where lylty_card_status=1 and crn is not NULL and crn <> '0'
		)
		where date_rank=1
	) as lcd on a.crn = lcd.crn
	
	inner join 
	(
		select lylty_card_nbr, start_txn_time, prod_nbr, tot_amt_incld_gst, tot_wow_dollar_incld_gst, division_nbr
				, case when checkout_nbr = 100 then 'online' else 'instore' end as checkout_type
		from wx_lty_digital_attribution_dev.redx_loyalty_article_sales_summary 
		where division_nbr in (1005,1030) and void_flag <> 'Y' and Date(start_txn_time) >= Date('2020-08-01')
	) ass on ass.lylty_card_nbr=lcd.lylty_card_nbr
	
	inner join
	(
		select prod_nbr, division_nbr
		from 
		(
			select *,rank() over (partition by prod_nbr, division_nbr order by last_update_date desc) as date_rank 
			from `wx-bq-poc.wx_lty_digital_attribution_dev.redx_loyalty_article_master`
		)
		where date_rank=1
	) am on ass.prod_nbr=am.prod_nbr and ass.division_nbr=am.division_nbr
	
	group by 1,2,3,4
);





drop table if exists `digital_attribution_modelling.sy_sem_inc_sales_narrow`;
create table `digital_attribution_modelling.sy_sem_inc_sales_narrow` as
(
	SELECT distinct date, a.crn
	FROM
	(
		(
			SELECT distinct crn, time_utc, date
			FROM `digital_attribution_modelling.sy_sem_inc_sales_base`
			where event_name <> 'shopping_clk'
		) a
		inner join
		(
			select crn, start_txn_time, sum(tot_amt_incld_gst-ass.tot_wow_dollar_incld_gst) as spend
			from 
			(
				select lylty_card_nbr, start_txn_time, prod_nbr, tot_amt_incld_gst, tot_wow_dollar_incld_gst, division_nbr, checkout_nbr
				from wx_lty_digital_attribution_dev.redx_loyalty_article_sales_summary 
				where division_nbr in (1005,1030) and void_flag <> 'Y' and Date(start_txn_time) >= Date('2020-08-01')
			) ass
			inner join
			(
				select prod_nbr, division_nbr 
				from 
				(
					select *,rank() over (partition by prod_nbr, division_nbr order by last_update_date desc) as date_rank 
					from `wx-bq-poc.wx_lty_digital_attribution_dev.redx_loyalty_article_master`
				)
				where date_rank=1
			) am on ass.prod_nbr=am.prod_nbr and ass.division_nbr=am.division_nbr
			inner join 
			(
				select lylty_card_nbr,crn 
				from 
				(
					select *,rank() over (partition by crn order by last_update_date desc) as date_rank 
					from `wx-bq-poc.wx_lty_digital_attribution_dev.redx_loyalty_lylty_card_detail` 
					where lylty_card_status=1 and crn is not NULL and crn <> '0'
				)
				where date_rank=1
			) as lcd on ass.lylty_card_nbr=lcd.lylty_card_nbr
			group by 1,2
		) b on a.crn = b.crn 
				and timestamp_diff(b.start_txn_time, a.time_utc, SECOND) > 0
				and timestamp_diff(b.start_txn_time, a.time_utc, DAY) between 0 and 7
				
	)
	
	union distinct
	
	SELECT distinct Date(a.event_time) as date, a.crn 
	from 
	(
		SELECT * from `digital_attribution_modelling.sy_sem_prod_clk`
	) a

	inner join
	(
		SELECT crn, start_txn_time, prod_nbr
			, sum(case when checkout_type = 'online' then spend else 0 end) as onl_spend
			, sum(case when checkout_type = 'instore' then spend else 0 end) as ins_spend
		from `digital_attribution_modelling.sy_sem_prod_spend`
		group by 1,2,3
	) e on a.crn = e.crn 
			and a.prod_nbr = e.prod_nbr
			and timestamp_diff(e.start_txn_time, a.event_time, SECOND) > 0
			and timestamp_diff(e.start_txn_time, a.event_time, DAY) between 0 and 7
	
);

