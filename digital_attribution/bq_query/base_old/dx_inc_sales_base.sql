drop table if exists `digital_attribution_modelling.sy_dx_inc_sales_crn_browse`;
create table `digital_attribution_modelling.sy_dx_inc_sales_crn_browse` as
(
SELECT distinct date, a.crn, a.type
FROM
(
	(
		SELECT distinct crn, time_utc, date, event_name,
		case when json_extract(attributes, "$.dom_url[0]") like '%browse%' then 'browse'
					 when json_extract(attributes, "$.dom_url[0]") like '%catalogue%' then 'catelogue'
					 else 'search' end as type
		FROM `wx-bq-poc.wx_lty_digital_attribution_dev.eventstore_events` 
		WHERE (json_extract(attributes, "$.dom_url[0]") like '%browse%' 
				or json_extract(attributes, "$.dom_url[0]") like '%catalogue%'
				or json_extract(attributes, "$.dom_url[0]") like '%search%')
			and event_name like '%view%'
	) a
	left join
	(
		select crn, start_txn_time, sum(tot_amt_incld_gst-ass.tot_wow_dollar_incld_gst) as off_spend
		from 
		(
			select lylty_card_nbr, start_txn_time, prod_nbr, tot_amt_incld_gst, tot_wow_dollar_incld_gst, division_nbr, checkout_nbr
			from wx_lty_digital_attribution_dev.redx_loyalty_article_sales_summary 
			where checkout_nbr <> 100 and division_nbr in (1005,1010) and void_flag <> 'Y' and Date(start_txn_time) between Date('2019-10-01') and Date('2020-04-30')
		) ass
		inner join
		(
			select prod_nbr, division_nbr 
			from 
			(
				select *,rank() over (partition by prod_nbr, division_nbr order by last_update_date desc) as date_rank 
				from `wx-bq-poc.wx_lty_digital_attribution_dev.redx_loyalty_article_master`
			)
			where date_rank=1
		) am on ass.prod_nbr=am.prod_nbr and ass.division_nbr=am.division_nbr
		inner join 
		(
			select lylty_card_nbr,crn 
			from 
			(
				select *,rank() over (partition by crn order by last_update_date desc) as date_rank 
				from `wx-bq-poc.wx_lty_digital_attribution_dev.redx_loyalty_lylty_card_detail` 
				where lylty_card_status=1 and crn is not NULL and crn <> '0'
			)
			where date_rank=1
		) as lcd on ass.lylty_card_nbr=lcd.lylty_card_nbr
		group by 1,2
	) b on a.crn = b.crn 
			and timestamp_diff(b.start_txn_time, a.time_utc, SECOND) > 60
			and timestamp_diff(b.start_txn_time, a.time_utc, DAY) between 0 and 7
			
	left join
	(
		select crn, start_txn_time, sum(tot_amt_incld_gst-ass.tot_wow_dollar_incld_gst) as onl_spend
		from 
		(
			select lylty_card_nbr, start_txn_time, prod_nbr, tot_amt_incld_gst, tot_wow_dollar_incld_gst, division_nbr, checkout_nbr
			from wx_lty_digital_attribution_dev.redx_loyalty_article_sales_summary 
			where checkout_nbr = 100 and division_nbr in (1005,1010) and void_flag <> 'Y' and Date(start_txn_time) between Date('2019-10-01') and Date('2020-04-30')
		) ass
		inner join
		(
			select prod_nbr, division_nbr 
			from 
			(
				select *,rank() over (partition by prod_nbr, division_nbr order by last_update_date desc) as date_rank 
				from `wx-bq-poc.wx_lty_digital_attribution_dev.redx_loyalty_article_master`
			)
			where date_rank=1
		) am on ass.prod_nbr=am.prod_nbr and ass.division_nbr=am.division_nbr
		inner join 
		(
			select lylty_card_nbr,crn 
			from 
			(
				select *,rank() over (partition by crn order by last_update_date desc) as date_rank 
				from `wx-bq-poc.wx_lty_digital_attribution_dev.redx_loyalty_lylty_card_detail` 
				where lylty_card_status=1 and crn is not NULL and crn <> '0'
			)
			where date_rank=1
		) as lcd on ass.lylty_card_nbr=lcd.lylty_card_nbr
		group by 1,2
	) c on a.crn = c.crn 
			and timestamp_diff(c.start_txn_time, a.time_utc, SECOND) >= 10
			and timestamp_diff(c.start_txn_time, a.time_utc, DAY) between 0 and 7	
)
where off_spend is not null and onl_spend is null

);




drop table if exists `digital_attribution_modelling.sy_dx_inc_sales_crn_listadd`;
create table `digital_attribution_modelling.sy_dx_inc_sales_crn_listadd` as
(
SELECT distinct date, a.crn
FROM
(
	(
		SELECT distinct crn, time_utc, date, event_name
		FROM `wx-bq-poc.wx_lty_digital_attribution_dev.eventstore_events` 
		WHERE event_name like '%list add%'
	) a
	left join
	(
		select crn, start_txn_time, sum(tot_amt_incld_gst-ass.tot_wow_dollar_incld_gst) as off_spend
		from 
		(
			select lylty_card_nbr, start_txn_time, prod_nbr, tot_amt_incld_gst, tot_wow_dollar_incld_gst, division_nbr, checkout_nbr
			from wx_lty_digital_attribution_dev.redx_loyalty_article_sales_summary 
			where checkout_nbr <> 100 and division_nbr in (1005,1010) and void_flag <> 'Y' and Date(start_txn_time) between Date('2019-10-01') and Date('2020-04-30')
		) ass
		inner join
		(
			select prod_nbr, division_nbr 
			from 
			(
				select *,rank() over (partition by prod_nbr, division_nbr order by last_update_date desc) as date_rank 
				from `wx-bq-poc.wx_lty_digital_attribution_dev.redx_loyalty_article_master`
			)
			where date_rank=1
		) am on ass.prod_nbr=am.prod_nbr and ass.division_nbr=am.division_nbr
		inner join 
		(
			select lylty_card_nbr,crn 
			from 
			(
				select *,rank() over (partition by crn order by last_update_date desc) as date_rank 
				from `wx-bq-poc.wx_lty_digital_attribution_dev.redx_loyalty_lylty_card_detail` 
				where lylty_card_status=1 and crn is not NULL and crn <> '0'
			)
			where date_rank=1
		) as lcd on ass.lylty_card_nbr=lcd.lylty_card_nbr
		group by 1,2
	) b on a.crn = b.crn 
			and timestamp_diff(b.start_txn_time, a.time_utc, SECOND) > 60
			and timestamp_diff(b.start_txn_time, a.time_utc, DAY) between 0 and 7
			
	left join
	(
		select crn, start_txn_time, sum(tot_amt_incld_gst-ass.tot_wow_dollar_incld_gst) as onl_spend
		from 
		(
			select lylty_card_nbr, start_txn_time, prod_nbr, tot_amt_incld_gst, tot_wow_dollar_incld_gst, division_nbr, checkout_nbr
			from wx_lty_digital_attribution_dev.redx_loyalty_article_sales_summary 
			where checkout_nbr = 100 and division_nbr in (1005,1010) and void_flag <> 'Y' and Date(start_txn_time) between Date('2019-10-01') and Date('2020-04-30')
		) ass
		inner join
		(
			select prod_nbr, division_nbr 
			from 
			(
				select *,rank() over (partition by prod_nbr, division_nbr order by last_update_date desc) as date_rank 
				from `wx-bq-poc.wx_lty_digital_attribution_dev.redx_loyalty_article_master`
			)
			where date_rank=1
		) am on ass.prod_nbr=am.prod_nbr and ass.division_nbr=am.division_nbr
		inner join 
		(
			select lylty_card_nbr,crn 
			from 
			(
				select *,rank() over (partition by crn order by last_update_date desc) as date_rank 
				from `wx-bq-poc.wx_lty_digital_attribution_dev.redx_loyalty_lylty_card_detail` 
				where lylty_card_status=1 and crn is not NULL and crn <> '0'
			)
			where date_rank=1
		) as lcd on ass.lylty_card_nbr=lcd.lylty_card_nbr
		group by 1,2
	) c on a.crn = c.crn 
			and timestamp_diff(c.start_txn_time, a.time_utc, SECOND) >= 1
			and timestamp_diff(c.start_txn_time, a.time_utc, DAY) between 0 and 7	
)
where off_spend is not null and onl_spend is null

);

drop table if exists `digital_attribution_modelling.sy_dx_inc_sales_crn_browse_prod`;
create table `digital_attribution_modelling.sy_dx_inc_sales_crn_browse_prod` as
(
SELECT distinct date, a.crn, a.type
FROM
(
	(
		SELECT distinct a.crn, a.type, b.time_utc, b.date, b.prod_nbr FROM
		(
			SELECT distinct crn, time_utc, date, event_name, 
				case when json_extract(attributes, "$.dom_url[0]") like '%browse%' then 'browse'
					 when json_extract(attributes, "$.dom_url[0]") like '%catalogue%' then 'catelogue'
					 else 'search' end as type
				, json_extract(attributes, "$.udo_tealium_session_id[0]") as session_id
					FROM `wx-bq-poc.wx_lty_digital_attribution_dev.eventstore_events` 
					WHERE (json_extract(attributes, "$.dom_url[0]") like '%browse%' 
							or json_extract(attributes, "$.dom_url[0]") like '%catalogue%'
							or json_extract(attributes, "$.dom_url[0]") like '%search%')
						and event_name like '%view%'
			) a

			inner join 
			(
			SELECT distinct crn, time_utc, date, event_name
				, SPLIT(json_extract(attributes, "$.dom_url[0]"),'/')[OFFSET(5)] as prod_nbr
				, json_extract(attributes, "$.udo_tealium_session_id[0]") as session_id
					FROM `wx-bq-poc.wx_lty_digital_attribution_dev.eventstore_events` 
					WHERE json_extract(attributes, "$.dom_url[0]") like '%woolworths.com.au/shop/productdetails%'
						and event_name like '%view%'
			) b on a.crn = b.crn and a.session_id = b.session_id and a.time_utc > b.time_utc
		where prod_nbr is not null
	) a
	left join
	(
		select crn, start_txn_time, SPLIT(ass.prod_nbr,'-')[OFFSET(0)] as prod_nbr, sum(tot_amt_incld_gst-ass.tot_wow_dollar_incld_gst) as off_spend
		from 
		(
			select lylty_card_nbr, start_txn_time, prod_nbr, tot_amt_incld_gst, tot_wow_dollar_incld_gst, division_nbr, checkout_nbr
			from wx_lty_digital_attribution_dev.redx_loyalty_article_sales_summary 
			where checkout_nbr <> 100 and division_nbr in (1005,1010) and void_flag <> 'Y' and Date(start_txn_time) between Date('2019-10-01') and Date('2020-04-30')
		) ass
		inner join
		(
			select prod_nbr, division_nbr 
			from 
			(
				select *,rank() over (partition by prod_nbr, division_nbr order by last_update_date desc) as date_rank 
				from `wx-bq-poc.wx_lty_digital_attribution_dev.redx_loyalty_article_master`
			)
			where date_rank=1
		) am on ass.prod_nbr=am.prod_nbr and ass.division_nbr=am.division_nbr
		inner join 
		(
			select lylty_card_nbr,crn 
			from 
			(
				select *,rank() over (partition by crn order by last_update_date desc) as date_rank 
				from `wx-bq-poc.wx_lty_digital_attribution_dev.redx_loyalty_lylty_card_detail` 
				where lylty_card_status=1 and crn is not NULL and crn <> '0'
			)
			where date_rank=1
		) as lcd on ass.lylty_card_nbr=lcd.lylty_card_nbr
		group by 1,2,3
	) b on a.crn = b.crn 
			and a.prod_nbr = b.prod_nbr
			and timestamp_diff(b.start_txn_time, a.time_utc, SECOND) > 60
			and timestamp_diff(b.start_txn_time, a.time_utc, DAY) between 0 and 7
			
	left join
	(
		select crn, start_txn_time, SPLIT(ass.prod_nbr,'-')[OFFSET(0)] as prod_nbr, sum(tot_amt_incld_gst-ass.tot_wow_dollar_incld_gst) as onl_spend
		from 
		(
			select lylty_card_nbr, start_txn_time, prod_nbr, tot_amt_incld_gst, tot_wow_dollar_incld_gst, division_nbr, checkout_nbr
			from wx_lty_digital_attribution_dev.redx_loyalty_article_sales_summary 
			where checkout_nbr = 100 and division_nbr in (1005,1010) and void_flag <> 'Y' and Date(start_txn_time) between Date('2019-10-01') and Date('2020-04-30')
		) ass
		inner join
		(
			select prod_nbr, division_nbr 
			from 
			(
				select *,rank() over (partition by prod_nbr, division_nbr order by last_update_date desc) as date_rank 
				from `wx-bq-poc.wx_lty_digital_attribution_dev.redx_loyalty_article_master`
			)
			where date_rank=1
		) am on ass.prod_nbr=am.prod_nbr and ass.division_nbr=am.division_nbr
		inner join 
		(
			select lylty_card_nbr,crn 
			from 
			(
				select *,rank() over (partition by crn order by last_update_date desc) as date_rank 
				from `wx-bq-poc.wx_lty_digital_attribution_dev.redx_loyalty_lylty_card_detail` 
				where lylty_card_status=1 and crn is not NULL and crn <> '0'
			)
			where date_rank=1
		) as lcd on ass.lylty_card_nbr=lcd.lylty_card_nbr
		group by 1,2,3
	) c on a.crn = c.crn 
			and a.prod_nbr = c.prod_nbr
			and timestamp_diff(c.start_txn_time, a.time_utc, SECOND) >= 10
			and timestamp_diff(c.start_txn_time, a.time_utc, DAY) between 0 and 7	
)
where off_spend is not null and onl_spend is null

);



