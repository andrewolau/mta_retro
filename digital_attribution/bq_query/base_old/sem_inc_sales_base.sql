drop table if exists `digital_attribution_modelling.sy_sem_inc_sales_broad`;
create table `digital_attribution_modelling.sy_sem_inc_sales_broad` as
(
	SELECT distinct date, a.crn
	FROM
	(
		(
			SELECT distinct crn, time_utc, date
			FROM `wx-bq-poc.wx_lty_digital_attribution_dev.eventstore_events` 
			where channel = 'sem' and Date(time_utc) between Date('2020-01-01') and Date('2020-06-28')
		) a
		inner join
		(
			select crn, start_txn_time, sum(tot_amt_incld_gst-ass.tot_wow_dollar_incld_gst) as spend
			from 
			(
				select lylty_card_nbr, start_txn_time, prod_nbr, tot_amt_incld_gst, tot_wow_dollar_incld_gst, division_nbr, checkout_nbr
				from wx_lty_digital_attribution_dev.redx_loyalty_article_sales_summary 
				where division_nbr in (1005,1010) and void_flag <> 'Y' and Date(start_txn_time) >= Date('2020-01-01')
			) ass
			inner join
			(
				select prod_nbr, division_nbr 
				from 
				(
					select *,rank() over (partition by prod_nbr, division_nbr order by last_update_date desc) as date_rank 
					from `wx-bq-poc.wx_lty_digital_attribution_dev.redx_loyalty_article_master`
				)
				where date_rank=1
			) am on ass.prod_nbr=am.prod_nbr and ass.division_nbr=am.division_nbr
			inner join 
			(
				select lylty_card_nbr,crn 
				from 
				(
					select *,rank() over (partition by crn order by last_update_date desc) as date_rank 
					from `wx-bq-poc.wx_lty_digital_attribution_dev.redx_loyalty_lylty_card_detail` 
					where lylty_card_status=1 and crn is not NULL and crn <> '0'
				)
				where date_rank=1
			) as lcd on ass.lylty_card_nbr=lcd.lylty_card_nbr
			group by 1,2
		) b on a.crn = b.crn 
				and timestamp_diff(b.start_txn_time, a.time_utc, SECOND) > 60
				and timestamp_diff(b.start_txn_time, a.time_utc, DAY) between 0 and 7
				
	)
);




