SELECT a.*, b.crn as crn_1, b.spend as spend_1, c.crn as crn_2, c.spend_fin as spend_2
FROM 
(	SELECT campaign_code, cast(campaign_start_date as date) as campaign_start_date, count(distinct crn) as crn, sum(inc_sales) as spend, 'GT' as source
		FROM `Attribution_Safari_GT_output.Safari_crn_level_game_theory_output_safari_full_promo_halo_post_current`
		where fw_start_date = DATE_SUB(DATE_TRUNC(current_date(), week(Monday)), INTERVAL 7 DAY) and pph = 'promo'
		group by 1,2
		
		union all
		
		SELECT campaign_code, CAST(campaign_start_date as date) as campaign_start_date, count(distinct crn) as crn, sum(inc_sales) as spend, 'WS' as source
		FROM `Attribution_Safari_Matching.ws_2b_current`
		where fw_start_date = DATE_SUB(DATE_TRUNC(current_date(), week(Monday)), INTERVAL 7 DAY) and open_flag = 1
		group by 1,2
		
		union all

		SELECT campaign_code, CAST(campaign_start_date as date) as campaign_start_date, count(distinct crn) as crn, sum(inc_sales) as spend, 'TTL' as source
		FROM `Attribution_Safari_Matching.ttl_crn_level_store`
		where CAST(fw_start_date as date) = DATE_SUB(DATE_TRUNC(current_date(), week(Monday)), INTERVAL 7 DAY) and open_flag = 1 and campaign_code not like 'W%'
		group by 1,2
		
		union all
		
		SELECT campaign_code, cast(campaign_start_date as date) as campaign_start_date, count(distinct crn) as crn, sum(inc_sales) as spend, 'GT-BigW' as source
		FROM `Attribution_Safari_GT_output.Safari_crn_level_game_theory_output_safari_full_promo_halo_post_current_big_w`
		where fw_start_date = DATE_SUB(DATE_TRUNC(current_date(), week(Monday)), INTERVAL 7 DAY) and pph = 'promo'
		group by 1,2
) a 
left join 
(
	SELECT campaign_code, campaign_start_date, count(distinct crn) as crn, sum(spend) as spend
	FROM `digital_attribution_modelling.dacamp_prod_activation`
	group by 1,2
) b on a.campaign_code = b.campaign_code and a.campaign_start_date = b.campaign_start_date 
left join
(
SELECT campaign_code, campaign_start_date, count(distinct crn) as crn, sum(attributed_inc_sales) as spend_fin
	FROM `digital_attribution_modelling.dacamp_prod_mc_union_fb`
	group by 1,2
) c on a.campaign_code = c.campaign_code and a.campaign_start_date = c.campaign_start_date

order by 1,2








SELECT a.*, b.crn as crn_1, b.spend as spend_1, c.crn as crn_2, c.spend_fin as spend_2
FROM 
(	
		
		SELECT campaign_code, cast(campaign_start_date as date) as campaign_start_date, count(distinct crn) as crn, sum(inc_sales) as spend, 'GT-BigW' as source
		FROM `Attribution_Safari_GT_output.Safari_crn_level_game_theory_output_safari_full_promo_halo_post_current_big_w`
		where fw_start_date = '2020-10-19' and pph = 'promo'
		group by 1,2
) a 
left join
(
	SELECT campaign_code, campaign_start_date, count(distinct crn) as crn, sum(spend) as spend
	FROM `digital_attribution_modelling.dacamp_prod_activation`
	group by 1,2
) b on a.campaign_code = b.campaign_code and a.campaign_start_date = b.campaign_start_date 
left join
(
SELECT campaign_code, campaign_start_date, count(distinct crn) as crn, sum(attributed_inc_sales) as spend_fin
	FROM `digital_attribution_modelling.dacamp_prod_mc_union_fb`
	group by 1,2
) c on a.campaign_code = c.campaign_code and a.campaign_start_date = c.campaign_start_date

order by 1,2
